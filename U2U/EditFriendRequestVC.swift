//
//  EditFriendRequestVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 11/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class EditFriendRequestVC: UIViewController {
   
    @IBOutlet weak var btnFriendUnFriend: UIButton!
    @IBOutlet weak var height_manage_req: NSLayoutConstraint!
    @IBOutlet weak var height_numberOfReceivedReq: NSLayoutConstraint!
    @IBOutlet weak var lbl_manageSentReq: UILabel!
    @IBOutlet weak var lbl_numberOfReq: UILabel!
    @IBOutlet weak var height_reqTbl: NSLayoutConstraint!
    @IBOutlet weak var tbl_sentReq: UITableView!
    @IBOutlet weak var loader_friendReq: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noRequest: UILabel!
    @IBOutlet weak var tbl_request: UITableView!
    @IBOutlet weak var lbl_contactNumber: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var lbl_friendReq: UILabel!
    
    var reqDelegate:UpdateRequest?
    var u2uHomeVM = U2UHomeViewModel()
    var selectedRequests = [String]()
    var selectedSentRequests = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_search.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        
        self.adjustTblHeight()
        getMoreFriendSentReq(page: 1)
    }
    func getMoreFriendReq(page:Int) {
        u2uHomeVM.page = page
        loader_friendReq.startAnimating()
        u2uHomeVM.getUserFriendReq(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friendReq.stopAnimating()
                self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                self.tbl_request.reloadData()
                if self.u2uHomeVM.friendRequest.count == 0{
                    //self.lbl_noRequest.isHidden = false
                }else{
                    //self.lbl_noRequest.isHidden = true
                }
                self.adjustTblHeight()
            }
        }
    }
    func getMoreFriendSentReq(page:Int) {
        u2uHomeVM.page = page
        loader_friendReq.startAnimating()
        u2uHomeVM.getUserSentFriendReq(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friendReq.stopAnimating()
                self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendSentRequest.count) request"
                self.tbl_sentReq.reloadData()
                if self.u2uHomeVM.friendSentRequest.count == 0{
                    //self.lbl_noRequest.isHidden = false
                }else{
                    //self.lbl_noRequest.isHidden = true
                }
                self.manageEmptyData()
            }
        }
    }
    func adjustTblHeight(){
        var count = 4//self.u2uHomeVM.friendRequest.count
        if self.u2uHomeVM.friendRequest.count < 4 {
            count = self.u2uHomeVM.friendRequest.count
        }
        height_reqTbl.constant = 58 * CGFloat(count)
        lbl_numberOfReq.text = "Respond to your \(u2uHomeVM.friendRequest.count) friend requests"
        lbl_contactNumber.text = "\(u2uHomeVM.friendRequest.count + u2uHomeVM.friendSentRequest.count) request"
        view.layoutIfNeeded()
    }
    func manageEmptyData() {
        if u2uHomeVM.friendRequest.count == 0{
            sentReqOnly()
        }
        if u2uHomeVM.friendSentRequest.count == 0{
            receivedReqOnly()
        }
    }
    var isBothShow = true
    @IBOutlet weak var imgSelectUnselect: UIImageView!
    @IBOutlet weak var btn_all: UIButton!
    @IBAction func selectAllBtnAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            imgSelectUnselect.image = UIImage(named: "unselected")
            selectedRequests.removeAll()
            selectedSentRequests.removeAll()
        }else{
            sender.isSelected = true
            imgSelectUnselect.image = UIImage(named: "allSelected")

            if isBothShow{
                selectedRequests = u2uHomeVM.friendRequest.map{$0.id}
                selectedSentRequests = u2uHomeVM.friendSentRequest.map{$0.id}
            }else{
                if isReceivedReq{
                    selectedRequests = u2uHomeVM.friendRequest.map{$0.id}
                    selectedSentRequests.removeAll()
                }else{
                    selectedSentRequests = u2uHomeVM.friendSentRequest.map{$0.id}
                    selectedRequests.removeAll()
                }
            }
        }
        tbl_request.reloadData()
        tbl_sentReq.reloadData()
        firendBtnDisableUnable()
    }
    @IBOutlet weak var leading_filter: NSLayoutConstraint!
    @IBOutlet weak var view_filter: UIView!
    
    @IBOutlet weak var btn_one: UIButton!
    @IBOutlet weak var btn_two: UIButton!
    @IBOutlet weak var btn_three: UIButton!
    var isFilter = false
    var isReceivedReq = true
    @IBAction func sortBtnAction(_ sender: UIButton) {
        isFilter = false
        leading_filter.constant = sender.frame.origin.x
        btn_one.setTitle("By name", for: .normal)
        btn_two.setTitle("By date", for: .normal)
        btn_three.setTitle("By common friends", for: .normal)
        view_filter.isHidden = false
    }
    @IBAction func filterBtnAction(_ sender: UIButton) {
        isFilter = true
        leading_filter.constant = sender.frame.origin.x
        btn_one.setTitle("Friend requests only", for: .normal)
        btn_two.setTitle("Sent invitations only", for: .normal)
        btn_three.setTitle("Suggestions", for: .normal)
        view_filter.isHidden = false
    }
    @IBAction func filterOptionBtnAction(_ sender: UIButton) {
        view_filter.isHidden = true
        if isFilter{
            switch sender.tag {
            case 1:
                receivedReqOnly()
            case 2:
                sentReqOnly()
            default:
                sugestionOnly()
            }
        }else{
            switch sender.tag {
            case 1:
                u2uHomeVM.sorting = "name"
            case 2:
                u2uHomeVM.sorting = "date"
            default:
                u2uHomeVM.sorting = ""
            }
            applySorting()
        }
    }
    func applySorting() {
        refreshData(search: txt_search.text!)
    }
    func receivedReqOnly() {
        selectedSentRequests.removeAll()
        isReceivedReq = true
        isBothShow = false
        height_reqTbl.constant += tbl_sentReq.frame.size.height + 20
        height_manage_req.constant = 0
        lbl_numberOfReq.text = "Respond to your \(u2uHomeVM.friendRequest.count) friend requests"
        tbl_request.isHidden = false
        tbl_sentReq.isHidden = true
    }
    func sentReqOnly() {
        selectedRequests.removeAll()
        isReceivedReq = false
        isBothShow = false
        height_reqTbl.constant = 0
        height_manage_req.constant = 0
        lbl_numberOfReq.text = "Mange to your sent requests"
        tbl_request.isHidden = true
        tbl_sentReq.isHidden = false
    }
    func sugestionOnly()  {
        
    }
    @IBAction func banReqBtnAction(_ sender: UIButton){
        
    }
    @IBAction func deleteReqBtnAction(_ sender: UIButton){
        if selectedRequests.count == 0 && selectedSentRequests.count == 0{
            U2UProxy.shared.showOnlyAlert(message: "Please select request", viewController: self)
            return
        }
        var ids = selectedRequests.joined(separator: ",")
        ids = "\(ids),\(selectedSentRequests.joined(separator: ","))"
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: ids ) { (status) in
        print(status)
        DispatchQueue.main.async {
            if status == "success"{
                self.refreshData(search: self.txt_search.text!)
            }
            }
        }
    }
    @IBAction func friendUnFriendBtnAction(_ sender: UIButton){

        if selectedRequests.count > 0{
            let ids = selectedRequests.joined(separator: ",")
            u2uHomeVM.acceptFriendRequest(viewController: self, request_id: ids) { (status) in
                print(status)
                DispatchQueue.main.async {
                    if status == "success"{
                        self.refreshData(search: self.txt_search.text!)
                    }
                }
            }
        }
        if selectedSentRequests.count > 0{
            let ids = selectedSentRequests.joined(separator: ",")
            u2uHomeVM.rejectFriendRequest(viewController: self, request_id: ids ) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.refreshData(search: self.txt_search.text!)
                }
                }
            }
        }

    }
    @IBAction func BackBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func searchBtnAction(_ sender: UIButton) {
        lbl_friendReq.isHidden = true
        lbl_contactNumber.isHidden = true
        view_search.isHidden = false
    }
}
 extension EditFriendRequestVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_request{
            return u2uHomeVM.friendRequest.count
        }else{
            return u2uHomeVM.friendSentRequest.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestTVC") as! FriendRequestTVC
        if tableView == tbl_request{
            cell.btn_reject.tag = indexPath.row
            cell.btn_acceptReq.tag = indexPath.row
            cell.btn_selectUnselect.tag = indexPath.row
            cell.btn_selectUnselect.layer.setValue("received_request", forKey: "request")
            cell.firendDetails = u2uHomeVM.friendRequest[indexPath.row]
            cell.delegate = self
            cell.selectionDelegate = self
            if selectedRequests.contains(u2uHomeVM.friendRequest[indexPath.row].id){
                cell.btn_selectUnselect.setImage(UIImage(named: "allSelected"), for: .normal)
            }else{
                cell.btn_selectUnselect.setImage(UIImage(named: "unselected"), for: .normal)
            }
            if indexPath.row == u2uHomeVM.friendRequest.count-1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFriendReq(page: u2uHomeVM.page + 1)
                }
            }
        }else{
            cell.btn_cancel.tag = indexPath.row
            cell.btn_selectUnselect.tag = indexPath.row
            cell.btn_selectUnselect.layer.setValue("sent_request", forKey: "request")
            cell.firendDetails = u2uHomeVM.friendSentRequest[indexPath.row]
            cell.delegate = self
            cell.selectionDelegate = self
            if selectedSentRequests.contains(u2uHomeVM.friendSentRequest[indexPath.row].id){
                cell.btn_selectUnselect.setImage(UIImage(named: "allSelected"), for: .normal)
            }else{
                cell.btn_selectUnselect.setImage(UIImage(named: "unselected"), for: .normal)
            }
            if indexPath.row == u2uHomeVM.friendSentRequest.count-1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFriendSentReq(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
 }
extension EditFriendRequestVC:SelectFriendReq{
    func selectReq(req_id: String, index: Int, request: String) {
        
        if request == "received_request"{
            if selectedRequests.contains(req_id){
                let index = selectedRequests.firstIndex(of: req_id)
                selectedRequests.remove(at: index!)
            }else{
                selectedRequests.append(req_id)
            }
        }else{
            if selectedSentRequests.contains(req_id){
                let index = selectedSentRequests.firstIndex(of: req_id)
                selectedSentRequests.remove(at: index!)
            }else{
                selectedSentRequests.append(req_id)
            }
        }
        
        var count = 0
        if request == "received_request"{
            tbl_request.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        }else{
            tbl_sentReq.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        }
        count += u2uHomeVM.friendRequest.count
        count += u2uHomeVM.friendSentRequest.count
        if (selectedRequests.count + selectedSentRequests.count) == count{
            btn_all.isSelected = true
            imgSelectUnselect.image = UIImage(named: "allSelected")
        }else{
            btn_all.isSelected = false
            imgSelectUnselect.image = UIImage(named: "unselected")
        }
        firendBtnDisableUnable()
    }
    func firendBtnDisableUnable()  {
        if selectedRequests.count > 0 && selectedSentRequests.count > 0{
            btnFriendUnFriend.isEnabled = false
        }else{
            btnFriendUnFriend.isEnabled = true
        }
    }
}
 extension EditFriendRequestVC:UITextFieldDelegate{
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
     }
     func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
     }
    func refreshData(search:String)  {
        u2uHomeVM.search = search
        u2uHomeVM.contact_LoadMore = true
        if isReceivedReq {
            u2uHomeVM.friendRequest.removeAll()
            tbl_request.reloadData()
            getMoreFriendReq(page: 1)
        }else{
            u2uHomeVM.friendSentRequest.removeAll()
            tbl_sentReq.reloadData()
            getMoreFriendSentReq(page: 1)
        }
    }
 }
 extension EditFriendRequestVC:RequestModify{
    func acceptRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.acceptFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendRequest.remove(at: index)
                   if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noRequest.isHidden = false
                    }else{self.lbl_noRequest.isHidden = true}
                    self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                    self.tbl_request.reloadData()
                    self.reqDelegate?.refreshRequest(friend: req)
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name(U2U_Notifications.Request_Refresh), object: nil)
                }
            }
        }
    }
    func rejectRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendRequest.remove(at: index)
                   if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noRequest.isHidden = false
                    }else{self.lbl_noRequest.isHidden = true}
                    self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                    self.tbl_request.reloadData()
                    self.reqDelegate?.refreshRequest(friend: req)
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name(U2U_Notifications.Request_Refresh), object: nil)
                }
            }
        }
    }
    func cancelRequest(index: Int) {
        let req = u2uHomeVM.friendSentRequest[index]
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: req.id) { (status) in
        print(status)
        DispatchQueue.main.async {
            if status == "success"{
                self.u2uHomeVM.friendSentRequest.remove(at: index)
                if self.u2uHomeVM.friendSentRequest.count == 0{
                    //self.lbl_noRequest.isHidden = false
                }else{//self.lbl_noRequest.isHidden = true
                    
                }
                self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                self.tbl_sentReq.reloadData()
                self.reqDelegate?.refreshRequest(friend: req)
                let nc = NotificationCenter.default
                nc.post(name: Notification.Name(U2U_Notifications.Request_Refresh), object: nil)
            }
            }
        }
    }
 }
