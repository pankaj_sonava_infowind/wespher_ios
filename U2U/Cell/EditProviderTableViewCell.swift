//
//  EditProviderTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 07/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class EditProviderTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgViewSelectUnselect: UIButton!
    
    var isselected:Bool = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
