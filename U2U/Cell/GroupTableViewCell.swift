//
//  GroupTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 06/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol GroupAction {
    func tapOnMessage(grp:Group)
    func selectGroup(chatroom_id:String,index:Int )
    func joinGroup(group_id:String,index:Int )
}
class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var img_like: UIImageView!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var groupImgView: UIImageView!
    @IBOutlet weak var lblGroupName: UILabel!
    @IBOutlet weak var lblGroupMembers: UILabel!
    @IBOutlet weak var lblGroupPostDay: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imgSelectUnselectCell: UIButton!
    @IBOutlet weak var btn_joinGroup: UIButton!
    
    var isselected:Bool = false
    var delegate:GroupAction?
    var selectedGroups:[String]!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    var groupDetails:Group?{ 
        didSet{
            groupImgView.sd_setImage(with: URL(string: groupDetails!.chatroom_group_pic ?? "")) { (image, error, casch, url) in
                if let imgg = image{
                    DispatchQueue.main.async {
                        self.groupImgView.image = imgg
                    }
                }else{
                    DispatchQueue.main.async {
                        self.groupImgView.image = UIImage(named: "User")
                    }
                }
            }
            //groupImgView.sd_setImage(with: URL(string: groupDetails!.chatroom_group_pic ?? ""), placeholderImage: UII hi Brian I hope you know howmage(named: "User"), options: .fromCacheOnly, context: nil)
            lblGroupName.text = groupDetails?.chatroom_name
            lblGroupMembers.text = "Owner name: \(groupDetails!.chatroom_owner_name ?? "")"
            lblDescription.text = "Owner id: \(groupDetails!.username ?? "")"
            if selectedGroups != nil{
                if selectedGroups.contains(groupDetails!.chatroom_id){
                    imgSelectUnselectCell.isSelected = true
                }else{
                    imgSelectUnselectCell.isSelected = false
                }
            }
            if groupDetails!.liked == "1"{
                img_like.isHidden = false
            }else{
                img_like.isHidden = true
            }
            if img != nil{
                if groupDetails?.pinned == "0"{
                    img.isHidden = true
                }else{
                    img.isHidden = false
                }
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionMoreInfo(_ sender: Any){
        
    }
    @IBAction func messageBtnAction(_ sender: UIButton) {
        delegate?.tapOnMessage(grp: groupDetails!)
    }
    
    @IBAction func selectUnSelectBtnAction(_ sender: UIButton) {
        delegate?.selectGroup(chatroom_id: groupDetails!.chatroom_id, index: sender.tag)
    }
    @IBAction func joinGroupBtnAction(_ sender: UIButton) {
        delegate?.joinGroup(group_id: groupDetails!.chatroom_id, index: sender.tag)
    }
}
