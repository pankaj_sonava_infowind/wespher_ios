//
//  RestaurentCollectionViewCell.swift
//  VKChatU2U
//
//  Created by mac on 27/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class RestaurantCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblRName: UILabel!
    @IBOutlet weak var lblRType: UILabel!
    @IBOutlet weak var lblRStatus: UILabel!
    
}
