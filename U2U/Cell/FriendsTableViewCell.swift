//
//  FriendsTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 03/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol FriendAction {
    func tapOnChat(friend:Friend)
    func tapOnAudioCall(friend:Friend)
    func tapOnVideoCall(friend:Friend)
    func slectFriend(firend_id:String,index:Int)
}

class FriendsTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgFriend: UIImageView!
    @IBOutlet weak var imgStatusFriend: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblFriendName: UILabel!
    @IBOutlet weak var imgSelectedUnselectedCell: UIButton!
    
    var isselected:Bool = false
    var delegate:FriendAction?
    var selectedFriends:[String]!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        imgFriend.layer.borderColor = UIColor.black.cgColor
    }

    var friendDetails:Friend?{
        didSet{
            imgFriend.sd_setImage(with: URL(string: friendDetails!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblFriendName.text = friendDetails?.firstname ?? "" + (friendDetails?.lastname ?? "")
            if selectedFriends != nil{
                if selectedFriends.contains(friendDetails!.id){
                    imgSelectedUnselectedCell.isSelected = true
                }else{
                    imgSelectedUnselectedCell.isSelected = false
                }
            }
            if friendDetails?.pined == 0{
                img.isHidden = true
            }else{
                img.isHidden = false
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func actionVideoCall(_ sender: Any){
        if let frnd = friendDetails{
            delegate?.tapOnVideoCall(friend: frnd)
        }
    }
    @IBAction func actionCall(_ sender: Any){
        if let frnd = friendDetails{
            delegate?.tapOnAudioCall(friend: frnd)
        }
    }
    @IBAction func actionMessage(_ sender: Any){
        if let frnd = friendDetails{
            delegate?.tapOnChat(friend: frnd)
        }
    }
    @IBAction func selectUnSelectBtnAction(_ sender: UIButton) {
        delegate?.slectFriend(firend_id: friendDetails!.id, index: sender.tag)
    }
}
