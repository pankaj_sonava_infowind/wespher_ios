//
//  AddMemberTVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 08/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol AddMember {
    func selectMember(index:Int)
}
class AddMemberTVC: UITableViewCell {

    @IBOutlet weak var btn_selectUnselect: UIButton!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    var delegate:AddMember?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    var member:Friend?{
        didSet{
            img_user.sd_setImage(with: URL(string: member!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lbl_name.text = member?.firstname ?? "" + (member?.lastname ?? "")
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectMemberBtnAction(_ sender: UIButton) {
        delegate?.selectMember(index: sender.tag)
    }
    
}
