//
//  AllC_ChildTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 28/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol ContactSelection {
    func selectContact(contact_id:String,index:Int)
}

class AllChildTableViewCell: UITableViewCell{
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var imgBussiness: UIImageView!
    @IBOutlet weak var imgSelectUnselectCell: UIButton!
    
    var isselected:Bool = false
    var selectedContacts:[String]!
    var delegate:ContactSelection?
    override func awakeFromNib(){
        super.awakeFromNib()
        // Initialization code
        
    }
    var contactDetals:Contact?{
        didSet{
            contactImg.sd_setImage(with: URL(string: contactDetals!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblContactName.text = contactDetals?.firstname ?? "" + (contactDetals?.lastname ?? "")
            
            if contactDetals!.is_online == 1{
                imgStatus.backgroundColor = .blue_online
            }else{
                imgStatus.backgroundColor = .gray
            }
            if contactDetals!.is_request == 1{
            }else{
            }
            if selectedContacts != nil{
                if selectedContacts.contains(contactDetals!.ID){
                    imgSelectUnselectCell.isSelected = true
                }else{
                    imgSelectUnselectCell.isSelected = false
                }
            }
//            if contactDetals?.pined == 0{
//                img.isHidden = true
//            }else{
//                img.isHidden = false
//            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func selectContactBtnAction(_ sender: UIButton) {
        delegate?.selectContact(contact_id: contactDetals!.j_id, index: sender.tag)
    }
}
