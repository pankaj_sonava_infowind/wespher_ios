//
//  MycontactsTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 08/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MycontactsTableViewCell: UITableViewCell
{

    @IBOutlet weak var btn_addFriend: UIButton!
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var imgContactStatus: UIImageView!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblMutualContacts: UILabel!
    
    var delegate:ModifyContact?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    var contactDetals:Contact?{
        didSet{
            imgContact.sd_setImage(with: URL(string: contactDetals!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblContactName.text = contactDetals?.firstname ?? "" + (contactDetals?.lastname ?? "")
            
            if contactDetals!.is_online == 1{
                imgContactStatus.backgroundColor = .blue_online
            }else{
                imgContactStatus.backgroundColor = .gray
            }
            if contactDetals!.is_request == 1{
                btn_addFriend.setImage(UIImage.init(systemName: "minus"), for: .normal)
                btn_addFriend.backgroundColor = UIColor.red
            }else{
                btn_addFriend.setImage(UIImage(named: "AddContact"), for: .normal)
                btn_addFriend.backgroundColor = UIColor.white
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func addFrirndBtnAction(_ sender: UIButton) {
        delegate?.tapOnSendRequest(index: sender.tag, contact: contactDetals!, cellFor: "contact")
    }
    
}
