//
//  RestaurentTableViewCell.swift
//  VKChatU2U
//
//  Created by mac on 27/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class RestaurantTableViewCell: UITableViewCell
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblRName: UILabel!
    @IBOutlet weak var lblRType: UILabel!
    @IBOutlet weak var lblRStatus: UILabel!
    
    @IBAction func actionMoreInfo(_ sender: Any)
    {
        
    }
    @IBAction func actionShare(_ sender: Any)
    {
        
    }
    @IBAction func actionMsg(_ sender: Any)
    {
       print("Hellooo")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
