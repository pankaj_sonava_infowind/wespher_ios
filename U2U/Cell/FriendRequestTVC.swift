//
//  FriendRequestTVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 26/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol SelectFriendReq {
    func selectReq(req_id:String,index:Int,request:String)
}

class FriendRequestTVC: UITableViewCell {
    
    @IBOutlet weak var btn_selectUnselect: UIButton!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var lbl_numberOfMutalContact: UILabel!
    @IBOutlet weak var btn_reject: UIButton!
    @IBOutlet weak var img_userStatus: UIImageView!
    @IBOutlet weak var btn_acceptReq: UIButton!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_user: UIImageView!
    
    var delegate:RequestModify?
    var selectionDelegate:SelectFriendReq?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    var firendDetails:Friend?{
        didSet{
            img_user.sd_setImage(with: URL(string: firendDetails!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lbl_name.text = firendDetails?.firstname ?? "" + (firendDetails?.lastname ?? "")
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectBtnAction(_ sender: UIButton) {
        let request = sender.layer.value(forKey: "request") as! String
        selectionDelegate?.selectReq(req_id: firendDetails!.id!, index: sender.tag, request: request)
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        delegate?.cancelRequest(index: sender.tag)
    }
    @IBAction func acceptReqBtnAction(_ sender: UIButton) {
        delegate?.acceptRequest(index: sender.tag)
    }
    @IBAction func rejectBtnAction(_ sender: UIButton) {
        delegate?.rejectRequest(index: sender.tag)
    }
}
