//
//  GroupsCollectionViewCell.swift
//  VKChatU2U
//
//  Created by mac on 06/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class GroupsCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var featuredImgView: UIImageView!
    @IBOutlet weak var lblFeaturedName: UILabel!
    @IBOutlet weak var lblFeaturedMember: UILabel!
    @IBOutlet weak var lblFeaturedPostDay: UILabel!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var btn_joinGroup: UIButton!
    
    var delegate:GroupAction?
    var groupDetails:Group?{
        didSet{
            featuredImgView.sd_setImage(with: URL(string: groupDetails!.chatroom_group_pic ?? "")) { (image, error, casch, url) in
                if let imgg = image{
                    DispatchQueue.main.async {
                        self.featuredImgView.image = imgg
                    }
                }else{
                    DispatchQueue.main.async {
                        self.featuredImgView.image = UIImage(named: "User")
                    }
                }
            }
            //featuredImgView.sd_setImage(with: URL(string: groupDetails!.chatroom_group_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblFeaturedName.text = groupDetails?.chatroom_name
            lblFeaturedMember.text = "Owner name: \(groupDetails!.chatroom_owner_name ?? "")"
            lblFeaturedPostDay.text = "Owner id: \(groupDetails!.username ?? "")"
        }
    }
    @IBAction func messageBtnAction(_ sender: UIButton) {
        delegate?.tapOnMessage(grp: groupDetails!)
    }
    @IBAction func joinGrpBtnAction(_ sender: UIButton) {
        delegate?.joinGroup(group_id: groupDetails!.chatroom_id, index: sender.tag)
    }
}
