//
//  ContactsCollectionViewCell.swift
//  VKChatU2U
//
//  Created by mac on 03/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SDWebImage

protocol ModifyContact {
    func tapOnSendRequest(index:Int,contact:Contact,cellFor:String)
    func tapOnMessageBtn(index:Int,contact:Contact)
}

class ContactsCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var btn_friendReq: UIButton!
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var imgStatusContact: UIImageView!
    @IBOutlet weak var lblContactName: UILabel!
    
    var delegate:ModifyContact?
    var contactDetals:Contact?{
        didSet{
            imgContact.sd_setImage(with: URL(string: contactDetals!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblContactName.text = contactDetals?.firstname ?? "" + (contactDetals?.lastname ?? "")
            
            if contactDetals!.is_online == 1{
                imgStatusContact.backgroundColor = .blue_online
            }else{
                imgStatusContact.backgroundColor = .gray
            }
            if contactDetals!.is_request == 1{
                btn_friendReq.setImage(UIImage.init(systemName: "minus"), for: .normal)
                btn_friendReq.backgroundColor = UIColor.red
            }else{
                btn_friendReq.setImage(UIImage(named: "AddContact"), for: .normal)
                btn_friendReq.backgroundColor = UIColor.white
            }
        }
    }
    @IBAction func sendFriendReqBtnAction(_ sender: UIButton) {
        let cellfor = sender.layer.value(forKey: "For") as! String
        delegate?.tapOnSendRequest(index: sender.tag, contact: contactDetals!, cellFor: cellfor)
    }
    @IBAction func messageBtnAction(_ sender: UIButton) {
        delegate?.tapOnMessageBtn(index: sender.tag, contact: contactDetals!)
    }
}
