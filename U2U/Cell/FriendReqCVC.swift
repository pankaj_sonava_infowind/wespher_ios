//
//  FriendReqCVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol RequestModify{
    func acceptRequest(index:Int)
    func rejectRequest(index:Int)
    func cancelRequest(index:Int)
}

class FriendReqCVC: UICollectionViewCell {
    
    @IBOutlet weak var btn_reject: UIButton!
    @IBOutlet weak var btn_acceptReq: UIButton!
    @IBOutlet weak var img_userStatus: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var img_user: UIImageView!
   
    var delegate:RequestModify?
    var firendDetails:Friend?{
        didSet{
            img_user.sd_setImage(with: URL(string: firendDetails!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lbl_name.text = firendDetails?.firstname ?? "" + (firendDetails?.lastname ?? "")
        }
    }
    @IBAction func acceptFriendReqBtnAction(_ sender: UIButton) {
        delegate?.acceptRequest(index: sender.tag)
    }
    @IBAction func rejectRequestBtnAction(_ sender: UIButton) {
        delegate?.rejectRequest(index: sender.tag)
    }
}
