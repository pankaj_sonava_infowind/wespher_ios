//
//  U2U_ApiManager.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 14/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import SVProgressHUD
import SwiftyJSON

class U2U_ApiManager {
    private init() {}
    static let shared = U2U_ApiManager()
        
    static let socket_url = "wss://wespher.com/teamniosocketserver"
    static let base_url = "https://wespher.rockstardaddy.com/wespher_support_system/ws/app.php?act="
    static let image_base_url = "https://wespher.rockstardaddy.com/wespher_support_system/include/img/"
    static let imgPrefix = "./include/img/share//"
    static let downloadingUrl = "https://wespher.rockstardaddy.com/wespher_support_system"
        
    static let contactList_endPoint = "contact-list-u2u"
    static let sendFriendRequest_endPoint = "send-friend-request"
    static let acceptRequest_endPoint = "accept-request"
    static let rejectRequest_endPoint = "reject-request"
    static let friendRequestList_endPoint = "friend-request-list-u2u"
    static let friendList_endPoint = "friend-list-u2u"
    static let createGroup_endPoint = "create-group-u2u"
    static let createRoom_endPoint = "pm-u2u"
    static let updateGroup_endPoint = "update-group-u2u"
    static let onlineUser_endPoint = "online-users-u2u"
    static let userCalls_endPoint = "calls_list_u2u"
    static let addGroupMember_endPoint = "add-group-members"
    static let deleteGrupMember_endPoint = "delete-group-members"
    static let suggestedContactList_endPoint = "suggested-contact-list-u2u"
    static let groupList_endPoint = "group-list-u2u"
    static let featuredGroupList_endPoint = "featured-group-list-u2u"
    static let suggestedGroupList_endPoint = "suggested-group-list-u2u"
    static let unFriend_endPoint = "unfriend"
    static let pinned_endPoint = "pined_friend"
    static let unPinned_endPoint = "unpined_friend"
    static let pinnedGroup_endPoint = "pin_chat"
    static let starGroup_endPoint = "star_chat"
    static let sentFriendReq_endPoint = "send-friend-request-list-u2u"
    static let joinGroup_endPoint = "join-group"
    static let like_group_endPoint = "like_group"
    static let dislike_group_endPoint = "dislike_group"
    static let getArchiveList_endPoint = "archive-chats-u2u"
    
    static let uploadVoice_endpoint = "voice-note"
    static let uploadImage_endPoint = "share-photo"
    static let uploaadVideo_endPoint = "share-video"
    
    func hitRequest(method:String,end_point:String,param:[String:String]?,viewcontroller:UIViewController,callback:@escaping (_ json:JSON?) -> Void) {
            
            let urlStr = U2U_ApiManager.base_url + end_point
            
            var request = URLRequest(url: URL(string: urlStr)!)
            request.httpMethod = method
            if let pr = param{
                let keys = Array(pr.keys)
                var postString = ""
                for key in keys {
                    postString = postString + "\(key)=\(pr[key] ?? "")&"
                }
                let postData = postString.data(using: .utf8)
                request.httpBody = postData
            }
            //SVProgressHUD.show(withStatus: "Wait...")
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                }
                if error == nil{
                    if let json = try? JSON(data: data!){
                        callback(json)
                    }else{
                        DispatchQueue.main.async {
                            U2UProxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.Wrong, viewController: viewcontroller) { (isTrue) in
                                if isTrue{
                                    self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        U2UProxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: error!.localizedDescription, viewController: viewcontroller) { (isTrue) in
                            if isTrue{
                                self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                            }
                        }
                    }
                    callback(nil)
                }
            }
            task.resume()
    }
    func hitRequestWithData(method:String,end_point:String,param:[String:String],dataKye:String,data:Data,viewcontroller:UIViewController,callback:@escaping (_ json:JSON?) -> Void) {
            let imgData = data
            let urlStr = U2U_ApiManager.base_url + end_point
            let url = NSURL(string: urlStr)! as URL
            let request: NSMutableURLRequest = NSMutableURLRequest(url:url )
            let boundary = generateBoundaryString()
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpMethod = method
        if dataKye == "voice-recorder-file"{
            let postData = createBodyWithParametersForAudio(parameters: param, filePathKey: dataKye, videoData: data, boundary: boundary)
            request.httpBody = postData
        }else{
            let postData = createBodyWithParameters(param, filePathKey: dataKye, imageDataKey: data, boundary: boundary)
            request.httpBody = postData
        }
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
                DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                }
                if error == nil{
                    if let json = try? JSON(data: data!){
                        callback(json)
                    }else{
                        DispatchQueue.main.async {
                            U2UProxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.Wrong, viewController: viewcontroller) { (isTrue) in
                                if isTrue{
                                    SVProgressHUD.show(withStatus: "Wait...")
                                    self.hitRequestWithData(method: method, end_point: end_point, param: param, dataKye: dataKye, data:  imgData, viewcontroller: viewcontroller, callback: callback)
                                }
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        U2UProxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: error!.localizedDescription, viewController: viewcontroller) { (isTrue) in
                            if isTrue{
                                self.hitRequestWithData(method: method, end_point: end_point, param: param, dataKye: dataKye, data:  imgData, viewcontroller: viewcontroller, callback: callback)
                            }
                        }
                    }
                    callback(nil)
                }
            }
            task.resume()
    }
    func generateBoundaryString() -> String{
        return "Boundary-\(UUID().uuidString)"
    }
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        if imageDataKey.count != 0 {
            let filename = "picture.png"
            let mimetype = "image/jpg"
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imageDataKey)
            body.appendString(string: "\r\n")
        }else{
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"\r\n\r\n")
            let value : String = ""
            body.appendString(string: "\(value)\r\n")
        }
        body.appendString(string: "--\(boundary)--\r\n")
        return body as Data
    }
    func createBodyWithParametersForAudio(parameters: [String: String]?, filePathKey: String?, videoData: Data, boundary: String) -> Data {
        let body = NSMutableData();
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string:"--\(boundary)\r\n")
                body.appendString(string:"Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string:"\(value)\r\n")
            }
        }
        let mimetype = "audio/wav"
        body.appendString(string:"--\(boundary)\r\n")
        body.appendString(string:"Content-Disposition: form-data; name=\"voice-recorder-file\"; filename=\"zhcmessagestemp.wav\"\r\n")
        body.appendString(string:"Content-Type: \(mimetype)\r\n\r\n")
        body.append(videoData)
        body.appendString(string:"\r\n")
        body.appendString(string:"--\(boundary)--\r\n")
        return body as Data
    }
}
