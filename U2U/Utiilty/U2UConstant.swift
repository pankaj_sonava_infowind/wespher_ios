//
//  U2UConstant.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation

let imageCache = NSCache<NSString, AnyObject>()
let KBundleIdentifier = "Sonava.JitsiDemo"
class MessageTypeW {
    static let user_media_img = "user_media_img"
    static let typing = "typing"
    static let typingstop = "typingstop"
    static let callaccepted = "callaccepted"
    static let callrejected = "callrejected"
    static let callsent = "callsent"
    static let callcancelled = "callcancelled"
    static let callended = "callended"
    static let user_media_vid = "user_media_vid"
    static let user_media_file = "user_media_file"
    static let user_media_voice_note = "user_media_voice_note"
}
class U2UClass_identifire {
    static let U2UChatViewController = "U2UChatViewController"
    static let U2UCallViewController = "U2UCallViewController"
    static let U2UChatListCell = "U2UChatListCell"
    static let U2UChatCollectionViewCell = "U2UChatCollectionViewCell"
    static let U2UCallTableViewCell = "U2UCallsTableViewCell"
    static let U2UCallCollectionViewCell = "U2UCallCollectionViewCell"
}
class ResponseData  {
    static let data = "data"
    static let status = "status"
    static let success = "success"
    static let message  = "message"
    static let statusCode = "code"
    static let loginData = "LoginData"
    static let chatNotification = "ChatNotification"
    static let ticketNotification = "TicketNotification"
}
class U2U_Notifications {
    static let Friend_Refresh = "Friend_Refresh"
    static let Request_Refresh = "Request_Refresh"
    static let Contact_Refresh = "Contact_Refresh"
    static let SuggestedContact_Refresh = "SuggestedContact_Refresh"
    static let Group_Refresh = "Group_Refresh"
    static let SuggestedGroup_Refresh = "SuggestedGroup_Refresh"
    static let FeaturedGroup_Refresh = "FeaturedGroup_Refresh"
}
extension UIColor{
    class var dark_header:UIColor {return UIColor(red: 13.0/255.0, green: 135.0/255.0, blue: 174.0/255.0, alpha: 1.0)}
    class var light_header:UIColor {return UIColor(red: 15.0/255.0, green: 169.0/255.0, blue: 189.0/255.0, alpha: 1.0)}
}
extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue), allowLossyConversion: true)
        append(data!)
    }
}
class MSG_Type {
    static let message = "1"
    static let isFalse = "0"
    static let isEmptyValue = ""
    static let callsent = "callsent"
    static let callDial = "30"
    static let callAccept = "31"
    static let callCancel = "33"
    static let callReject = "32"
    static let callEnd = "34"
    static let callCanceled = "callcancelled"
    static let callAccepted = "callaccepted"
    static let callRejected = "callrejected"
    static let callEnded = "callended"
    static let blank = "blank"
}
class Room_Type {
    static let rmtype = "1"
}
class Socket_Constant {
    static let voice_note = "user_media_voice_note"
    static let usermsg = "usermsg"
    static let qaid = "qaid"
    static let msgtype = "msgtype"
    static let name = "name"
    static let message = "message"
    static let iduser = "iduser"
    static let room = "room"
    static let rmtype = "rmtype"
    static let time = "time"
    static let token = "token"
    static let local_id = "local_id"
    static let quoted_id = "quoted_id"
    static let share_media = "share_media"
    static let file_names = "file_names"
    static let file = "file"
    static let username = "username"
    static let type = "type"
    static let k = "k"
    static let dept = "dept"
    static let media_img = "user_media_img"
    static let media_vid = "user_media_vid"
    static let media_file = "user_media_file"
}
extension Date {
    
    var europeanFormattedEn_US : String {
        Formatter.date.calendar = Calendar(identifier: .iso8601)
        Formatter.date.locale   = Locale(identifier: "en_US_POSIX")
        Formatter.date.timeZone = .current
        Formatter.date.dateFormat = "dd/M/yyyy, H:mm"
        return Formatter.date.string(from: self)
    }
    
    func convertToLocalTime(fromTimeZone timeZoneAbbreviation: String) -> Date? {
        if let timeZone = TimeZone(abbreviation: timeZoneAbbreviation) {
            let targetOffset = TimeInterval(timeZone.secondsFromGMT(for: self))
            let localOffeset = TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT(for: self))
            return self.addingTimeInterval(targetOffset - localOffeset)
        }
        return nil
    }
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
}
class DateFormateType {
    static let yyyy_MM_dd = "yyyy-MM-dd"
    static let Today = "Today"
    static let MMM_d = "MMM d"
    static let yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
    static let HH_mm = "HH:mm"
    static let MMMM_dd_yyyy = "MMMM dd, yyyy"
    static let yyyy_MM_dd_T_HH_mm_ssZ = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let MMM_dd_yyyy = "MMM dd,yyyy"
}
class FileDirectoryConstant {
    static let directoryName = "WespherCall"
    static let output = "output"
}
extension Formatter {
    static let date = DateFormatter()
}
extension UITableViewCell {
    
    func UTCToLocalW(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormateType.yyyy_MM_dd_HH_mm_ss
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = DateFormateType.HH_mm
        
        return dateFormatter.string(from: dt!)
    }
    
    func getTimeW(str:String) -> String {
        let date = Date(timeIntervalSince1970: Double(str)!)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = DateFormateType.HH_mm
        let dateMainStr = dateFormatter.string(from: Date())
        let splitMainDate = dateMainStr.split(separator: " ")
        let strDate = dateFormatter.string(from: date)
        let splitAPIDate = strDate.split(separator: " ")
        
        let firstDateCom = "\(splitMainDate[0])"
        let secDateCom = "\(splitAPIDate[0])"
        
        if firstDateCom == secDateCom {
            dateFormatter.calendar = NSCalendar.current
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = DateFormateType.HH_mm
            let strDate = dateFormatter.string(from: date)
            let strReturn = "\(strDate)"
            return strReturn
        }
        return strDate
    }
    
    func convertStringForDateW(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormateType.yyyy_MM_dd
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.dateFormat = DateFormateType.MMMM_dd_yyyy
        
        return dateFormatter.string(from: dt!)
    }
    
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: 12pt !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: .utf8) else { return NSAttributedString() }
            
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(NSAttributedString.self)
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func chopPrefix(_ count: Int = 1) -> String {
        return substring(from: index(startIndex, offsetBy: count))
    }
    
    func chopSuffix(_ count: Int = 1) -> String {
        return substring(to: index(endIndex, offsetBy: -count))
    }
    
    var date: Date? {
        return Formatter.date.date(from: self)
    }
}
extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(_ urlString: String) {
        self.image = nil
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                return
            }
            DispatchQueue.main.async(execute: {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                    
                    self.image = downloadedImage
                }
            })
        }).resume()
    }
}
extension UIButton {
    
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
extension UILabel {
    
    func addImage(imageName: String) {
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        let myString:NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
        myString.append(attachmentString)
        
        self.attributedText = myString
    }
}
class ParamNameW {
    static let share_photo_token = "share-photo-token"
    static let share_photo_userid = "share-photo-userid"
    static let share_photo_username = "share-photo-username"
    static let share_photo_room = "share-photo-room"
    static let timestamp = "timestamp"
    static let voice_recorder_room = "voice-recorder-room"
    static let voice_recorder_username = "voice-recorder-username"
    static let voice_recorder_userid = "voice-recorder-userid"
    static let voice_recorder_token = "voice-recorder-token"
    static let share_photo = "share-photo[]"
    static let user_id = "user_id"
    static let token = "token"
    static let email = "email"
    static let provider_id = "provider_id"
    static let help_topic_id = "help_topic_id"
    static let department_id = "department_id"
    static let subject = "subject"
    static let name = "name"
    static let issue_summury = "issue_summury"
    static let priority_id = "priority_id"
    static let share_file_token = "share-file-token"
    static let share_file_userid = "share-file-userid"
    static let share_file_username = "share-file-username"
    static let share_file_room = "share-file-room"
    static let share_video_token = "share-video-token"
    static let share_video_userid = "share-video-userid"
    static let share_video_username = "share-video-username"
    static let share_video_room = "share-video-room"
    static let unread_count = "unread_count"
    static let staffmid = "staffmid"
    static let last_message = "last_message"
    static let is_deleted = "is_deleted"
    static let file_name = "file_name"
    static let from_id = "from_id"
    static let from_name = "from_name"
    static let isread = "isread"
    static let message = "message"
    static let time = "time"
    static let address = "address"
    static let login_token = "login_token"
    static let gender = "gender"
    static let date_of_birth = "date_of_birth"
    static let country = "country"
    static let profile_pic = "profile_pic"
    static let user_email = "user_email"
    static let username = "username"
    static let w_id = "w_id"
    static let isonline = "isonline"
    static let avatar_image = "avatar_image"
    static let id = "id"
    static let ID = "ID"
    static let category_id = "category_id"
    static let cluster_type = "cluster_type"
    static let creator_type = "creator_type"
    static let creator_uid = "creator_uid"
    static let title = "title"
    static let description = "description"
    static let alias = "alias"
    static let state = "state"
    static let featured = "featured"
    static let created = "created"
    static let hits = "hits"
    static let type = "type"
    static let key = "key"
    static let notification = "notification"
    static let parent_id = "parent_id"
    static let parent_type = "parent_type"
    static let longitude = "longitude"
    static let latitude = "latitude"
    static let cover_image = "cover_image"
    static let is_feature_provider = "is_feature_provider"
    static let is_liked = "is_liked"
    static let is_open = "is_open"
    static let subtitle = "subtitle"
    static let rating = "rating"
    static let total_reviews = "total_reviews"
    static let pinned = "pinned"
    static let firstname = "firstname"
    static let lastname = "lastname"
    static let chatroom = "chatroom"
    static let humano = "humano"
    static let fabid = "fabid"
    static let is_bot = "is_bot"
    static let departmentimage = "departmentimage"
    static let departmentdescription = "departmentdescription"
    static let chatroom_id = "chatroom_id"
    static let departmentname = "departmentname"
    static let login_type = "login_type"
    static let memberid = "memberid"
    static let session_type = "session_type"
    static let user = "user"
    static let providerid = "providerid"
    static let department_hours = "department_hours"
    static let isActive = "isActive"
    static let params = "params"
    static let attachmentdir = "attachmentdir"
    static let id_hash = "id_hash"
    static let chatname = "chatname"
    static let closed = "closed"
    static let departmentid = "departmentid"
    static let duedate = "duedate"
    static let espageid = "espageid"
    static let autoresponce = "autoresponce"
    static let priorityid = "priorityid"
    static let status = "status"
    static let topic = "topic"
    static let update = "update"
    static let chatroom_name = "chatroom_name"
    static let chatroom_created_time = "chatroom_created_time"
    static let chatroom_group_pic = "chatroom_group_pic"
    static let chatroom_owner_id = "chatroom_owner_id"
    static let chatroom_owner_name = "chatroom_owner_name"
    static let message_type = "message_type"
    static let message_is_deleted = "message_is_deleted"
    static let unread_num = "unread_num"
    static let is_read = "is_read"
    static let starred = "starred"
    static let is_online = "is_online"
    static let emailtemplateid = "emailtemplateid"
    static let emailid = "emailid"
    static let autoresponceemailid = "autoresponceemailid"
    static let managerid = "managerid"
    static let departmentsignature = "departmentsignature"
    static let ispublic = "ispublic"
    static let isdefault = "isdefault"
    static let overdueinterval = "overdueinterval"
    static let sendemail = "sendemail"
    static let ticketautoresponce = "ticketautoresponce"
    static let messageautoresponce = "messageautoresponce"
    static let canappendsignature = "canappendsignature"
    static let overduetypeid = "overduetypeid"
    static let priority = "priority"
    static let prioritycolour = "prioritycolour"
    static let priorityurgency = "priorityurgency"
    static let timeFrom = "timeFrom"
    static let timeTill = "timeTill"
    static let mime = "mime"
    static let is_pined = "is_pined"
    static let callstatus = "callstatus"
    static let room_id = "room_id"
    static let calltype = "calltype"
    static let friend_name = "friend_name"
    static let friend_username = "friend_username"
    static let friend_id = "friend_id"
    static let provider_token = "provider_token"
    static let business_phone = "business_phone"
    static let business_url = "business_url"
    static let business_address = "business_address"
    static let address1 = "address1"
    static let address2 = "address2"
    static let city = "city"
    static let zip = "zip"
    static let lastMsgId = "lastMsgId"
    static let userid = "userid"
    static let msg_id = "msg_id"
    static let user_media_music = "user_media_music"
    static let chat_id = "chat_id"
    static let messages = "messages"
    static let chat_name = "chat_name"
    static let qatype = "qatype"
    static let sendpremadeid = "sendpremadeid"
    static let users = "users"
    static let my_providers = "my_providers"
    static let suggested = "suggested"
    static let date = "date"
    static let quoted_msg = "quoted_msg"
    static let quoted_uname = "quoted_uname"
    static let quoted_id = "quoted_id"
    static let quoted_msgTime = "quoted_msgTime"
}
