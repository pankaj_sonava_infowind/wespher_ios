//
//  U2UProxy.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit

class U2UProxy {
    static let shared = U2UProxy()
    private init(){}

    var u2uUser:U2UUser!

    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    // Get random string
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    // MAKE GRADIENT LAYER
    func createGradient(colors:[CGColor],frame:CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        return gradient
    }
    
    // Register table view cell
    func registerTableViewCell(identifier:String,tableView:UITableView)  {
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
    }
    // Get Current Date String
    func getDateStr(formate:String,date:Date) -> String {
        Date_Formatter.dateFormat = formate
        return Date_Formatter.string(from: date)
    }
    // Email validate
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Show alert
    func showOnlyAlert(title:String = Alert_Msg.Opps,message:String,viewController:UIViewController)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Alert_Msg.Ok, style: .default, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlertOneAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,message:String,viewController:UIViewController,callback:@escaping () -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    func showAlertTwoAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,cancelTitle:String=Alert_Msg.Cancel,message:String,viewController:UIViewController,callback:@escaping (_ isOkay:Bool) -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback(true)
        }
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
            callback(false)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}
