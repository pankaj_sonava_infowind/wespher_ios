//
//  EditSuggestedProviderViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class EditSuggestedGroupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var btn_like: UIButton!
    @IBOutlet weak var bottom_lastMessage: NSLayoutConstraint!
    @IBOutlet weak var bottom_populrity: NSLayoutConstraint!
    @IBOutlet weak var height_popularity: NSLayoutConstraint!
    @IBOutlet weak var height_lastMsg: NSLayoutConstraint!
    @IBOutlet weak var btn_popularity: UIButton!
    @IBOutlet weak var btn_lastMessage: UIButton!
    @IBOutlet weak var btn_joined: UIButton!
    @IBOutlet weak var btn_name: UIButton!
    @IBOutlet weak var btn_selectAll: UIButton!
    @IBOutlet weak var view_edit: UIView!
    @IBOutlet weak var lbl_ALL: UILabel!
    @IBOutlet weak var lbl_nogroup: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var lbl_numberOfGroup: UILabel!
    @IBOutlet weak var tblViewEditSuggedtedGroup: UITableView!
    @IBOutlet weak var imgSelectUnselectAll: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var lblTitleHeader: UILabel!

    var selectedGroups = [String]()
    var arrFilter:[Int] = []
    var idEdit:Int = 2
    var u2uHomeVM = U2UHomeViewModel()
    var groupsData =  [Group]()
    var u2uEditGroupVM = U2UEditGroupViewModel()
    var delegate:EditFriend?
    
    @IBOutlet weak var loader_group: UIActivityIndicatorView!
    
    override func viewDidLoad() {
         super.viewDidLoad()
        viewSearch.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        if idEdit == 1{
            lblTitleHeader.text = "Edit Featured Groups"
            groupsData = u2uHomeVM.featuredGroups
        }
        if idEdit == 0{
            lblTitleHeader.text = "Edit Groups"
            groupsData = u2uHomeVM.groups
        }else{
            groupsData = u2uHomeVM.suggestedGroups
            view_edit.isHidden = true
            btn_selectAll.isHidden = true
            lbl_ALL.isHidden = true
            imgSelectUnselectAll.isHidden = true
        }
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfGroup.text = "\(groupsData.count) groups"
        tblViewEditSuggedtedGroup.tableFooterView = UIView()
    }
    var isFilter = false
    @IBOutlet weak var leading_filter: NSLayoutConstraint!
    @IBOutlet weak var view_filter: UIView!
    @IBAction func sortBtnAction(_ sender: UIButton) {
        isFilter = false
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 32
        height_popularity.constant = 32
        bottom_populrity.constant = 8
        bottom_lastMessage.constant = 8
        btn_name.setTitle("Name", for: .normal)
        btn_joined.setTitle("Date joined", for: .normal)
    }
    @IBAction func filterBtnAction(_ sender: UIButton) {
        isFilter = true
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 0
        height_popularity.constant = 0
        bottom_populrity.constant = 0
        bottom_lastMessage.constant = 0
        btn_name.setTitle("CREATED", for: .normal)
        btn_joined.setTitle("JOINED", for: .normal)
    }
    @IBAction func filterOptionAction(_ sender: UIButton) {
        view_filter.isHidden = true
        if isFilter{
            switch sender.tag {
            case 1:
                u2uHomeVM.filter = "created"
            case 2:
                u2uHomeVM.filter = "joined"
            default:
                u2uHomeVM.filter = ""
            }
        }else{
            switch sender.tag {
            case 1:
                u2uHomeVM.sorting = "group_name"
            case 2:
                u2uHomeVM.sorting = "joined_date"
            case 3:
                u2uHomeVM.sorting = "last_message"
            case 4:
                u2uHomeVM.sorting = "popularity"
            default:
                u2uHomeVM.sorting = ""
            }
        }
        refreshData(search: txt_search.text!)
    }
    func getMoreGroups(page:Int) {
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.lbl_numberOfGroup.text = "\(self.u2uHomeVM.groups.count) groups"
                self.groupsData = self.u2uHomeVM.groups
                self.tblViewEditSuggedtedGroup.reloadData()
                if self.u2uHomeVM.groups.count == 0{
                    self.lbl_nogroup.isHidden = false
                }else{
                    self.lbl_nogroup.isHidden = true
                }
            }
        }
    }
    @IBAction func pinBtnAction(_ sender: UIButton) {
        if selectedGroups.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select group", viewController: self)
            return
        }
        var unPinnedArray = [String]()
        var pinnedArray = [String]()
        
        for i in 0..<groupsData.count {
            let frnd = groupsData[i]
            for j in  0..<selectedGroups.count {
                if selectedGroups[j] == frnd.chatroom_id {
                    if frnd.pinned == "1"{
                        pinnedArray.append(frnd.chatroom_id)
                    }else{
                        unPinnedArray.append(frnd.chatroom_id)
                    }
                    break
                }
            }
        }
        selectedGroups.removeAll()
        if unPinnedArray.count > 0{
            //let ids = U2UProxy.shared.json(from: unPinnedArray) ?? ""
            let ids = unPinnedArray.joined(separator: ",")
            u2uEditGroupVM.pinnedRequest(viewController: self, chat_id: ids) {
                if pinnedArray.count > 0{
                    let pin_ids = pinnedArray.joined(separator: ",")
                    self.u2uEditGroupVM.pinnedRequest(viewController: self, chat_id: pin_ids) {
                        self.refreshData(search: self.txt_search.text!)
                    }
                }else{
                    self.refreshData(search: self.txt_search.text!)
                    self.delegate?.updateFriendList()
                }
            }
        }else{
            if pinnedArray.count > 0{
                let pin_ids = pinnedArray.joined(separator: ",")
                //let pin_ids = U2UProxy.shared.json(from: pinnedArray) ?? ""
                self.u2uEditGroupVM.pinnedRequest(viewController: self, chat_id: pin_ids) {
                    self.refreshData(search: self.txt_search.text!)
                }
            }
        }
    }
    func getMoreFeaturedGroups(page:Int) {
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getfeaturedGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.lbl_numberOfGroup.text = "\(self.u2uHomeVM.featuredGroups.count) groups"
                self.groupsData = self.u2uHomeVM.featuredGroups
                self.tblViewEditSuggedtedGroup.reloadData()
                if self.u2uHomeVM.featuredGroups.count == 0{
                    self.lbl_nogroup.isHidden = false
                }else{
                    self.lbl_nogroup.isHidden = true
                }
            }
        }
    }
    func getMoreSuggestedGroups(page:Int) {
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getSuggestedGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.lbl_numberOfGroup.text = "\(self.u2uHomeVM.suggestedGroups.count) groups"
                self.groupsData = self.u2uHomeVM.suggestedGroups
                self.tblViewEditSuggedtedGroup.reloadData()
                if self.u2uHomeVM.suggestedGroups.count == 0{
                    self.lbl_nogroup.isHidden = false
                }else{
                    self.lbl_nogroup.isHidden = true
                }
            }
        }
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        lblTitleHeader.isHidden = true
        lbl_numberOfGroup.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSelectUnselectAll(_ sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
            imgSelectUnselectAll.image = UIImage(named: "unselected")
            selectedGroups.removeAll()
        }else{
            sender.isSelected = true
            imgSelectUnselectAll.image = UIImage(named: "allSelected")
            selectedGroups = groupsData.map{$0.chatroom_id}
        }
        tblViewEditSuggedtedGroup.reloadData()
    }
    @IBAction func createNewGroupBtnAction(_ sender: Any) {
        let newGroupVC = storyboard?.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
        newGroupVC.delegate = self
        navigationController?.pushViewController(newGroupVC, animated: true)
    }
    @IBAction func likeBtnAction(_ sender: UIButton) {
        if selectedGroups.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select group", viewController: self)
            return
        }
        var unLikedArray = [String]()
        var likedArray = [String]()
        
        for i in 0..<groupsData.count {
            let frnd = groupsData[i]
            for j in  0..<selectedGroups.count {
                if selectedGroups[j] == frnd.chatroom_id {
                    if frnd.liked == "1"{
                        likedArray.append(frnd.chatroom_id)
                    }else{
                        unLikedArray.append(frnd.chatroom_id)
                    }
                    break
                }
            }
        }
        selectedGroups.removeAll()
        if unLikedArray.count > 0{
            //let ids = U2UProxy.shared.json(from: unPinnedArray) ?? ""
            let ids = unLikedArray.joined(separator: ",")
            u2uEditGroupVM.likeRequest(viewController: self, group_id: ids) {
             if likedArray.count > 0{
                    let pin_ids = likedArray.joined(separator: ",")
                    self.u2uEditGroupVM.dislikeRequest(viewController: self, group_id: pin_ids) {
                        self.refreshData(search: self.txt_search.text!)
                    }
                }else{
                DispatchQueue.main.async {
                    self.refreshData(search: self.txt_search.text!)
                     self.delegate?.updateFriendList()
                }
                }
            }
        }else{
            if likedArray.count > 0{
                let pin_ids = likedArray.joined(separator: ",")
                //let pin_ids = U2UProxy.shared.json(from: pinnedArray) ?? ""
                self.u2uEditGroupVM.dislikeRequest(viewController: self, group_id: pin_ids) {
                    self.refreshData(search: self.txt_search.text!)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tblViewEditSuggedtedGroup.dequeueReusableCell(withIdentifier: "SuggestedGroupCell", for: indexPath) as! GroupTableViewCell
        cell.imgSelectUnselectCell.tag = indexPath.row
        cell.selectedGroups = selectedGroups
        cell.groupDetails = groupsData[indexPath.row]
        cell.delegate = self
        if indexPath.row == groupsData.count-1{
            if idEdit == 0{
                if u2uHomeVM.contact_LoadMore{
                    getMoreGroups(page: u2uHomeVM.page + 1)
                }
            }else if idEdit == 1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFeaturedGroups(page: u2uHomeVM.page + 1)
                }
            }else{
                if u2uHomeVM.contact_LoadMore{
                    getMoreSuggestedGroups(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
}
extension EditSuggestedGroupViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String) {
        DispatchQueue.main.async {
            self.u2uHomeVM.search = search
            self.u2uHomeVM.contact_LoadMore = true
            if self.idEdit == 0{
                self.u2uHomeVM.groups.removeAll()
                self.tblViewEditSuggedtedGroup.reloadData()
                self.getMoreGroups(page: 1)
            }else if self.idEdit == 1{
                self.u2uHomeVM.featuredGroups.removeAll()
                self.tblViewEditSuggedtedGroup.reloadData()
                self.getMoreFeaturedGroups(page: 1)
            }else{
                self.u2uHomeVM.suggestedGroups.removeAll()
                self.tblViewEditSuggedtedGroup.reloadData()
                self.getMoreSuggestedGroups(page: 1)
            }
        }
    }
}
extension EditSuggestedGroupViewController:UpdateGroup{
    func refresh() {
        refreshData(search: txt_search.text!)
    }
}
extension EditSuggestedGroupViewController:GroupAction{
    func joinGroup(group_id: String, index: Int) {
        
    }
    
    func tapOnMessage(grp: Group) {
        
    }
    func selectGroup(chatroom_id: String, index: Int) {
        if selectedGroups.contains(chatroom_id){
            let index = selectedGroups.firstIndex(of: chatroom_id)
            selectedGroups.remove(at: index!)
        }else{
            selectedGroups.append(chatroom_id)
        }
        tblViewEditSuggedtedGroup.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedGroups.count == groupsData.count{
            btn_selectAll.isSelected = true
            imgSelectUnselectAll.image = UIImage(named: "allSelected")
        }else{
            btn_selectAll.isSelected = false
            imgSelectUnselectAll.image = UIImage(named: "unselected")
        }
    }
}
