//
//  MyGroupViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyGroupViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var bottom_lastMessage: NSLayoutConstraint!
    @IBOutlet weak var bottom_populrity: NSLayoutConstraint!
    @IBOutlet weak var height_popularity: NSLayoutConstraint!
    @IBOutlet weak var height_lastMsg: NSLayoutConstraint!
    @IBOutlet weak var btn_popularity: UIButton!
    @IBOutlet weak var btn_lastMessage: UIButton!
    @IBOutlet weak var btn_joined: UIButton!
    @IBOutlet weak var btn_name: UIButton!
    @IBOutlet weak var lbl_noGroup: UILabel!
    @IBOutlet weak var lbl_numberOfGroups: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var tblViewMyGroup: UITableView!
    @IBOutlet weak var collViewMyGroup: UICollectionView!
    @IBOutlet weak var containerViewMyGroup: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    @IBOutlet weak var loader_group: UIActivityIndicatorView!
    var u2uHomeVM = U2UHomeViewModel()
    var selectGroup:Group!
    override func viewDidLoad(){
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfGroups.text = "\(u2uHomeVM.groups.count) groups"
        tblViewMyGroup.tableFooterView = UIView()
    }
    
    @IBAction func searchBtnAction(_ sender: UIButton) {
        lblTitleHeader.isHidden = true
        lbl_numberOfGroups.isHidden = true
        viewSearch.isHidden = false
    }
    var isFilter = false
    @IBOutlet weak var leading_filter: NSLayoutConstraint!
    @IBOutlet weak var view_filter: UIView!
    @IBAction func sortBtnAction(_ sender: UIButton) {
        isFilter = false
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 32
        height_popularity.constant = 32
        bottom_populrity.constant = 8
        bottom_lastMessage.constant = 8
        btn_name.setTitle("Name", for: .normal)
        btn_joined.setTitle("Date joined", for: .normal)
    }
    @IBAction func filterBtnAction(_ sender: UIButton) {
        isFilter = true
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 0
        height_popularity.constant = 0
        bottom_populrity.constant = 0
        bottom_lastMessage.constant = 0
        btn_name.setTitle("CREATED", for: .normal)
        btn_joined.setTitle("JOINED", for: .normal)
    }
    @IBAction func filterOptionAction(_ sender: UIButton) {
        view_filter.isHidden = true
        if isFilter{
            switch sender.tag {
            case 1:
                u2uHomeVM.filter = "created"
            case 2:
                u2uHomeVM.filter = "joined"
            default:
                u2uHomeVM.filter = ""
            }
        }else{
            switch sender.tag {
            case 1:
                u2uHomeVM.sorting = "group_name"
            case 2:
                u2uHomeVM.sorting = "joined_date"
            case 3:
                u2uHomeVM.sorting = "last_message"
            case 4:
                u2uHomeVM.sorting = "popularity"
            default:
                u2uHomeVM.sorting = ""
            }
        }
        refresh()
    }
    var loading = false
    func getMoreGroups(page:Int) {
        loading  = true
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.loading  = false
                self.lbl_numberOfGroups.text = "\(self.u2uHomeVM.groups.count) groups"
//                if self.collViewMyGroup.isHidden{
//                    self.tblViewMyGroup.reloadData()
//                }else{
//                    self.collViewMyGroup.reloadData()
//                }
                self.tblViewMyGroup.reloadData()
                self.collViewMyGroup.reloadData()
                if self.u2uHomeVM.groups.count == 0{
                    self.lbl_noGroup.isHidden = false
                }else{
                    self.lbl_noGroup.isHidden = true
                }
            }
        }
    }
    @IBAction func actionEditMyGroups(_ sender: Any){
        let editSuggestedGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedGroupViewController") as! EditSuggestedGroupViewController
        editSuggestedGroupVC.u2uHomeVM.groups = u2uHomeVM.groups
        editSuggestedGroupVC.idEdit = 0
        editSuggestedGroupVC.delegate = self
        self.navigationController?.pushViewController(editSuggestedGroupVC, animated: true)
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionShowCollViewMyGroup(_ sender: Any){
        collViewMyGroup.reloadData()
        tblViewMyGroup.isHidden = true
        collViewMyGroup.isHidden = false
    }
    @IBAction func actionShowTblViewMyGroup(_ sender: Any){
        tblViewMyGroup.isHidden = false
        collViewMyGroup.isHidden = true
        tblViewMyGroup.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.groups.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewMyGroup.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as! GroupTableViewCell
        cell.groupDetails = u2uHomeVM.groups[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.groups.count-1{
            if !loading{
                if u2uHomeVM.contact_LoadMore{
                    getMoreGroups(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.delegate = self
        providerDetailVC.fromView = "group"
        providerDetailVC.selectedGroup = u2uHomeVM.groups[indexPath.row]
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return u2uHomeVM.groups.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collViewMyGroup.dequeueReusableCell(withReuseIdentifier: "GroupCell", for: indexPath) as! GroupsCollectionViewCell
        cell.groupDetails = u2uHomeVM.groups[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.groups.count-1{
            if !loading{
                if u2uHomeVM.contact_LoadMore{
                    getMoreGroups(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.delegate = self
        providerDetailVC.fromView = "group"
        providerDetailVC.selectedGroup = u2uHomeVM.groups[indexPath.row]
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let yourWidth = collectionView.bounds.width/3.0
        //      //  let yourHeight = yourWidth
        //
        //        return CGSize(width: yourWidth, height: 180)
        let noOfCellsInRow = 3
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension MyGroupViewController:UpdateGroup{
    func refresh() {
        u2uHomeVM.groups.removeAll()
        tblViewMyGroup.reloadData()
        collViewMyGroup.reloadData()
        u2uHomeVM.search = txt_search.text!
        u2uHomeVM.contact_LoadMore = true
        getMoreGroups(page: 1)
    }
}
extension MyGroupViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        u2uHomeVM.groups.removeAll()
        tblViewMyGroup.reloadData()
        collViewMyGroup.reloadData()
        u2uHomeVM.search = textField.text!
        u2uHomeVM.contact_LoadMore = true
        getMoreGroups(page: 1)
    }
}
extension MyGroupViewController:EditFriend{
    func updateFriendList() {
        u2uHomeVM.groups.removeAll()
        DispatchQueue.main.async {
            self.tblViewMyGroup.reloadData()
        }
        u2uHomeVM.search = txt_search.text!
        u2uHomeVM.contact_LoadMore = true
        getMoreGroups(page: 1)
    }
}
extension MyGroupViewController:GroupAction{
    func joinGroup(group_id: String, index: Int) {
        
    }
    
    func selectGroup(chatroom_id: String, index: Int) {
        
    }
    func tapOnMessage(grp: Group) {
        self.selectGroup = grp
        let chat_dic = ["chat_id":selectGroup.chatroom,"chat_name":selectGroup.chatroom_name,"token":U2UProxy.shared.u2uUser.token]
        self.u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.group = self.selectGroup
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension MyGroupViewController:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}

