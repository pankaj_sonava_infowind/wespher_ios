//
//  AddMemberViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 08/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol UpdateMember {
    func memberAdded(ids:[String])
}

class AddMemberViewController: UIViewController {
    @IBOutlet weak var lbl_noFriends: UILabel!
    @IBOutlet weak var loader_friend: UIActivityIndicatorView!
    @IBOutlet weak var tbl_member: UITableView!
    @IBOutlet weak var view_header: UIView!
    
    let u2uHomeVM = U2UHomeViewModel()
    var selectedMember = [String]()
    var delegate:UpdateMember?
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.view_header.layer.insertSublayer(U2UProxy.shared.createGradient(colors: [UIColor.dark_header.cgColor,UIColor.light_header.cgColor], frame: self.view_header.frame), at: 0)
        }
        getMoreFriend(page: 1)
    }
    func getMoreFriend(page:Int) {
        u2uHomeVM.page = page
        loader_friend.startAnimating()
        u2uHomeVM.getUserFriends(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friend.stopAnimating()
                self.tbl_member.reloadData()
                if self.u2uHomeVM.friends.count == 0{
                    self.lbl_noFriends.isHidden = false
                }else{
                    self.lbl_noFriends.isHidden = true
                }
            }
        }
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            //self.delegate?.memberAdded(ids: [U2UProxy.shared.u2uUser.user_id])
        }
    }
    @IBAction func addMemberBtnAction(_ sender: UIButton) {
        selectedMember.append(U2UProxy.shared.u2uUser.user_id)
        self.dismiss(animated: true) {
            self.delegate?.memberAdded(ids: self.selectedMember)
        }
    }
}
extension AddMemberViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.friends.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddMemberTVC") as! AddMemberTVC
        cell.btn_selectUnselect.tag = indexPath.row
        cell.delegate = self
        cell.member = u2uHomeVM.friends[indexPath.row]
        if selectedMember.contains(u2uHomeVM.friends[indexPath.row].j_id) {
            cell.btn_selectUnselect.isSelected = true
        }else{
            cell.btn_selectUnselect.isSelected = false
        }
        if indexPath.row == u2uHomeVM.friends.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreFriend(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
}
extension AddMemberViewController:AddMember{
    func selectMember(index: Int) {
        if selectedMember.contains(u2uHomeVM.friends[index].j_id) {
            let indx = selectedMember.firstIndex(of: u2uHomeVM.friends[index].j_id)
            selectedMember.remove(at: indx!)
        }else{
            selectedMember.append(u2uHomeVM.friends[index].j_id)
        }
        tbl_member.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
    }
}
