//
//  CreateGroupVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 26/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

protocol UpdateGroup {
    func refresh()
}
class CreateGroupVC: UIViewController {

    @IBOutlet weak var txt_groupName: UITextField!
    @IBOutlet weak var top_bottomView: NSLayoutConstraint!
    @IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_browse: UIButton!
    @IBOutlet weak var view_header: UIView!
    @IBOutlet weak var txtView_description: UITextView!
    @IBOutlet weak var lbl_imgSize: UILabel!
    @IBOutlet weak var lbl_groupType: UILabel!
    
    var grpType = ["Open","Closed"]
    var imagePicker = UIImagePickerController()
    var selectedImgData:Data!
    var groupID = "0"
    var delegate:UpdateGroup?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
        imagePicker.delegate = self
    }
    func viewSetUp() {
        DispatchQueue.main.async {
            self.view_header.layer.insertSublayer(U2UProxy.shared.createGradient(colors: [UIColor.dark_header.cgColor,UIColor.light_header.cgColor], frame: self.view_header.frame), at: 0)
            self.btn_browse.layer.insertSublayer(U2UProxy.shared.createGradient(colors: [UIColor.dark_header.cgColor,UIColor.light_header.cgColor], frame: self.btn_browse.frame), at: 0)
            self.btn_submit.layer.insertSublayer(U2UProxy.shared.createGradient(colors: [UIColor.dark_header.cgColor,UIColor.light_header.cgColor], frame: self.btn_submit.frame), at: 0)
        }
        txt_groupName.leftPadding(point: 10)
        txt_groupName.placeHolderColor(text: "Group name", color: UIColor.darkGray)
    }
    func animateBottomView(constant:CGFloat)  {
        UIView.animate(withDuration: 0.3) {
            self.top_bottomView.constant = constant
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func slectGroupTypeAction(_ sender: UIButton) {
        animateBottomView(constant: -359)
    }
    @IBAction func browseImgAction(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Group Image", message: "Browse image from?", preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(galleryAction)
        actionSheet.addAction(cancelAction)
        present(actionSheet, animated: true, completion: nil)
    }
    @IBAction func submitBtnAction(_ sender: Any) {
        //gn,array,userid,page_id
        let validation = isValidUserEntry()
        if !validation.0{
            U2UProxy.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let addMemberVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberViewController") as! AddMemberViewController
        addMemberVC.delegate = self
        self.present(addMemberVC, animated: true, completion: nil)
    }
    func groupCreated()  {
        U2UProxy.shared.showAlertOneAction(title: "Done!", okayTitle: "Okay", message: "Group created successfuly", viewController: self) {
            self.delegate?.refresh()
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func resetBtnAction(_ sender: Any) {
        selectedImgData = nil
        lbl_imgSize.text = ""
        lbl_groupType.text = "Group type"
        txt_groupName.text = ""
        txtView_description.text = "Describe your group" 
   }
    @IBAction func selectDoneAction(_ sender: UIButton) {
        animateBottomView(constant: 12)
        lbl_groupType.text = grpType[picker_view.selectedRow(inComponent: 0)]
        if lbl_groupType.text! == "Open"{
            groupID = "0"
        }else{
            groupID = "1"
        }
    }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
        if txt_groupName.text!.isEmpty{
            return (false,"Please enter group name")
        }
        if lbl_groupType.text! == "Group type" {
            return (false,"Please select group type")
        }
        if txtView_description.text! == "Describe your group" {
            return (false,"Please enter group description")
        }
        if selectedImgData == nil {
            return (false,"Please select group image")
        }
        return (true,"")
    }
}
extension CreateGroupVC:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return grpType.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return grpType[row]
    }
}
extension CreateGroupVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            if let data = img.jpegData(compressionQuality: 0.5){
                self.selectedImgData = data
                let sizeKB = data.count/1024
                lbl_imgSize.text = "Select File (\(sizeKB)KB)"
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
extension CreateGroupVC:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Describe your group"{
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = "Describe your group"
        }
    }
}
extension CreateGroupVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
extension CreateGroupVC:UpdateMember{
    func memberAdded(ids: [String]) {
        let idArray = ids
        let param = ["gn":txt_groupName.text!,
                     "array":U2UProxy.shared.json(from: idArray) ?? "",
                     "userid":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "group_type":groupID,
                     "description":txtView_description.text!]
        SVProgressHUD.show(withStatus: "Creating...")
        U2U_ApiManager.shared.hitRequestWithData(method: HTTP_Method.post, end_point: U2U_ApiManager.createGroup_endPoint, param: param, dataKye: "file", data: selectedImgData, viewcontroller: self) { (result) in
            if let resultDic = result?.dictionary{
                if resultDic["stat"]?.int == 1{
                    DispatchQueue.main.async {
                        self.groupCreated()
                    }
                }else{
                    DispatchQueue.main.async {
                        U2UProxy.shared.showOnlyAlert(message: resultDic["error"]?.string ?? "", viewController: self)
                    }
                }
            }
        }
    }
}
