//
//  GroupViewController.swift
//  VKChatU2U
//
//  Created by mac on 06/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class GroupViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var loader_suggestedGroup: UIActivityIndicatorView!
    @IBOutlet weak var loader_featuredGroup: UIActivityIndicatorView!
    @IBOutlet weak var loader_groups: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noGroup: UILabel!
    @IBOutlet weak var lbl_noFeaturedGroup: UILabel!
    @IBOutlet weak var lbl_noSuggestedGroup: UILabel!
    @IBOutlet weak var groupTblView: UITableView!
    @IBOutlet weak var FeaturedGroupCollectionView: UICollectionView!
    @IBOutlet weak var suggestedGroupCollectionView: UICollectionView!
    
    var u2uHomeVM:U2UHomeViewModel!
    var selectGroup:Group!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parentVC = self.parent as! U2UHomeVC
        u2uHomeVM = parentVC.u2uHomeVM
        getGroup()
        groupTblView.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.Group_Refresh),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.SuggestedGroup_Refresh),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.FeaturedGroup_Refresh),
        object: nil)
        
    }
    @objc func refreseData(notification:NSNotification)  {
        u2uHomeVM.contact_LoadMore = true
        if notification.name.rawValue == U2U_Notifications.Group_Refresh{
            self.u2uHomeVM.groups.removeAll()
            groupTblView.reloadData()
            loader_groups.startAnimating()
            u2uHomeVM.getGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_groups.stopAnimating()
                    if self.u2uHomeVM.groups.count == 0{
                        self.lbl_noGroup.isHidden = false
                    }else{self.lbl_noGroup.isHidden = true}
                    self.groupTblView.reloadData()
                }
            }
        }else if notification.name.rawValue == U2U_Notifications.SuggestedGroup_Refresh{
            self.u2uHomeVM.suggestedGroups.removeAll()
            suggestedGroupCollectionView.reloadData()
            loader_suggestedGroup.startAnimating()
            u2uHomeVM.getSuggestedGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_suggestedGroup.stopAnimating()
                    if self.u2uHomeVM.suggestedGroups.count == 0{
                        self.lbl_noSuggestedGroup.isHidden = false
                    }else{self.lbl_noSuggestedGroup.isHidden = true}
                    self.suggestedGroupCollectionView.reloadData()
                }
            }
            
        }else if notification.name.rawValue == U2U_Notifications.FeaturedGroup_Refresh{
            self.u2uHomeVM.featuredGroups.removeAll()
            FeaturedGroupCollectionView.reloadData()
            loader_featuredGroup.startAnimating()
            u2uHomeVM.getfeaturedGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_featuredGroup.stopAnimating()
                    if self.u2uHomeVM.featuredGroups.count == 0{
                        self.lbl_noFeaturedGroup.isHidden = false
                    }else{self.lbl_noFeaturedGroup.isHidden = true}
                    self.FeaturedGroupCollectionView.reloadData()
                }
            }
        }
    }
    func getGroup() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.groups.count == 0{
            loader_groups.startAnimating()
            u2uHomeVM.getGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_groups.stopAnimating()
                    if self.u2uHomeVM.groups.count == 0{
                        self.lbl_noGroup.isHidden = false
                    }else{self.lbl_noGroup.isHidden = true}
                    self.groupTblView.reloadData()
                    self.getFeaturedGroup()
                }
            }
        }else{
            getFeaturedGroup()
        }
    }
    func getFeaturedGroup() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.featuredGroups.count == 0{
            loader_featuredGroup.startAnimating()
            u2uHomeVM.getfeaturedGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_featuredGroup.stopAnimating()
                    if self.u2uHomeVM.featuredGroups.count == 0{
                        self.lbl_noFeaturedGroup.isHidden = false
                    }else{self.lbl_noFeaturedGroup.isHidden = true}
                    self.FeaturedGroupCollectionView.reloadData()
                    self.getSuggestedGroup()
                }
            }
        }else{
            getSuggestedGroup()
        }
    }
    func getSuggestedGroup() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.suggestedGroups.count == 0{
            loader_suggestedGroup.startAnimating()
            u2uHomeVM.getSuggestedGroups(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_suggestedGroup.stopAnimating()
                    if self.u2uHomeVM.suggestedGroups.count == 0{
                        self.lbl_noSuggestedGroup.isHidden = false
                    }else{self.lbl_noSuggestedGroup.isHidden = true}
                    self.suggestedGroupCollectionView.reloadData()
                }
            }
        }
    }
    @IBAction func actionEditMyGroups(_ sender: Any){
        let editSuggestedGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedGroupViewController") as! EditSuggestedGroupViewController
        editSuggestedGroupVC.u2uHomeVM.groups = u2uHomeVM.groups
        editSuggestedGroupVC.idEdit = 0
        editSuggestedGroupVC.delegate = self
        self.navigationController?.pushViewController(editSuggestedGroupVC, animated: true)
    }
    
    @IBAction func actionEditFeaturedGroups(_ sender: Any){
        let editSuggestedGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedGroupViewController") as! EditSuggestedGroupViewController
        editSuggestedGroupVC.u2uHomeVM.featuredGroups = u2uHomeVM.featuredGroups
        editSuggestedGroupVC.idEdit = 1
        self.navigationController?.pushViewController(editSuggestedGroupVC, animated: true)
    }
    @IBAction func actionSeeAllMyGroup(_ sender: Any){
        let myGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "MyGroupViewController") as! MyGroupViewController
        myGroupVC.u2uHomeVM.groups = u2uHomeVM.groups
        self.navigationController?.pushViewController(myGroupVC, animated: true)
    }
    @IBAction func actionSeeMoreFeaturedGroup(_ sender: Any){
        let featuredGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedGroupViewController") as! FeaturedGroupViewController
        featuredGroupVC.u2uHomeVM.featuredGroups = u2uHomeVM.featuredGroups
        featuredGroupVC.id = 1
        self.navigationController?.pushViewController(featuredGroupVC, animated: true)
    }
    @IBAction func actionSeeMoreSuggestedCollectoinView(_ sender: Any){
        let featuredGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "FeaturedGroupViewController") as! FeaturedGroupViewController
        featuredGroupVC.u2uHomeVM.suggestedGroups = u2uHomeVM.suggestedGroups
        featuredGroupVC.id = 2
        self.navigationController?.pushViewController(featuredGroupVC, animated: true)
    }
    @IBAction func actionEditSuggestedGroup(_ sender: Any){
        let editSuggestedGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedGroupViewController") as! EditSuggestedGroupViewController
        editSuggestedGroupVC.u2uHomeVM.suggestedGroups = u2uHomeVM.suggestedGroups
        editSuggestedGroupVC.idEdit = 2
        self.navigationController?.pushViewController(editSuggestedGroupVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.groups.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = groupTblView.dequeueReusableCell(withIdentifier: "GroupCell", for: indexPath) as! GroupTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.groupDetails = u2uHomeVM.groups[indexPath.row]
        cell.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.selectedGroup = u2uHomeVM.groups[indexPath.row]
        providerDetailVC.delegate = self
        providerDetailVC.fromView = "group"
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.FeaturedGroupCollectionView{
            return u2uHomeVM.featuredGroups.count
        }
        return u2uHomeVM.suggestedGroups.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.FeaturedGroupCollectionView{
            let cell = FeaturedGroupCollectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedGroupCell", for: indexPath) as! GroupsCollectionViewCell
            cell.groupDetails = u2uHomeVM.featuredGroups[indexPath.row]
            return cell
        }
        else{
            let cell = suggestedGroupCollectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedGroupCell", for: indexPath) as! GroupsCollectionViewCell
            cell.groupDetails = u2uHomeVM.suggestedGroups[indexPath.row]
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if collectionView == self.FeaturedGroupCollectionView{
            let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
            providerDetailVC.fromView = "group"
            providerDetailVC.selectedGroup = u2uHomeVM.featuredGroups[indexPath.row]
            self.navigationController?.pushViewController(providerDetailVC, animated: true)
        }
    }
}
extension GroupViewController:EditFriend{
    func updateFriendList() {
        u2uHomeVM.groups.removeAll()
        DispatchQueue.main.async {
            self.groupTblView.reloadData()
            self.getGroup()
        }
    }
}
extension GroupViewController:GroupAction{
    func joinGroup(group_id: String, index: Int) {
        
    }
    
    func selectGroup(chatroom_id: String, index: Int) {
        
    }
    
    func tapOnMessage(grp: Group) {
        self.selectGroup = grp
        let chat_dic = ["chat_id":selectGroup.chatroom,"chat_name":selectGroup.chatroom_name,"token":U2UProxy.shared.u2uUser.token]
        self.u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.group = self.selectGroup
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension GroupViewController:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}
extension GroupViewController:UpdateGroup{
    func refresh() {
        u2uHomeVM.groups.removeAll()
        DispatchQueue.main.async {
            self.groupTblView.reloadData()
            self.getGroup()
        }
    }
}
