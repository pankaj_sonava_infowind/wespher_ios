//
//  FeaturedGroupViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class FeaturedGroupViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var bottom_lastMessage: NSLayoutConstraint!
    @IBOutlet weak var bottom_populrity: NSLayoutConstraint!
    @IBOutlet weak var height_popularity: NSLayoutConstraint!
    @IBOutlet weak var height_lastMsg: NSLayoutConstraint!
    @IBOutlet weak var btn_popularity: UIButton!
    @IBOutlet weak var btn_lastMessage: UIButton!
    @IBOutlet weak var btn_joined: UIButton!
    @IBOutlet weak var btn_name: UIButton!
    
    @IBOutlet weak var view_filter: UIView!
    @IBOutlet weak var lbl_numberOfGroups: UILabel!
    @IBOutlet weak var tblViewFeaturedGroup: UITableView!
    @IBOutlet weak var collViewFeaturedGroup: UICollectionView!
    
    @IBOutlet weak var lblTitleHeader: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var lbl_noGroups: UILabel!
    @IBOutlet weak var loader_group: UIActivityIndicatorView!
    
    var id = 0
    var u2uHomeVM = U2UHomeViewModel()
    var groupsData = [Group]()
    override func viewDidLoad(){
        super.viewDidLoad()
        viewSearch.isHidden = true
        if id == 1{
            lblTitleHeader.text = "Featured Group"
            groupsData = u2uHomeVM.featuredGroups
        }
        else if id == 2{
            lblTitleHeader.text = "Suggedted Group"
            groupsData = u2uHomeVM.suggestedGroups
        }
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfGroups.text = "\(groupsData.count) groups"
        tblViewFeaturedGroup.tableFooterView = UIView()
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        lblTitleHeader.isHidden = true
        lbl_numberOfGroups.isHidden = true
        viewSearch.isHidden = false
    }
    var isFilter = false
    @IBOutlet weak var leading_filter: NSLayoutConstraint!
    @IBAction func sortBtnAction(_ sender: UIButton) {
        isFilter = false
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 32
        height_popularity.constant = 32
        bottom_populrity.constant = 8
        bottom_lastMessage.constant = 8
        btn_name.setTitle("Name", for: .normal)
        btn_joined.setTitle("Date joined", for: .normal)
    }
    @IBAction func filterBtnAction(_ sender: UIButton) {
        isFilter = true
        view_filter.isHidden = false
        leading_filter.constant = sender.frame.origin.x
        height_lastMsg.constant = 0
        height_popularity.constant = 0
        bottom_populrity.constant = 0
        bottom_lastMessage.constant = 0
        btn_name.setTitle("CREATED", for: .normal)
        btn_joined.setTitle("JOINED", for: .normal)
    }
    @IBAction func filterOptionAction(_ sender: UIButton) {
        view_filter.isHidden = true
        if isFilter{
            switch sender.tag {
            case 1:
                u2uHomeVM.filter = "created"
            case 2:
                u2uHomeVM.filter = "joined"
            default:
                u2uHomeVM.filter = ""
            }
        }else{
            switch sender.tag {
            case 1:
                u2uHomeVM.sorting = "group_name"
            case 2:
                u2uHomeVM.sorting = "joined_date"
            case 3:
                u2uHomeVM.sorting = "last_message"
            case 4:
                u2uHomeVM.sorting = "popularity"
            default:
                u2uHomeVM.sorting = ""
            }
        }
        refreshData(search: txt_search.text!)
    }
    func getMoreFeaturedGroups(page:Int) {
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getfeaturedGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.lbl_numberOfGroups.text = "\(self.u2uHomeVM.featuredGroups.count) groups"
                self.groupsData = self.u2uHomeVM.featuredGroups
                self.tblViewFeaturedGroup.reloadData()
                if self.groupsData.count == 0{
                    self.lbl_noGroups.isHidden = false
                }else{
                    self.lbl_noGroups.isHidden = true
                }
            }
        }
    }
    func getMoreSuggestedGroups(page:Int) {
        u2uHomeVM.page = page
        loader_group.startAnimating()
        u2uHomeVM.getSuggestedGroups(viewController: self) {
            DispatchQueue.main.async {
                self.loader_group.stopAnimating()
                self.lbl_numberOfGroups.text = "\(self.u2uHomeVM.suggestedGroups.count) groups"
                self.groupsData = self.u2uHomeVM.suggestedGroups
                self.tblViewFeaturedGroup.reloadData()
                if self.groupsData.count == 0{
                    self.lbl_noGroups.isHidden = false
                }else{
                    self.lbl_noGroups.isHidden = true
                }
            }
        }
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionEditFeaturedGroups(_ sender: Any){
        let editSuggestedGroupVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedGroupViewController") as! EditSuggestedGroupViewController
        editSuggestedGroupVC.u2uHomeVM.featuredGroups = u2uHomeVM.featuredGroups
        editSuggestedGroupVC.idEdit = id
        self.navigationController?.pushViewController(editSuggestedGroupVC, animated: true)
    }
    @IBAction func actionShowFeaturedCollV(_ sender: Any){
        tblViewFeaturedGroup.isHidden = true
        collViewFeaturedGroup.isHidden = false
        collViewFeaturedGroup.reloadData()
    }
    
    @IBAction func actionShowFeaturedTblV(_ sender: Any){
        tblViewFeaturedGroup.isHidden = false
        collViewFeaturedGroup.isHidden = true
        tblViewFeaturedGroup.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewFeaturedGroup.dequeueReusableCell(withIdentifier: "FeaturedGroupCell", for: indexPath) as! GroupTableViewCell
        cell.btn_joinGroup.tag = indexPath.row
        cell.delegate = self
        cell.groupDetails = groupsData[indexPath.row]
        if indexPath.row == groupsData.count - 1{
            if id == 1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFeaturedGroups(page: u2uHomeVM.page + 1)
                }
            }
            if id == 2{
                if u2uHomeVM.contact_LoadMore{
                    getMoreSuggestedGroups(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.fromView = "group"
        providerDetailVC.selectedGroup = groupsData[indexPath.row]
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupsData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collViewFeaturedGroup.dequeueReusableCell(withReuseIdentifier: "FeaturedGroupCell", for: indexPath) as! GroupsCollectionViewCell
        cell.btn_joinGroup.tag = indexPath.row
        cell.delegate = self
        cell.groupDetails = groupsData[indexPath.row]
            return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.selectedGroup = groupsData[indexPath.row]
        providerDetailVC.fromView = "group"
        
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let yourWidth = collectionView.bounds.width/3.0
        //      //  let yourHeight = yourWidth
        //
        //        return CGSize(width: yourWidth, height: 180)
        let noOfCellsInRow = 3
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 180)
    }

}
extension FeaturedGroupViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String)  {
        u2uHomeVM.suggestedGroups.removeAll()
        u2uHomeVM.featuredGroups.removeAll()
        groupsData.removeAll()
        tblViewFeaturedGroup.reloadData()
        collViewFeaturedGroup.reloadData()
        u2uHomeVM.search = search
        u2uHomeVM.contact_LoadMore = true
        if id == 1{
            getMoreFeaturedGroups(page: 1)
        }
        if id == 2{
            getMoreSuggestedGroups(page: 1)
        }
    }
}
extension FeaturedGroupViewController:GroupAction{
    func tapOnMessage(grp: Group) {
        
    }
    func selectGroup(chatroom_id: String, index: Int) {
        
    }
    func joinGroup(group_id: String, index: Int) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "chatid":group_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.joinGroup_endPoint, param: param, viewcontroller: self) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
            }
            if status == 1{
                DispatchQueue.main.async {
                    self.groupsData.remove(at: index)
                    self.tblViewFeaturedGroup.reloadData()
                    self.collViewFeaturedGroup.reloadData()
                    U2UProxy.shared.showOnlyAlert(message: "You have joined succesfully", viewController: self)
                    let nc = NotificationCenter.default
                    nc.post(name: Notification.Name(U2U_Notifications.SuggestedGroup_Refresh), object: nil)
                    nc.post(name: Notification.Name(U2U_Notifications.FeaturedGroup_Refresh), object: nil)
                }
            }
        }
    }
}
