//
//  EditFriendViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

protocol EditFriend {
    func updateFriendList()
}

class EditFriendViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var btn_unfriend: UIButton!
    @IBOutlet weak var btn_pin: UIButton!
    @IBOutlet weak var btn_selectAll: UIButton!
    @IBOutlet weak var loader_friends: UIActivityIndicatorView!
    @IBOutlet weak var lbl_nofriends: UILabel!
    @IBOutlet weak var lbl_numberOfContact: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var tblViewEditFriend: UITableView!
    @IBOutlet weak var imgSelectedUnselectedTblView: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    
    var delegate:EditFriend?
    var u2uHomeVM = U2UHomeViewModel()
    var u2uEditFriendVM = U2UEditFriendViewModel()
    
    var selectedFriends = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfContact.text = "\(u2uHomeVM.friends.count) groups"
        tblViewEditFriend.tableFooterView = UIView()
    }
    func getMoreFriend(page:Int) {
        u2uHomeVM.page = page
        loader_friends.startAnimating()
        u2uHomeVM.getUserFriends(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friends.stopAnimating()
                self.lbl_numberOfContact.text = "\(self.u2uHomeVM.friends.count) contacts"
                self.tblViewEditFriend.reloadData()
                if self.u2uHomeVM.friends.count == 0{
                    self.lbl_nofriends.isHidden = false
                }else{
                    self.lbl_nofriends.isHidden = true
                }
            }
        }
    }
    @IBAction func unfriendBtnAction(_ sender: Any) {
        if selectedFriends.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select friend", viewController: self)
            return
        }
        let ids = U2UProxy.shared.json(from: selectedFriends) ?? ""
        selectedFriends.removeAll()
        u2uEditFriendVM.unFriendRequest(viewController: self, request_id: ids) {
            DispatchQueue.main.async {
                self.refreshData(search: self.txt_search.text!)
                self.delegate?.updateFriendList()
            }
        }
    }
    @IBAction func pinBtnAction(_ sender: Any) {
        if selectedFriends.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select friend", viewController: self)
            return
        }
        var unPinnedArray = [String]()
        var pinnedArray = [String]()
        
        for i in 0..<u2uHomeVM.friends.count {
            let frnd = u2uHomeVM.friends[i]
            for j in  0..<selectedFriends.count {
                if selectedFriends[j] == frnd.id {
                    if frnd.pined == 1{
                        pinnedArray.append(frnd.id)
                    }else{
                        unPinnedArray.append(frnd.id)
                    }
                    break
                }
            }
        }
        selectedFriends.removeAll()
        if unPinnedArray.count > 0{
            let ids = U2UProxy.shared.json(from: unPinnedArray) ?? ""
            u2uEditFriendVM.pinnedRequest(viewController: self, request_id: ids) {
                if pinnedArray.count > 0{
                    let pin_ids = U2UProxy.shared.json(from: pinnedArray) ?? ""
                    self.u2uEditFriendVM.unPinnedRequest(viewController: self, request_id: pin_ids) {
                        self.refreshData(search: self.txt_search.text!)
                    }
                }else{
                    self.refreshData(search: self.txt_search.text!)
                    self.delegate?.updateFriendList()
                }
            }
        }else{
            if pinnedArray.count > 0{
                let pin_ids = U2UProxy.shared.json(from: pinnedArray) ?? ""
                self.u2uEditFriendVM.unPinnedRequest(viewController: self, request_id: pin_ids) {
                    self.refreshData(search: self.txt_search.text!)
                }
            }
        }
    }
    @IBAction func searchBtnClick(_ sender: Any) {
        lbl_title.isHidden = true
        lbl_numberOfContact.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSelectAll(_ sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
            imgSelectedUnselectedTblView.image = UIImage(named: "unselected")
            selectedFriends.removeAll()
        }else{
            sender.isSelected = true
            imgSelectedUnselectedTblView.image = UIImage(named: "allSelected")
            selectedFriends = u2uHomeVM.friends.map{$0.id}
        }
        tblViewEditFriend.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return u2uHomeVM.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblViewEditFriend.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendsTableViewCell
        cell.imgSelectedUnselectedCell.tag = indexPath.row
        cell.selectedFriends = selectedFriends
        cell.friendDetails = u2uHomeVM.friends[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.friends.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreFriend(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
}
extension EditFriendViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String) {
        DispatchQueue.main.async {
            self.u2uHomeVM.friends.removeAll()
            self.tblViewEditFriend.reloadData()
            self.u2uHomeVM.search = search
            self.u2uHomeVM.contact_LoadMore = true
            self.getMoreFriend(page: 1)
        }
    }
}
extension EditFriendViewController:FriendAction{
    func tapOnAudioCall(friend: Friend) {}
    func tapOnVideoCall(friend: Friend) {}
    func tapOnChat(friend: Friend) {}
    
    func slectFriend(firend_id: String, index: Int) {
        if selectedFriends.contains(firend_id){
            let index = selectedFriends.firstIndex(of: firend_id)
            selectedFriends.remove(at: index!)
        }else{
            selectedFriends.append(firend_id)
        }
        tblViewEditFriend.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedFriends.count == u2uHomeVM.friends.count{
            btn_selectAll.isSelected = true
            imgSelectedUnselectedTblView.image = UIImage(named: "allSelected")
        }else{
            btn_selectAll.isSelected = false
            imgSelectedUnselectedTblView.image = UIImage(named: "unselected")
        }
    }
}
