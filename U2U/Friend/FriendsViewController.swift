//
//  FriendsViewController.swift
//  VKChatU2U
//
//  Created by mac on 03/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class FriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var loaer_suggestedContact: UIActivityIndicatorView!
    @IBOutlet weak var loader_contact: UIActivityIndicatorView!
    @IBOutlet weak var loader_friendReq: UIActivityIndicatorView!
    @IBOutlet weak var loader_friends: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noReq: UILabel!
    @IBOutlet weak var collView_friendReq: UICollectionView!
    @IBOutlet weak var lbl_noSuggestedContact: UILabel!
    @IBOutlet weak var lbl_noContact: UILabel!
    @IBOutlet weak var lbl_noFriend: UILabel!
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var contactCollectionView: UICollectionView!
    @IBOutlet weak var SuggestedContactCollectionView: UICollectionView!
    var isAudio = false
    var isMessage = true
    
    var u2uHomeVM:U2UHomeViewModel!
    var chatFriend:Friend?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let parentVC = self.parent as! U2UHomeVC
        u2uHomeVM = parentVC.u2uHomeVM
        getUserFriends()
        tblview.tableFooterView = UIView()
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.Friend_Refresh),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.Request_Refresh),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.Contact_Refresh),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(self.refreseData(notification:)),
        name:NSNotification.Name(rawValue: U2U_Notifications.SuggestedContact_Refresh),
        object: nil)
    }
    @objc func refreseData(notification:NSNotification)  {
        u2uHomeVM.contact_LoadMore = true
        if notification.name.rawValue == U2U_Notifications.Friend_Refresh{
            self.u2uHomeVM.friends.removeAll()
            self.tblview.reloadData()
            loader_friends.startAnimating()
            u2uHomeVM.getUserFriends(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_friends.stopAnimating()
                    if self.u2uHomeVM.friends.count == 0{
                        self.lbl_noFriend.isHidden = false
                    }else{self.lbl_noFriend.isHidden = true}
                    self.tblview.reloadData()
                }
            }
        }else if notification.name.rawValue == U2U_Notifications.Request_Refresh{
            self.u2uHomeVM.friendRequest.removeAll()
            collView_friendReq.reloadData()
            loader_friendReq.startAnimating()
            u2uHomeVM.getUserFriendReq(viewController: self) {
                DispatchQueue.main.async {
                 self.loader_friendReq.stopAnimating()
                    if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noReq.isHidden = false
                    }else{self.lbl_noReq.isHidden = true}
                    self.collView_friendReq.reloadData()
                }
            }
            
        }else if notification.name.rawValue == U2U_Notifications.Contact_Refresh{
            self.u2uHomeVM.contacts.removeAll()
            contactCollectionView.reloadData()
            loader_contact.startAnimating()
            u2uHomeVM.getUserContacts(viewController: self) { () in
                DispatchQueue.main.async {
                    self.loader_contact.stopAnimating()
                    if self.u2uHomeVM.contacts.count == 0{
                        self.lbl_noContact.isHidden = false
                    }else{self.lbl_noContact.isHidden = true}
                    
                    self.contactCollectionView.reloadData()
                }
            }
        }else if notification.name.rawValue == U2U_Notifications.SuggestedContact_Refresh{
            self.u2uHomeVM.suggestedContacts.removeAll()
            SuggestedContactCollectionView.reloadData()
            loaer_suggestedContact.startAnimating()
            u2uHomeVM.getUserSuggestedContacts(viewController: self) { () in
                DispatchQueue.main.async {
                    self.loaer_suggestedContact.stopAnimating()
                    if self.u2uHomeVM.suggestedContacts.count == 0{
                        self.lbl_noSuggestedContact.isHidden = false
                    }else{self.lbl_noSuggestedContact.isHidden = true}
                    
                    self.SuggestedContactCollectionView.reloadData()
                }
            }
        }
    }
    func getUserFriends() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.friends.count == 0{
            loader_friends.startAnimating()
            u2uHomeVM.getUserFriends(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_friends.stopAnimating()
                    if self.u2uHomeVM.friends.count == 0{
                        self.lbl_noFriend.isHidden = false
                    }else{self.lbl_noFriend.isHidden = true}
                    self.tblview.reloadData()
                    self.getUserFriendReq()
                }
            }
        }else{
            getUserFriendReq()
        }
    }
    func getUserFriendReq() {
        u2uHomeVM.contact_LoadMore = true
           if u2uHomeVM.friendRequest.count == 0{
            loader_friendReq.startAnimating()
               u2uHomeVM.getUserFriendReq(viewController: self) {
                   DispatchQueue.main.async {
                    self.loader_friendReq.stopAnimating()
                       if self.u2uHomeVM.friendRequest.count == 0{
                           self.lbl_noReq.isHidden = false
                       }else{self.lbl_noReq.isHidden = true}
                       self.collView_friendReq.reloadData()
                       self.getUserContacts()
                   }
               }
           }else{
               getUserContacts()
           }
       }
    func getUserContacts() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.contacts.count == 0{
            loader_contact.startAnimating()
            u2uHomeVM.getUserContacts(viewController: self) { () in
                DispatchQueue.main.async {
                    self.loader_contact.stopAnimating()
                    if self.u2uHomeVM.contacts.count == 0{
                        self.lbl_noContact.isHidden = false
                    }else{self.lbl_noContact.isHidden = true}
                    
                    self.contactCollectionView.reloadData()
                    self.getSuggestedContact()
                }
            }
        }else{
            getSuggestedContact()
        }
    }
    func getSuggestedContact() {
        u2uHomeVM.contact_LoadMore = true
        if u2uHomeVM.suggestedContacts.count == 0{
            loaer_suggestedContact.startAnimating()
            u2uHomeVM.getUserSuggestedContacts(viewController: self) { () in
                DispatchQueue.main.async {
                    self.loaer_suggestedContact.stopAnimating()
                    if self.u2uHomeVM.suggestedContacts.count == 0{
                        self.lbl_noSuggestedContact.isHidden = false
                    }else{self.lbl_noSuggestedContact.isHidden = true}
                    
                    self.SuggestedContactCollectionView.reloadData()
                }
            }
        }
    }
    @IBAction func actionEditFriend(_ sender: Any){
        let editFriendVCon = self.storyboard?.instantiateViewController(withIdentifier: "EditFriendViewController") as! EditFriendViewController
        editFriendVCon.u2uHomeVM.friends = u2uHomeVM.friends
        editFriendVCon.delegate = self
        self.navigationController?.pushViewController(editFriendVCon, animated: true)
    }
    @IBAction func actionEditContacts(_ sender: Any){
        let editContactsVCon = self.storyboard?.instantiateViewController(withIdentifier: "EditContactViewController") as! EditContactViewController
        editContactsVCon.u2uHomeVM.contacts = u2uHomeVM.contacts
        self.navigationController?.pushViewController(editContactsVCon, animated: true)
    }
    @IBAction func actionEditSuggestedContacts(_ sender: Any){
        let editSuggestedContactsVCon = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedContactViewController") as! EditSuggestedContactViewController
        editSuggestedContactsVCon.u2uHomeVM.suggestedContacts = u2uHomeVM.suggestedContacts
        self.navigationController?.pushViewController(editSuggestedContactsVCon, animated: true)
    }
    @IBAction func actionSeeAllFriends(_ sender: Any){
        let myFriendVCon = self.storyboard?.instantiateViewController(withIdentifier: "MyFriendsViewController") as! MyFriendsViewController
        myFriendVCon.u2uHomeVM.friends = u2uHomeVM.friends
        self.navigationController?.pushViewController(myFriendVCon, animated: true)
    }
    @IBAction func actionSeeMoreContacts(_ sender: Any){
        let myContactVCon = self.storyboard?.instantiateViewController(withIdentifier: "MyContactViewController") as! MyContactViewController
        myContactVCon.u2uHomeVM.contacts = u2uHomeVM.contacts
        self.navigationController?.pushViewController(myContactVCon, animated: true)
    }
    @IBAction func actionSeeMoreSuggestedContacts(_ sender: Any){
        let suggestedContactVCon = self.storyboard?.instantiateViewController(withIdentifier: "SuggestedContactViewController") as! SuggestedContactViewController
        suggestedContactVCon.u2uHomeVM.suggestedContacts = u2uHomeVM.suggestedContacts
        self.navigationController?.pushViewController(suggestedContactVCon, animated: true)
    }
    @IBAction func seeMoreFriendReqBtnAction(_ sender: Any) {
        let friendReqVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendRequestVC") as! FriendRequestVC
        friendReqVC.u2uHomeVM.friendRequest = u2uHomeVM.friendRequest
        friendReqVC.reqDelegate = self
        self.navigationController?.pushViewController(friendReqVC, animated: true)
    }
    @IBAction func editFriendReqBtnAction(_ sender: Any) {
        let friendReqVC = self.storyboard?.instantiateViewController(withIdentifier: "EditFriendRequestVC") as! EditFriendRequestVC
        friendReqVC.u2uHomeVM.friendRequest = u2uHomeVM.friendRequest
        friendReqVC.reqDelegate = self
        self.navigationController?.pushViewController(friendReqVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return u2uHomeVM.friends.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblview.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendsTableViewCell
        cell.friendDetails = u2uHomeVM.friends[indexPath.row]
        cell.delegate = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if collectionView == contactCollectionView{
            return u2uHomeVM.contacts.count
        }else if collectionView == SuggestedContactCollectionView{
            return u2uHomeVM.suggestedContacts.count
        }else{
            return u2uHomeVM.friendRequest.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if collectionView == collView_friendReq{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FriendReqCVC", for: indexPath) as! FriendReqCVC
            cell.firendDetails = u2uHomeVM.friendRequest[indexPath.row]
            cell.btn_acceptReq.tag = indexPath.row
            cell.btn_reject.tag = indexPath.row
            cell.delegate = self
            return cell
        }
        let cell = contactCollectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactsCollectionViewCell
        cell.btn_message.tag = indexPath.row
        cell.btn_friendReq.tag = indexPath.row
        cell.delegate = self
        if collectionView == self.contactCollectionView{
            cell.contactDetals = u2uHomeVM.contacts[indexPath.row]
            cell.btn_friendReq.layer.setValue("Contact", forKey:  "For")
        }
        else {
            cell.contactDetals = u2uHomeVM.suggestedContacts[indexPath.row]
            cell.btn_friendReq.layer.setValue("Suggest", forKey:  "For")
            cell.btn_friendReq.isHidden = true
        }
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
extension FriendsViewController:ModifyContact{
    func tapOnSendRequest(index: Int, contact: Contact, cellFor:String) {
        var newContact = contact
        let req_status = contact.is_request
        if newContact.is_request == 1{
            newContact.is_request = 0
        }else{
            newContact.is_request = 1
        }
        let indexPath = IndexPath(row: index, section: 0)
        if req_status == 1{
            if let id = contact.id{
                u2uHomeVM.rejectFriendRequest(viewController: self, request_id: id) { (status) in
                    DispatchQueue.main.async {
                        if status == "success"{
                            if cellFor == "Contact"{
                                self.u2uHomeVM.contacts[index] = newContact
                                self.contactCollectionView.reloadItems(at: [indexPath])
                            }else{
                                self.u2uHomeVM.suggestedContacts[index] = newContact
                                self.SuggestedContactCollectionView.reloadItems(at: [indexPath])
                            }
                            U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request canceled successfully.", viewController: self)
                        }else{
                            U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                        }
                    }
                }
            }
        }else{
            u2uHomeVM.sendFriendRequest(viewController: self, target_user_id: contact.ID) { status in
                DispatchQueue.main.async {
                    if status == "success"{
                        if cellFor == "Contact"{
                            self.u2uHomeVM.contacts[index] = newContact
                            self.contactCollectionView.reloadItems(at: [indexPath])
                        }else{
                            self.u2uHomeVM.suggestedContacts[index] = newContact
                            self.SuggestedContactCollectionView.reloadItems(at: [indexPath])
                        }
                        U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request sent successfully.", viewController: self)
                    }else{
                        U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                    }
                }
            }
        }
    }
    func tapOnMessageBtn(index: Int, contact: Contact) {
        
    }
}
extension FriendsViewController:RequestModify{
    func cancelRequest(index: Int) {
        
    }
    func acceptRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.acceptFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noReq.isHidden = false
                    }else{self.lbl_noReq.isHidden = true}
                    self.u2uHomeVM.friendRequest.remove(at: index)
                    self.collView_friendReq.reloadData()
                    self.u2uHomeVM.friends.removeAll()
                    self.getUserFriends()
                }
            }
        }
    }
    func rejectRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendRequest.remove(at: index)
                    if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noReq.isHidden = false
                    }else{self.lbl_noReq.isHidden = true}
                    self.collView_friendReq.reloadData()
                }
            }
        }
    }
}
extension FriendsViewController:UpdateRequest{
    func refreshRequest(friend: Friend?) {
        if let frnd = friend{
            u2uHomeVM.friendRequest = u2uHomeVM.friendRequest.filter{$0.id != frnd.id}
            if self.u2uHomeVM.friendRequest.count == 0{
                self.lbl_noReq.isHidden = false
            }else{self.lbl_noReq.isHidden = true}
            self.collView_friendReq.reloadData()
        }
    }
}
extension FriendsViewController:FriendAction{
    func tapOnAudioCall(friend: Friend) {
        isAudio = true
        isMessage = false
        friendAction(friend: friend)
    }
    func tapOnVideoCall(friend: Friend) {
        isAudio = false
        isMessage = false
        friendAction(friend: friend)
    }
    func tapOnChat(friend: Friend) {
        isMessage = true
        friendAction(friend: friend)
    }
    func friendAction(friend: Friend) {
        if friend.chatroom.count > 4{
            let chat_dic = ["chat_id":friend.chatroom,"chat_name":friend.chat_name,"token":U2UProxy.shared.u2uUser.token]
            u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
            self.chatFriend = friend
            self.isSocketAvailable()
        }else{
            createRoomAndNavigate(friend: friend)
        }
    }
    func createRoomAndNavigate(friend:Friend)  {
        u2uHomeVM.createOrGetRoom(viewController: self, recepient_id: friend.j_id) {
            self.chatFriend = friend
            self.isSocketAvailable()
        }
    }
    func isSocketAvailable() {
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func slectFriend(firend_id: String, index: Int) {
        
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.friend = self.chatFriend
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        u2uChatDetailsVC.isAudio = self.isAudio
        u2uChatDetailsVC.isMessage = self.isMessage
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension FriendsViewController:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}
extension FriendsViewController:EditFriend{
    func updateFriendList() {
        u2uHomeVM.friends.removeAll()
        DispatchQueue.main.async {
            self.tblview.reloadData()
            self.getUserFriends()
        }
    }
}
