//
//  EditContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class EditContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_numberOfContact: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var loader_contact: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noContacts: UILabel!
    @IBOutlet weak var tblViewEditContacts: UITableView!
    @IBOutlet weak var imgSelectedUnselectedAll: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    
    var selectedContacts = [String]()
    
    var u2uHomeVM = U2UHomeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfContact.text = "\(u2uHomeVM.contacts.count) contacts"
        tblViewEditContacts.tableFooterView = UIView()
    }
    func getMoreContact(page:Int) {
        u2uHomeVM.page = page
        loader_contact.startAnimating()
        u2uHomeVM.getUserContacts(viewController: self) {
            DispatchQueue.main.async {
                self.loader_contact.stopAnimating()
                self.lbl_numberOfContact.text = "\(self.u2uHomeVM.contacts.count) contacts"
                self.tblViewEditContacts.reloadData()
                if self.u2uHomeVM.contacts.count == 0{
                    self.lbl_noContacts.isHidden = false
                }else{
                    self.lbl_noContacts.isHidden = true
                }
            }
        }
    }
    @IBOutlet weak var btn_selectAll: UIButton!
    @IBAction func sentReqBtnAction(_ sender: UIButton) {
        if selectedContacts.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select contact", viewController: self)
            return
        }
        let ids = selectedContacts.joined(separator: ",")
        u2uHomeVM.sendFriendRequest(viewController: self, target_user_id: ids) { (status) in
            DispatchQueue.main.async {
                if status == "success"{
                    self.refreshData(search: self.txt_search.text!)
                }
            }
        }
    }
    @IBAction func pinBtnAction(_ sender: UIButton) {
        if selectedContacts.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select contact", viewController: self)
            return
        }
        var unPinnedArray = [String]()
        var pinnedArray = [String]()
        
        for i in 0..<u2uHomeVM.contacts.count {
            let frnd = u2uHomeVM.contacts[i]
            for j in  0..<selectedContacts.count {
                if selectedContacts[j] == frnd.ID {
//                    if frnd.pined == 1{
//                        pinnedArray.append(frnd.id)
//                    }else{
//                        unPinnedArray.append(frnd.id)
//                    }
                    break
                }
            }
        }
        //selectedContacts.removeAll()
    }
    @IBAction func notificationBtnAction(_ sender: UIButton) {
    }
    @IBAction func searchBtnACtion(_ sender: Any) {
        lbl_title.isHidden = true
        lbl_numberOfContact.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSelectUnsselectedAll(_ sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
            imgSelectedUnselectedAll.image = UIImage(named: "unselected")
            selectedContacts.removeAll()
        }else{
            sender.isSelected = true
            imgSelectedUnselectedAll.image = UIImage(named: "allSelected")
            selectedContacts = u2uHomeVM.contacts.map{$0.ID}
        }
        tblViewEditContacts.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblViewEditContacts.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! AllChildTableViewCell
        cell.imgSelectUnselectCell.tag = indexPath.row
        cell.selectedContacts = selectedContacts
        cell.contactDetals = u2uHomeVM.contacts[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.contacts.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreContact(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    }
}
extension EditContactViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String)  {
        u2uHomeVM.contacts.removeAll()
        tblViewEditContacts.reloadData()
        u2uHomeVM.search = search
        u2uHomeVM.contact_LoadMore = true
        getMoreContact(page: 1)
    }
}
extension EditContactViewController:ContactSelection{
    func selectContact(contact_id: String, index: Int) {
        if selectedContacts.contains(contact_id){
            let index = selectedContacts.firstIndex(of: contact_id)
            selectedContacts.remove(at: index!)
        }else{
            selectedContacts.append(contact_id)
        }
        tblViewEditContacts.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedContacts.count == u2uHomeVM.contacts.count{
            btn_selectAll.isSelected = true
            imgSelectedUnselectedAll.image = UIImage(named: "allSelected")
        }else{
            btn_selectAll.isSelected = false
            imgSelectedUnselectedAll.image = UIImage(named: "unselected")
        }
    }
}
