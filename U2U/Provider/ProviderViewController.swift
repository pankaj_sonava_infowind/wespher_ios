//
//  ProviderViewController.swift
//  VKChatU2U
//
//  Created by mac on 28/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ProviderViewController: UIViewController{
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    var allCVCR:AllContactViewController?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
    }
    
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionPanGesture(_ sender: Any){
        let velocity = (sender as AnyObject).velocity(in: view)
        print("velocity",velocity)
        if (velocity.x > 0){
            // user dragged towards the right(Left)
            let all_ChildVC = self.storyboard?.instantiateViewController(withIdentifier: "AllC_ChildViewController") as! AllChildViewController
            all_ChildVC.allCVCR = allCVCR
            ViewEmbedder.embed(vc:all_ChildVC, parent: self, container: view)
            allCVCR!.lblContact.backgroundColor = .groupTableViewBackground
            allCVCR!.lblFriend.backgroundColor = .groupTableViewBackground
            allCVCR!.lblAll.backgroundColor = .blue
            allCVCR!.lblProvider.backgroundColor = .groupTableViewBackground
            return
        }
        else{
            // user dragged towards the left(right)
            let contactVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            contactVC.allCVCR = allCVCR
            ViewEmbedder.embed(vc:contactVC, parent: self, container: view)
            
            allCVCR!.lblContact.backgroundColor = .blue
            allCVCR!.lblFriend.backgroundColor = .groupTableViewBackground
            allCVCR!.lblAll.backgroundColor = .groupTableViewBackground
            allCVCR!.lblProvider.backgroundColor = .groupTableViewBackground
            return
        }
    }
}
