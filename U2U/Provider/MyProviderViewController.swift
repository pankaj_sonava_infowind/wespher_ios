//
//  ViewController.swift
//  VKChatU2U
//
//  Created by mac on 27/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class MyProviderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewCollection: UIView!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    var id:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        viewCollection.isHidden = true
        viewTable.isHidden = false
        
        viewSearch.isHidden = true
        
        if id == 1
        {
            lblTitleHeader.text = "Featured Provider"
            viewTable.isHidden = true
            viewCollection.isHidden = false
            
            
            
            	
        }
        else if id == 2
        {
            lblTitleHeader.text = "Suggested Provider"
        }
    }
    @IBAction func actionBack(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFunnel(_ sender: Any)
    {
         viewTable.isHidden = true
    }
    @IBAction func actionFilter(_ sender: Any)
    {
        
    }
    @IBAction func actionShowCollView(_ sender: Any)
    {
        viewTable.isHidden = true
        viewCollection.isHidden = false
    }
    @IBAction func actionShowTblView(_ sender: Any)
    {
        viewCollection.isHidden = true
        viewTable.isHidden = false
    }
    @IBAction func actionEditMyProvider(_ sender: Any)
    {
        let editMyProviderVC = self.storyboard?.instantiateViewController(withIdentifier: "EditMyProviderViewController") as! EditMyProviderViewController
        editMyProviderVC.idEdit = id
        self.navigationController?.pushViewController(editMyProviderVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return RestaurantModel.sharedInstance.arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantTableViewCell
        
        let obj = RestaurantModel.sharedInstance.arr[indexPath.row]
        
        cell.lblRName.text = obj.restaurantName
        cell.lblRType.text = obj.restaurantType
        
        let rStatus = obj.restaurantStatus
        if rStatus == "Close"
        {
            cell.lblRStatus.textColor = .red
            cell.lblRStatus.text = obj.restaurantStatus
        }
        else
        {
            cell.lblRStatus.text = obj.restaurantStatus
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantTableViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.fromView = "provider"
        
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let yourWidth = collectionView.bounds.width/3.0
//      //  let yourHeight = yourWidth
//
//        return CGSize(width: yourWidth, height: 180)
        let noOfCellsInRow = 3
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 180) 
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return RestaurantModel.sharedInstance.arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "R_Collection_Cell", for: indexPath) as! RestaurantCollectionViewCell
        
        let obj = RestaurantModel.sharedInstance.arr[indexPath.row]
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        
        cell.lblRName.text = obj.restaurantName
        cell.lblRType.text = obj.restaurantType
        
        let rStatus = obj.restaurantStatus
        if rStatus == "Close"
        {
            cell.lblRStatus.textColor = .red
            cell.lblRStatus.text = obj.restaurantStatus
        }
        else
        {
            cell.lblRStatus.text = obj.restaurantStatus
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
        providerDetailVC.fromView = "provider"
        
        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }

    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
//    {
//        return CGSize(width: collectionView.frame.width/4, height: collectionView.frame.height/3)
//    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout
//        collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
}


