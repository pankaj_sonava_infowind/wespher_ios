//
//  EditMyProviderViewController.swift
//  VKChatU2U
//
//  Created by mac on 07/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class EditMyProviderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var editProviderTblView: UITableView!
    @IBOutlet weak var imgViewSelectUnselectAll: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    //var arr:[RestaurantModel] = []
    var selectAll = "NO"
    var arrFilter:[Int] = []
    var idEdit:Int?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        viewSearch.isHidden = true
        editProviderTblView.tableFooterView = UIView()
        if idEdit == 1{
            lblTitleHeader.text = "Edit Featured Provider"
        }else if idEdit == 2{
            lblTitleHeader.text = "Edit Suggested Provider"
        }
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSelectAll(_ sender: Any){
        if selectAll == "NO"{
            selectAll = "YES"
            imgViewSelectUnselectAll.image = UIImage(named: "allSelected")
            editProviderTblView.reloadData()
        }else if selectAll == "YES"{
            selectAll = "NO"
            imgViewSelectUnselectAll.image = UIImage(named: "unselected")
            editProviderTblView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return RestaurantModel.sharedInstance.arr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProviderCell") as! EditProviderTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let j = indexPath.row
        
        if let cell = tableView.cellForRow(at: indexPath) as? EditProviderTableViewCell
        {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if cell.isselected == false{
                //cell.accessoryType = .checkmark
                cell.imgViewSelectUnselect.imageView?.image = UIImage(named: "allSelected")
                cell.isselected = true
                arrFilter.append(indexPath.row)
                print(arrFilter.count)
                if arrFilter.count == RestaurantModel.sharedInstance.arr.count{
                    imgViewSelectUnselectAll.image = UIImage(named: "allSelected")
                }
               return
            }
            else if cell.isselected == true{
                imgViewSelectUnselectAll.image = UIImage(named: "unselected")
                //cell.accessoryType = .none
                cell.imgViewSelectUnselect.imageView?.image = UIImage(named: "unselected")
                cell.isselected = false
                //arrFilter.remove(at: indexPath.row)
                print("arrFilter.count",arrFilter.count)
                for i in 0...arrFilter.count-1{
                    print(arrFilter[i],j)
                    if j == arrFilter[i]{
                        arrFilter.remove(at: i)
                        print(arrFilter.count)
                        if arrFilter.count == 0{
                            imgViewSelectUnselectAll.image = UIImage(named: "unselected")
                        }
                        return
                    }
                }
               
            }

        }
        
    }
}
