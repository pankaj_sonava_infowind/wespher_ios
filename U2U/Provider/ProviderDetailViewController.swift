//
//  ProviderDetailViewController.swift
//  VKChatU2U
//
//  Created by mac on 06/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class ProviderDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblrestaurant: UILabel!
    @IBOutlet weak var lblReataurantStatus: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblReviews: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet weak var viewBtns: UIView!
    @IBOutlet weak var viewBtnLbl: UIView!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnGrpPlus: UIButton!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var viewHeaderPetLuvers: UIView!
    @IBOutlet weak var viewHeaderYogedhPub: UIView!
    @IBOutlet weak var lbl_groupName: UILabel!
    
    var fromView:String?
    
    @IBOutlet weak var heightConstantlblAbout: NSLayoutConstraint!
    @IBOutlet weak var heightConstantBtnView: NSLayoutConstraint!
    @IBOutlet weak var heightConstantShareView: NSLayoutConstraint!
    var arrSection:[String] = ["Channels","Audio Channels"]
    var selectedGroup:Group!
    var delegate:UpdateGroup?
    var memberID:[String]!
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()

        lblrestaurant.text = "Indian Restaurant | $$ |"
        lblReataurantStatus.text = "Closed"
        lblReviews.text = "3245 reviews"
        
        //tblView.sectionHeaderHeight = 50
        
        if fromView != nil{
            if fromView == "provider"{
                viewHeaderPetLuvers.isHidden = true
                viewHeaderYogedhPub.isHidden = false
                heightConstantlblAbout.constant = 0
                heightConstantBtnView.constant = 30
                heightConstantShareView.constant = 70
                btnPlus.isHidden = true
                btnGrpPlus.isHidden = true
                lblAbout.isHidden = true
            }
            else{
                viewHeaderPetLuvers.isHidden = false
                viewHeaderYogedhPub.isHidden = true
                lblAbout.text = "About our group"
                heightConstantBtnView.constant = 0
                heightConstantShareView.constant = 0
                viewBtns.isHidden = true
                viewBtnLbl.isHidden = true
                setGroupData()
            }
        }
        // floatRatingView.delegate = self
    }
    func setGroupData() {
        if selectedGroup != nil{
            lbl_groupName.text = selectedGroup.chatroom_name
            imgView.sd_setImage(with: URL(string: selectedGroup.chatroom_group_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblrestaurant.text = selectedGroup.username
            if selectedGroup.is_open == 1{
                lblReataurantStatus.text = "Open"
            }else{
                lblReataurantStatus.text = "Closed"
            }
        }
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addMemberBtnAction(_ sender: UIButton) {
        let addMemberVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMemberViewController") as! AddMemberViewController
        addMemberVC.delegate = self
        if memberID == nil{
            addMemberVC.selectedMember = selectedGroup.chatroom_members.map{$0.user_id}.filter{$0 != U2UProxy.shared.u2uUser.user_id}
        }else{
            addMemberVC.selectedMember = memberID
        }
        self.present(addMemberVC, animated: true, completion: nil)
    }
    @IBAction func actionForword(_ sender: Any){
        lblRate.text = "4.5"
        floatRatingView.rating = 4.5
    }
    @IBAction func actionLike(_ sender: Any){
    }
    @IBAction func actionShare(_ sender: Any){
        
    }
    @IBAction func actionAbout(_ sender: Any) {
    }
    @IBAction func actionMenu(_ sender: Any) {
    }
    @IBAction func actionDirections(_ sender: Any) {
    }
    @IBAction func actionSchedule(_ sender: Any) {
    }
    @IBAction func actionBook(_ sender: Any) {
    }
    func numberOfSections(in tableView: UITableView) -> Int{
        return arrSection.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0{
            return 5
        }
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblView.dequeueReusableCell(withIdentifier: "ChannelCell", for: indexPath) as! ChannelTableViewCell
        if indexPath.section == 0{
            cell.lblChannel.text = "# Channel \(indexPath.row)"
            cell.lblStatus.text = "99"
        }
        else{
            cell.lblChannel.text = "# Audio Channel \(indexPath.row)"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderTableViewCell
        
        headerCell.lblTitle.text = arrSection[section]
        return headerCell
    }
}
extension ProviderDetailViewController:UpdateMember{
    func memberAdded(ids: [String]) {
        memberID = ids
        let param = ["array":U2UProxy.shared.json(from: memberID!) ?? "",
                     "user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "chat_id":selectedGroup.chatroom]
        SVProgressHUD.show(withStatus: "wait...")
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.addGroupMember_endPoint, param: (param as! [String : String]), viewcontroller: self) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    DispatchQueue.main.async {
                        self.delegate?.refresh()
                    }
                }else{
                    DispatchQueue.main.async {
                        U2UProxy.shared.showOnlyAlert(message: dic["message"]?.string ?? "", viewController: self)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: self)
                }
            }
        }
    }
}
class ChannelTableViewCell:UITableViewCell{
    @IBOutlet weak var lblChannel: UILabel!
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewlblStatus: UIView!
}
class HeaderTableViewCell:UITableViewCell{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
