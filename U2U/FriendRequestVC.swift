//
//  FriendRequestVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 26/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol UpdateRequest {
    func refreshRequest(friend:Friend?)
}
class FriendRequestVC: UIViewController {
    
    @IBOutlet weak var height_manage_req: NSLayoutConstraint!
    @IBOutlet weak var height_numberOfReceivedReq: NSLayoutConstraint!
    @IBOutlet weak var lbl_manageSentReq: UILabel!
    @IBOutlet weak var lbl_numberOfReq: UILabel!
    @IBOutlet weak var height_reqTbl: NSLayoutConstraint!
    @IBOutlet weak var tbl_sentReq: UITableView!
    @IBOutlet weak var loader_friendReq: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noRequest: UILabel!
    @IBOutlet weak var tbl_request: UITableView!
    @IBOutlet weak var lbl_contactNumber: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var view_search: UIView!
    @IBOutlet weak var lbl_friendReq: UILabel!
    
    var reqDelegate:UpdateRequest?
    var u2uHomeVM = U2UHomeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_search.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        
        self.adjustTblHeight()
        getMoreFriendSentReq(page: 1)
    }
    func getMoreFriendReq(page:Int) {
        u2uHomeVM.page = page
        loader_friendReq.startAnimating()
        u2uHomeVM.getUserFriendReq(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friendReq.stopAnimating()
                self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                self.tbl_request.reloadData()
                if self.u2uHomeVM.friendRequest.count == 0{
                    //self.lbl_noRequest.isHidden = false
                }else{
                    //self.lbl_noRequest.isHidden = true
                }
                self.adjustTblHeight()
            }
        }
    }
    func getMoreFriendSentReq(page:Int) {
        u2uHomeVM.page = page
        loader_friendReq.startAnimating()
        u2uHomeVM.getUserSentFriendReq(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friendReq.stopAnimating()
                self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendSentRequest.count) request"
                self.tbl_sentReq.reloadData()
                if self.u2uHomeVM.friendSentRequest.count == 0{
                    //self.lbl_noRequest.isHidden = false
                }else{
                    //self.lbl_noRequest.isHidden = true
                }
                //self.adjustTblHeight()
                self.manageEmptyData()
            }
        }
    }
    func manageEmptyData() {
        if u2uHomeVM.friendRequest.count == 0{
            sentReqOnly()
        }
        if u2uHomeVM.friendSentRequest.count == 0{
            receivedReqOnly()
        }
    }
    func adjustTblHeight(){
        var count = 4//self.u2uHomeVM.friendRequest.count
        if self.u2uHomeVM.friendRequest.count < 4 {
            count = self.u2uHomeVM.friendRequest.count
        }
        height_reqTbl.constant = 58 * CGFloat(count)
        lbl_numberOfReq.text = "Respond to your \(u2uHomeVM.friendRequest.count) friend requests"
        lbl_contactNumber.text = "\(u2uHomeVM.friendRequest.count + u2uHomeVM.friendSentRequest.count) request"
        view.layoutIfNeeded()
    }
    @IBAction func editBtnAction(_ sender: UIButton) {
        let friendReqVC = self.storyboard?.instantiateViewController(withIdentifier: "EditFriendRequestVC") as! EditFriendRequestVC
        friendReqVC.u2uHomeVM.friendRequest = u2uHomeVM.friendRequest
        friendReqVC.reqDelegate = self
        self.navigationController?.pushViewController(friendReqVC, animated: true)
    }
    
    @IBOutlet weak var leading_filter: NSLayoutConstraint!
    @IBOutlet weak var view_filter: UIView!
    
    @IBOutlet weak var btn_one: UIButton!
    @IBOutlet weak var btn_two: UIButton!
    @IBOutlet weak var btn_three: UIButton!
    var isFilter = false
    var isReceivedReq = true
    @IBAction func sortBtnAction(_ sender: UIButton) {
        isFilter = false
        leading_filter.constant = sender.frame.origin.x
        btn_one.setTitle("By name", for: .normal)
        btn_two.setTitle("By date", for: .normal)
        btn_three.setTitle("By common friends", for: .normal)
        view_filter.isHidden = false
    }
    @IBAction func filterBtnAction(_ sender: UIButton) {
        isFilter = true
        leading_filter.constant = sender.frame.origin.x
        btn_one.setTitle("Friend requests only", for: .normal)
        btn_two.setTitle("Sent invitations only", for: .normal)
        btn_three.setTitle("Suggestions", for: .normal)
        view_filter.isHidden = false
    }
    @IBAction func filterOptionBtnAction(_ sender: UIButton) {
        view_filter.isHidden = true
        if isFilter{
            switch sender.tag {
            case 1:
                receivedReqOnly()
            case 2:
                sentReqOnly()
            default:
                sugestionOnly()
            }
        }else{
            switch sender.tag {
            case 1:
                u2uHomeVM.sorting = "name"
            case 2:
                u2uHomeVM.sorting = "date"
            default:
                u2uHomeVM.sorting = ""
            }
            applySorting()
        }
    }
    
    func applySorting() {
        refreshData(search: txt_search.text!)
    }
    func receivedReqOnly() {
        isReceivedReq = true
        height_reqTbl.constant += tbl_sentReq.frame.size.height + 20
        height_manage_req.constant = 0
        lbl_numberOfReq.text = "Respond to your \(u2uHomeVM.friendRequest.count) friend requests"
        tbl_request.isHidden = false
        tbl_sentReq.isHidden = true
    }
    func sentReqOnly() {
        isReceivedReq = false
        height_reqTbl.constant = 0
        height_manage_req.constant = 0
        lbl_numberOfReq.text = "Mange to your sent requests"
        tbl_request.isHidden = true
        tbl_sentReq.isHidden = false
    }
    func sugestionOnly()  {
        
    }
    @IBAction func BackBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func searchBtnAction(_ sender: UIButton) {
        lbl_friendReq.isHidden = true
        lbl_contactNumber.isHidden = true
        view_search.isHidden = false
    }
}
 extension FriendRequestVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbl_request{
            return u2uHomeVM.friendRequest.count
        }else{
            return u2uHomeVM.friendSentRequest.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendRequestTVC") as! FriendRequestTVC
        if tableView == tbl_request{
            cell.btn_reject.tag = indexPath.row
            cell.btn_acceptReq.tag = indexPath.row
            cell.firendDetails = u2uHomeVM.friendRequest[indexPath.row]
            cell.delegate = self
            if indexPath.row == u2uHomeVM.friendRequest.count-1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFriendReq(page: u2uHomeVM.page + 1)
                }
            }
        }else{
            cell.btn_cancel.tag = indexPath.row
            cell.firendDetails = u2uHomeVM.friendSentRequest[indexPath.row]
            cell.delegate = self
            if indexPath.row == u2uHomeVM.friendSentRequest.count-1{
                if u2uHomeVM.contact_LoadMore{
                    getMoreFriendSentReq(page: u2uHomeVM.page + 1)
                }
            }
        }
        return cell
    }
 }
 extension FriendRequestVC:UITextFieldDelegate{
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
     }
     func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
     }
    func refreshData(search:String)  {
        u2uHomeVM.search = search
        u2uHomeVM.contact_LoadMore = true
        if isReceivedReq {
            u2uHomeVM.friendRequest.removeAll()
            tbl_request.reloadData()
            getMoreFriendReq(page: 1)
        }else{
            u2uHomeVM.friendSentRequest.removeAll()
            tbl_sentReq.reloadData()
            getMoreFriendSentReq(page: 1)
        }
    }
 }
 extension FriendRequestVC:RequestModify{
    func acceptRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.acceptFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendRequest.remove(at: index)
                   if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noRequest.isHidden = false
                    }else{self.lbl_noRequest.isHidden = true}
                    self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                    self.tbl_request.reloadData()
                    self.reqDelegate?.refreshRequest(friend: req)
                }
            }
        }
    }
    
    func rejectRequest(index: Int) {
        let req = u2uHomeVM.friendRequest[index]
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendRequest.remove(at: index)
                   if self.u2uHomeVM.friendRequest.count == 0{
                        self.lbl_noRequest.isHidden = false
                    }else{self.lbl_noRequest.isHidden = true}
                    self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                    self.tbl_request.reloadData()
                    self.reqDelegate?.refreshRequest(friend: req)
                }
            }
        }
    }
    func cancelRequest(index: Int) {
        let req = u2uHomeVM.friendSentRequest[index]
        u2uHomeVM.rejectFriendRequest(viewController: self, request_id: req.id) { (status) in
            print(status)
            DispatchQueue.main.async {
                if status == "success"{
                    self.u2uHomeVM.friendSentRequest.remove(at: index)
                   if self.u2uHomeVM.friendSentRequest.count == 0{
                        //self.lbl_noRequest.isHidden = false
                    }else{//self.lbl_noRequest.isHidden = true
                    
                    }
                    self.lbl_contactNumber.text = "\(self.u2uHomeVM.friendRequest.count) request"
                    self.tbl_sentReq.reloadData()
                    self.reqDelegate?.refreshRequest(friend: req)
                }
            }
        }
    }
 }
extension FriendRequestVC:UpdateRequest{
    func refreshRequest(friend: Friend?) {
        refreshData(search: txt_search.text!)
    }
}
