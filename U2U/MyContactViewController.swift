//
//  MyContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 08/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
class MyContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var loader_contact: UIActivityIndicatorView!
    
    @IBOutlet weak var lbl_noContact: UILabel!
    @IBOutlet weak var lbl_myContact: UILabel!
    @IBOutlet weak var lbl_contactCount: UILabel!
    @IBOutlet weak var tblViewMyContacts: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txt_search: UITextField!
    
    var u2uHomeVM = U2UHomeViewModel()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_contactCount.text = "\(u2uHomeVM.contacts.count) contacts"
        tblViewMyContacts.tableFooterView = UIView()
}
    func getMoreContact(page:Int) {
        u2uHomeVM.page = page
        loader_contact.startAnimating()
        u2uHomeVM.getUserContacts(viewController: self) {
            DispatchQueue.main.async {
                self.loader_contact.stopAnimating()
                self.lbl_contactCount.text = "\(self.u2uHomeVM.contacts.count) contacts"
                self.tblViewMyContacts.reloadData()
                if self.u2uHomeVM.contacts.count == 0{
                    self.lbl_noContact.isHidden = false
                }else{
                    self.lbl_noContact.isHidden = true
                }
            }
        }
    }
    @IBAction func searchBtnAction(_ sender: UIButton) {
        lbl_myContact.isHidden = true
        lbl_contactCount.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionEditMyContacts(_ sender: Any){
        let editContactsVC = self.storyboard?.instantiateViewController(withIdentifier: "EditContactViewController") as! EditContactViewController
        editContactsVC.u2uHomeVM.contacts = u2uHomeVM.contacts
        self.navigationController?.pushViewController(editContactsVC, animated: true)
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.contacts.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblViewMyContacts.dequeueReusableCell(withIdentifier: "MyContactsCell", for: indexPath) as!     MycontactsTableViewCell
        cell.contactDetals = u2uHomeVM.contacts[indexPath.row]
        cell.btn_addFriend.tag = indexPath.row
        cell.delegate = self
        if indexPath.row == u2uHomeVM.contacts.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreContact(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
}
extension MyContactViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        u2uHomeVM.contacts.removeAll()
        tblViewMyContacts.reloadData()
        u2uHomeVM.search = textField.text!
        u2uHomeVM.contact_LoadMore = true
        getMoreContact(page: 1)
    }
}
extension MyContactViewController:ModifyContact{
    func tapOnSendRequest(index: Int, contact: Contact, cellFor: String) {
        var newContact = contact
        let req_status = contact.is_request
        if newContact.is_request == 1{
            newContact.is_request = 0
        }else{
            newContact.is_request = 1
        }
        let indexPath = IndexPath(row: index, section: 0)
        if req_status == 1{
            if let id = contact.id{
                u2uHomeVM.rejectFriendRequest(viewController: self, request_id: id) { (status) in
                    DispatchQueue.main.async {
                        if status == "success"{
                            self.u2uHomeVM.contacts[index] = newContact
                            self.tblViewMyContacts.reloadRows(at: [indexPath], with: .none)
                            U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request canceled successfully.", viewController: self)
                            let nc = NotificationCenter.default
                            nc.post(name: Notification.Name(U2U_Notifications.Contact_Refresh), object: nil)
                        }else{
                            U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                        }
                    }
                }
            }
        }else{
            u2uHomeVM.sendFriendRequest(viewController: self, target_user_id: contact.ID) { status in
                DispatchQueue.main.async {
                    if status == "success"{
                        self.u2uHomeVM.contacts[index] = newContact
                        self.tblViewMyContacts.reloadRows(at: [indexPath], with: .none)
                        U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request sent successfully.", viewController: self)
                        let nc = NotificationCenter.default
                        nc.post(name: Notification.Name(U2U_Notifications.Contact_Refresh), object: nil)
                    }else{
                        U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                    }
                }
            }
        }
    }
    func tapOnMessageBtn(index: Int, contact: Contact) {
        
    }
}
