//
//  AllC_ChildViewController.swift
//  VKChatU2U
//
//  Created by mac on 28/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit
import Contacts

class AllChildViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,objPass
{
    @IBOutlet weak var imgContact: UIImageView!
    @IBOutlet weak var imgGroupChat: UIImageView!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblGroupChat: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var allCVCR:AllContactViewController?
    //var arrContact:[ContactModel] = []
    var contacts = [CNContact]()
    
    var contact_fetch = 0
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        lblContact.text = "New contact"
        lblGroupChat.text = "New group chat"
    }
    @IBAction func actionPanGesture(_ sender: Any){
        let velocity = (sender as AnyObject).velocity(in: view)
        print("velocity",velocity)
        
        if (velocity.x > 0)
        {
            return
        }
        else
        {
            // user dragged towards the left(right)
            let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderViewController") as! ProviderViewController
            //providerVC.allCVCR = allCVCR
            ViewEmbedder.embed(vc:providerVC, parent: self, container: view)
            
            allCVCR!.lblContact.backgroundColor = .groupTableViewBackground
            allCVCR!.lblFriend.backgroundColor = .groupTableViewBackground
            allCVCR!.lblAll.backgroundColor = .groupTableViewBackground
            allCVCR!.lblProvider.backgroundColor = .blue
            
            return
        }
        
    }
    
    func objectOfAllContVC(obj: AllContactViewController)
    {
        allCVCR = obj
    }
    
    @IBAction func actionContact(_ sender: Any)
    {
        let contactStore = CNContactStore()
        let keys = [CNContactPhoneNumbersKey,CNContactFormatter.descriptorForRequiredKeys(for: .fullName)] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        
        if self.contact_fetch == 0
        {
            do {
                try contactStore.enumerateContacts(with: request)
                {
                    (contact, stop) in
                    
                    self.contacts.append(contact)
                    self.contact_fetch = 1
                }
            }
            catch {
                print("unable to fetch contacts")
            }
        }
        let newContactVC = self.storyboard?.instantiateViewController(withIdentifier: "NewContactViewController") as! NewContactViewController
        newContactVC.all_ChildVCR = self
        self.navigationController?.pushViewController(newContactVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "All_Cell") as! AllChildTableViewCell
        
        
        return cell
    }
  
}
