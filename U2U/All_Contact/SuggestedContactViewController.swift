//
//  SuggestedContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 08/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SuggestedContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var lblnoContact: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var lbl_numberContact: UILabel!
    @IBOutlet weak var tblViewSuggestedContact: UITableView!
    @IBOutlet weak var lbl_suggestedContact: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var loader_Contact: UIActivityIndicatorView!
    
    var u2uHomeVM = U2UHomeViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberContact.text = "\(u2uHomeVM.suggestedContacts.count) contacts"
        tblViewSuggestedContact.tableFooterView?.isHidden = true
    }
    func getMoreSuggestedContact(page:Int) {
        u2uHomeVM.page = page
        loader_Contact.startAnimating()
        u2uHomeVM.getUserSuggestedContacts(viewController: self) {
            DispatchQueue.main.async {
                self.loader_Contact.stopAnimating()
                self.lbl_numberContact.text = "\(self.u2uHomeVM.suggestedContacts.count) contacts"
                self.tblViewSuggestedContact.reloadData()
                if self.u2uHomeVM.suggestedContacts.count == 0{
                    self.lblnoContact.isHidden = false
                }else{
                    self.lblnoContact.isHidden = true
                }
            }
        }
    }
    @IBAction func searchBtnAction(_ sender: Any) {
         lbl_suggestedContact.isHidden = true
         lbl_numberContact.isHidden = true
         viewSearch.isHidden = false
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionEditSuggestedConatacts(_ sender: Any){
        let editSuggestedContactsVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSuggestedContactViewController") as! EditSuggestedContactViewController
        editSuggestedContactsVC.u2uHomeVM.suggestedContacts = u2uHomeVM.suggestedContacts
        self.navigationController?.pushViewController(editSuggestedContactsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.suggestedContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblViewSuggestedContact.dequeueReusableCell(withIdentifier: "MyContactsCell", for: indexPath) as! MycontactsTableViewCell
        cell.contactDetals = u2uHomeVM.suggestedContacts[indexPath.row]
        cell.btn_addFriend.tag = indexPath.row
        cell.delegate = self
        if indexPath.row == u2uHomeVM.suggestedContacts.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreSuggestedContact(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
}
extension SuggestedContactViewController:ModifyContact{
    func tapOnSendRequest(index: Int, contact: Contact, cellFor: String) {
        var newContact = contact
        let req_status = contact.is_request
        if newContact.is_request == 1{
            newContact.is_request = 0
        }else{
            newContact.is_request = 1
        }
        let indexPath = IndexPath(row: index, section: 0)
        if req_status == 1{
            if let id = contact.id{
                u2uHomeVM.rejectFriendRequest(viewController: self, request_id: id) { (status) in
                    DispatchQueue.main.async {
                        if status == "success"{
                            self.u2uHomeVM.suggestedContacts[index] = newContact
                            self.tblViewSuggestedContact.reloadRows(at: [indexPath], with: .none)
                            U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request canceled successfully.", viewController: self)
                        }else{
                            U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                        }
                    }
                }
            }
        }else{
            u2uHomeVM.sendFriendRequest(viewController: self, target_user_id: contact.ID) { status in
                DispatchQueue.main.async {
                    if status == "success"{
                        self.u2uHomeVM.suggestedContacts[index] = newContact
                        self.tblViewSuggestedContact.reloadRows(at: [indexPath], with: .none)
                        U2UProxy.shared.showOnlyAlert(title: "Done!", message: "Request sent successfully.", viewController: self)
                    }else{
                        U2UProxy.shared.showOnlyAlert(message: status, viewController: self)
                    }
                }
            }
        }
    }
    
    func tapOnMessageBtn(index: Int, contact: Contact) {
        
    }
}
extension SuggestedContactViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        u2uHomeVM.suggestedContacts.removeAll()
        tblViewSuggestedContact.reloadData()
        u2uHomeVM.search = textField.text!
        u2uHomeVM.contact_LoadMore = true
        getMoreSuggestedContact(page: 1)
    }
}
