//
//  ContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 30/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController
{
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    var allCVCR:AllContactViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func actionPanGesture(_ sender: Any)
    {
        let velocity = (sender as AnyObject).velocity(in: view)
        print("velocity",velocity)
        
        if (velocity.x > 0)
        {
            // user dragged towards the right(Left)
            let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderViewController") as! ProviderViewController
            providerVC.allCVCR = allCVCR
            ViewEmbedder.embed(vc:providerVC, parent: self, container: view)
            
            allCVCR!.lblContact.backgroundColor = .groupTableViewBackground
            allCVCR!.lblFriend.backgroundColor = .groupTableViewBackground
            allCVCR!.lblAll.backgroundColor = .groupTableViewBackground
            allCVCR!.lblProvider.backgroundColor = .blue
            
            return
        }
        else
        {
            return
        }
    }
    
}
