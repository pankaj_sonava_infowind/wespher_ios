//
//  NewContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 31/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class NewContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var all_ChildVCR:AllChildViewController?
    
    override func viewDidLoad(){
        super.viewDidLoad()
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return all_ChildVCR!.contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "All_Cell") as! AllChildTableViewCell
        
        let firstContact = all_ChildVCR!.contacts[indexPath.row]
        let phoneno = (firstContact.phoneNumbers[0].value ).value(forKey: "digits") as! String
        let name = firstContact.givenName
        
        cell.lblContactName.text = name
        
        return cell
    }
    
}
