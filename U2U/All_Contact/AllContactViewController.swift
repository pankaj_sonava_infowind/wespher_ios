//
//  AllContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 28/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

protocol objPass
{
    func objectOfAllContVC(obj:AllContactViewController)
}

class AllContactViewController: UIViewController
{
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnProvider: UIButton!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnFriend: UIButton!
    @IBOutlet weak var lblAll: UILabel!
    @IBOutlet weak var lblProvider: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblFriend: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet var swipeGesture: UISwipeGestureRecognizer!
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    var delegate:AllChildViewController?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        lblProvider.backgroundColor = .groupTableViewBackground
        lblContact.backgroundColor = .groupTableViewBackground
        lblFriend.backgroundColor = .groupTableViewBackground
        lblAll.backgroundColor = .blue
        
        let allCVC = self.storyboard?.instantiateViewController(withIdentifier: "AllC_ChildViewController") as! AllChildViewController
        allCVC.allCVCR = self
        
        ViewEmbedder.embed(vc: allCVC, parent: self, container: containerView)
    }
    @IBAction func actionSwipeGesture(_ sender: Any)
    {
        let allCVC = self.storyboard?.instantiateViewController(withIdentifier: "AllC_ChildViewController") as! AllChildViewController
        allCVC.allCVCR = self
        // ViewEmbedder.embed(withIdentifier: "ProviderViewController", parent: self, container: containerView)
    }
    @IBAction func actionPanGesture(_ sender: Any)
    {
      // let direction = UIPanGestureRecognizerDirectionUndefined
        let velocity = (sender as AnyObject).velocity(in: containerView)
        print("velocity",velocity)
        
//        CGPoint vel = [rec velocityInView:self.view];
//        if (vel.x > 0)
        
        //let vel = self.view.velo
        
        lblProvider.backgroundColor = .groupTableViewBackground
        lblContact.backgroundColor = .groupTableViewBackground
        lblFriend.backgroundColor = .groupTableViewBackground
        lblAll.backgroundColor = .blue
        //btnAll.currentTitleColor =
        // btnAll.currentTitleColor = UIColor.blue
        containerView.backgroundColor = .white
        let allCVC = self.storyboard?.instantiateViewController(withIdentifier: "AllC_ChildViewController") as! AllChildViewController
        allCVC.allCVCR = self
        
        if delegate != nil
        {
            delegate!.objectOfAllContVC(obj:self)
        }
        
        //ViewEmbedder.embed(withIdentifier: "AllC_ChildViewController", parent: self, container: containerView)
        ViewEmbedder.embed(vc: allCVC, parent: self, container: containerView)
    }
    
    @IBAction func actionAll(_ sender: Any)
    {
        
        lblProvider.backgroundColor = .groupTableViewBackground
        lblContact.backgroundColor = .groupTableViewBackground
        lblFriend.backgroundColor = .groupTableViewBackground
        lblAll.backgroundColor = .blue
        //btnAll.currentTitleColor =
       // btnAll.currentTitleColor = UIColor.blue
        containerView.backgroundColor = .white
        
         let allCVC = self.storyboard?.instantiateViewController(withIdentifier: "AllC_ChildViewController") as! AllChildViewController
        allCVC.allCVCR = self
        
        ViewEmbedder.embed(vc: allCVC, parent: self, container: containerView)
        
    }
    
    @IBAction func actionProviders(_ sender: Any)
    {
        lblContact.backgroundColor = .groupTableViewBackground
        lblFriend.backgroundColor = .groupTableViewBackground
        lblAll.backgroundColor = .groupTableViewBackground
        lblProvider.backgroundColor = .blue
        
        //ViewEmbedder.embed(withIdentifier: "ProviderViewController", parent: self, container: containerView)
        
        let providerVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderViewController") as! ProviderViewController
        providerVC.allCVCR = self
        ViewEmbedder.embed(vc: providerVC, parent: self, container: containerView)
    }

    @IBAction func actionContacts(_ sender: Any)
    {
        lblProvider.backgroundColor = .groupTableViewBackground
        lblContact.backgroundColor = .blue
        lblFriend.backgroundColor = .groupTableViewBackground
        lblAll.backgroundColor = .groupTableViewBackground
       // ViewEmbedder.embed(withIdentifier: "ContactViewController", parent: self, container: containerView)
        
        let contactVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
        contactVC.allCVCR = self
        ViewEmbedder.embed(vc: contactVC, parent: self, container: containerView)
    }
    
    @IBAction func actionFriends(_ sender: Any)
    {
        lblProvider.backgroundColor = .groupTableViewBackground
        lblContact.backgroundColor = .groupTableViewBackground
        lblFriend.backgroundColor = .blue
        lblAll.backgroundColor = .groupTableViewBackground
    }
    
}
