//
//  CommanTVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class CommanTVC: UITableViewCell {

    @IBOutlet weak var img_user: UIImageView!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var userDetails:Friend?{
        didSet{
            img_user.sd_setImage(with: URL(string: userDetails!.profile_pic ?? "")) { (image, error, casch, url) in
                if let imgg = image{
                    DispatchQueue.main.async {
                        self.img_user.image = imgg
                    }
                }else{
                    DispatchQueue.main.async {
                        self.img_user.image = UIImage(named: "User")
                    }
                }
            }
            lbl_name.text = userDetails?.firstname
            lbl_email.text = userDetails?.username
            if userDetails?.online == "1"{
                lbl_status.text = "| Online"
            }else{
                lbl_status.text = "| Offline"
            }
            
        }
    }
    
}
