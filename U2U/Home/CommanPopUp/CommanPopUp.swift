//
//  CommanPopUp.swift
//  RockStarDaddy
//
//  Created by mac on 25/09/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

protocol CommanPopUpDelegate {
    func selectUser(user:Friend,type:String)
}

class CommanPopUp: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet var contentView           : UIView!
    @IBOutlet var tblDepartment         : UITableView!
    
    //MARK:- variable
    var delegate : CommanPopUpDelegate?
    var u2uHomeVM:U2UHomeViewModel!
    //MARK:- Override methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setUpView() {
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("CommanPopUp", owner: self, options: nil)
        //Bundle.main.loadNibNamed("AudioCallPopUp", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    func loadView() {
        tblDepartment.register(UINib(nibName: "CommanTVC", bundle: Bundle(identifier: KBundleIdentifier)), forCellReuseIdentifier: "CommanTVC")
        tblDepartment.delegate = self
        tblDepartment.dataSource = self
        tblDepartment.estimatedRowHeight = 200
        tblDepartment.rowHeight = UITableView.automaticDimension
    }
    //MARK:- Action methods
    @IBAction fileprivate func btnActionClose(_ sender:UIButton) {
        self.removeFromSuperview()
    }
    
}

//MARK:- Tableview delegate and datasource methods
extension CommanPopUp : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommanTVC", for: indexPath) as! CommanTVC
        cell.userDetails = u2uHomeVM.friends[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.selectUser(user: u2uHomeVM.friends[indexPath.row], type: lbl_heading.text!)
    }
    
}
