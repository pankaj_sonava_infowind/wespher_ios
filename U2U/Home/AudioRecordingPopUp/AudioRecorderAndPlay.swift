//
//  AudioRecorderAndPlay.swift
//  TestRecording
//
//  Created by mac on 06/08/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation


protocol AudioRecorderProtocol {
    func sendChat(data:Data)
}
class AudioRecorderAndPlay: UIView,AVAudioRecorderDelegate {
    
    //MARK:- Outlets
    @IBOutlet var contentView : UIView!
    @IBOutlet weak var audioRecView : UIView!
    @IBOutlet weak var recordButton : UIButton!
    @IBOutlet weak var crossBtn : UIButton!
    @IBOutlet weak var slideToCancel : UILabel!
    @IBOutlet weak var countDownLabel : UILabel!
    @IBOutlet weak var showAlert : UILabel!
    @IBOutlet weak var viewPlay : UIView!
    @IBOutlet weak var btnPlayAudio : UIButton!
    @IBOutlet fileprivate weak var playbackSlider : UISlider!
    
    //MARK:- Variable
    var delegate : AudioRecorderProtocol?
    
    var timeer: Timer?
    var audioPlayer: AVPlayer?
    var timer:Timer!
    var recordSeconds = 0
    var recordMinutes = 0
    var audioRecorder: AVAudioRecorder?
    var fileName = "audioFile.wav"
    var recordingAnimationDuration = 0.5
    var recordingLabelText = "<<< Slide to cancel"
    var state : RecordViewState = .none {
        didSet {
            if state != .recording{
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.slideToCancel.alpha = 1.0
                    self.countDownLabel.alpha = 1.0
                    self.invalidateIntrinsicContentSize()
                    self.setNeedsLayout()
                    self.layoutIfNeeded()
                })
            }else{
                self.slideToCancel.alpha = 1.0
                self.countDownLabel.alpha = 1.0
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.layoutIfNeeded()
            }
        }
    }
    
    enum RecordViewState {
        case recording
        case none
    }
    
    //MARK:- Variables
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK:- Setup view
    func setUpView() {
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("AudioRecorderAndPlay", owner: self, options: nil)
        //Bundle.main.loadNibNamed("AudioRecorderAndPlay", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.layer.borderColor = UIColor.lightGray.cgColor
        contentView.layer.borderWidth = 0.5
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        self.viewPlay.isHidden = true
        self.crossBtn.isHidden = false
        setupLabel()
        setupCountDownLabel()
        setupRecorder()
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.userDidTapRecord(_:)))
        longPress.cancelsTouchesInView = false
        longPress.allowableMovement = 10
        longPress.minimumPressDuration = 0.2
        recordButton.addGestureRecognizer(longPress)
    }
    
    func setupLabel() {
        slideToCancel.textAlignment = .center
        self.audioRecView.addSubview(slideToCancel)
        self.audioRecView.backgroundColor = UIColor.clear
        slideToCancel.alpha = 0.0
        slideToCancel.font = UIFont.boldSystemFont(ofSize: 14)
        slideToCancel.textAlignment = .center
        slideToCancel.textColor = UIColor.black
    }
    
    func setupCountDownLabel() {
        countDownLabel.textAlignment = .center
        countDownLabel.alpha = 0.0
        countDownLabel.font = UIFont.systemFont(ofSize: 15)
        countDownLabel.textAlignment = .center
        countDownLabel.textColor = UIColor.red
    }
    
    func setupRecorder() {
        let recordSettings = [
            AVFormatIDKey: Int(kAudioFormatLinearPCM),
            AVSampleRateKey: 44100,
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
        ]
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
        } catch{
        }
        do {
            try audioRecorder = AVAudioRecorder(url: getFileURL(),settings: recordSettings)
            audioRecorder?.prepareToRecord()
        } catch{
        }
    }
    
    //MARK:- Get data
    func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        return paths[0]
    }
    
    func getFileURL() -> URL{
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.wav")
        return soundFileURL
    }
    
    //MARK:- Action Methods
    @objc func userDidTapRecord(_ gesture: UIGestureRecognizer) {
        let button = gesture.view as! UIButton
        let location = gesture.location(in: button)
        var startLocation = CGPoint.zero
        switch gesture.state {
        case .began:
            startLocation = location
            userDidBeginRecord(button)
        case .changed:
            let translate = CGPoint(x: location.x - startLocation.x, y: location.y - startLocation.y)
            if !button.bounds.contains(translate) {
                if state == .recording {
                    userDidTapRecordThenSwipe(button)
                }
            }
        case .ended:
            if state == .none { return }
            let translate = CGPoint(x: location.x - startLocation.x, y: location.y - startLocation.y)
            if !button.frame.contains(translate) {
                userDidStopRecording(button)
            }
        case .failed, .possible ,.cancelled : if state == .recording { userDidStopRecording(button) } else { userDidTapRecordThenSwipe(button)}
        @unknown default:
            break
        }
    }
    
    func userDidTapRecordThenSwipe(_ sender: UIButton) {
        slideToCancel.text = nil
        countDownLabel.text = nil
        state = .none
        timer.invalidate()
        audioRecorder?.stop()
        self.showAlert.isHidden = false
        self.crossBtn.isHidden = false
    }
    
    func userDidStopRecording(_ sender: UIButton) {
        if recordSeconds > 0 {
            self.viewPlay.isHidden = false
        }
        slideToCancel.text = nil
        countDownLabel.text = nil
        timer.invalidate()
        audioRecorder?.stop()
    }
    
    func userDidBeginRecord(_ sender : UIButton) {
        state = .recording
        self.showAlert.isHidden = true
        self.crossBtn.isHidden = true
        self.viewPlay.isHidden = true
        slideToCancel.text = self.recordingLabelText
        recordMinutes = 0
        recordSeconds = 0
        slideToCancel.alpha = 1.0
        countDownLabel.alpha = 1.0
        countdown()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countdown) , userInfo: nil, repeats: true)
        self.audioRecorder?.delegate = self
        self.audioRecorder?.record()
    }
    
    @objc func countdown() {
        var seconds = "\(recordSeconds)"
        if recordSeconds < 10 {
            seconds = "0\(recordSeconds)"
        }
        var minutes = "\(recordMinutes)"
        if recordMinutes < 10 {
            minutes = "0\(recordMinutes)"
        }
        countDownLabel.text = "● \(minutes):\(seconds)"
        recordSeconds += 1
        if recordSeconds == 60 {
            recordMinutes += 1
            recordSeconds = 0
        }
    }
    
    @IBAction fileprivate func btnActionSend(_ sender:UIButton) {
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.wav")
        do {
            let imageData = try Data(contentsOf: soundFileURL as URL)
            self.delegate?.sendChat(data: imageData)
        } catch {
        }
    }
    
    @IBAction fileprivate func btnActionPlay(_ sender:UIButton){
        if sender.isSelected == true {
            sender.isSelected = false
            audioPlayer?.pause()
        } else {
            sender.isSelected = true
            self.playbackSlider.value = 0
            let fileMgr = FileManager.default
            
            let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
            let soundFileURL = dirPaths[0].appendingPathComponent("sound.wav")
            let playerItem = AVPlayerItem(url: soundFileURL)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer?.play()
            self.playbackSlider.addTarget(self, action: #selector(self.playbackSliderValueChanged(_:)), for: .valueChanged)
            timeer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime(_ timer: Timer) {
        if audioPlayer == nil {
            self.playbackSlider.value = 0.0
            return
        }
        self.playbackSlider.value = Float(CMTimeGetSeconds((audioPlayer?.currentTime())!)) / Float(CMTimeGetSeconds((self.audioPlayer?.currentItem?.asset.duration)!))
    }
    
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider) {
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        audioPlayer!.seek(to: targetTime)
        if audioPlayer!.rate == 0 {
            audioPlayer?.play()
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    @IBAction func btnActionCloseAudioView(_ sender:UIButton){
        audioPlayer = nil
        var bottomSafeArea:CGFloat = 0
        if #available(iOS 11.0, *) {
            bottomSafeArea = safeAreaInsets.bottom
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.frame = CGRect(x:UIScreen.main.bounds.width, y:UIScreen.main.bounds.height-(50+bottomSafeArea) , width:UIScreen.main.bounds.width , height:50)
        }) { (isSucce) in
            self.removeFromSuperview()
        }
    }
    
}
