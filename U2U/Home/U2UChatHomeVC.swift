//
//  U2UChatHomeVC.swift
//  RockStarDaddy
//
//  Created by mac on 29/08/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//
import UIKit
import Network
import SVProgressHUD

class U2UChatHomeVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet  weak var viewChat :          UIView!
    @IBOutlet  weak var viewCall :          UIView!
    @IBOutlet  weak var viewContainer :     UIView!
    @IBOutlet  weak var lblChat :           UILabel!
    @IBOutlet  weak var lblCall :           UILabel!
    
    //MARK:- Variables
    var lastStoryboardID                    : String?
    let monitor                             = NWPathMonitor()

    var u2uHomeVM:U2UHomeViewModel!
    var isAudio = false
    var isMessage = true
    var chatFriend:Friend?
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*UIFont.jbs_registerFont(
            withFilenameString: "NunitoSans-Regular.ttf",
            bundle: Bundle(identifier: KBundleIdentifier)!
        )*/
        let parentVC = self.parent as! U2UHomeVC
        u2uHomeVM = parentVC.u2uHomeVM
        navigationController?.navigationBar.isHidden = true
        self.prepareLayout()
        
        ViewEmbedder.embed(withIdentifier: self.lastStoryboardID ?? "", parent: self, container: self.viewContainer) { (controller) in
        }
    }
    
    //MARK:- Setup UI's
    fileprivate func prepareLayout() {
        lastStoryboardID = U2UClass_identifire.U2UChatViewController
        self.setUpUI(ticketLblColor: UIColor.white, calllLblColor: UIColor.white, chatLblColor: #colorLiteral(red: 0.2206390798, green: 0.6431847215, blue: 0.8170766234, alpha: 1), ticketViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0), calllViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0), chatViewColor: UIColor.white)
    }
    func setUpUI(ticketLblColor:UIColor,calllLblColor:UIColor,chatLblColor:UIColor,ticketViewColor:UIColor,calllViewColor:UIColor,chatViewColor:UIColor) {
        self.lblCall.textColor = calllLblColor
        self.lblChat.textColor = chatLblColor
        self.viewCall.backgroundColor = calllViewColor
        self.viewChat.backgroundColor = chatViewColor
    }
    //MARK:- Action Methods
    @IBAction func btnActionChatShow(_ sender:UIButton) {
        self.setUpUI(ticketLblColor: UIColor.white, calllLblColor: UIColor.white, chatLblColor: #colorLiteral(red: 0.2206390798, green: 0.6431847215, blue: 0.8170766234, alpha: 1), ticketViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0), calllViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0), chatViewColor: UIColor.white)
        lastStoryboardID = U2UClass_identifire.U2UChatViewController
        ViewEmbedder.embed(withIdentifier: lastStoryboardID!, parent: self, container: self.viewContainer) { (controller) in
        }
    }
    @IBAction func btnActionCallShow(_ sender:UIButton) {
        self.setUpUI(ticketLblColor: UIColor.white, calllLblColor: #colorLiteral(red: 0.2206390798, green: 0.6431847215, blue: 0.8170766234, alpha: 1), chatLblColor: UIColor.white, ticketViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0), calllViewColor: UIColor.white, chatViewColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0))
        lastStoryboardID =  U2UClass_identifire.U2UCallViewController
        ViewEmbedder.embed(withIdentifier: lastStoryboardID!, parent: self, container: self.viewContainer) { (controller) in}
    }
    
    @IBAction fileprivate func btnActionBackToHome(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction fileprivate func btnActionSideMenu(_ sender:UIButton) {
        //AppDelegate.appDelegate.kMainViewController.showLeftView(animated: true, completionHandler: nil)
    }
    
    @IBAction func btnActionOpenProviderDetail(_ sender:UIButton) {
        /*let objProviderVC = self.storyboard?.instantiateViewController(withIdentifier: Class_identifire.ProviderDetailViewController) as! ProviderDetailViewController
        self.navigationController?.pushViewController(objProviderVC, animated: true)*/
    }
    
    @IBAction func btnActionOpenMessagePopUp(_ sender:UIButton){
        addCommonPopView(heading: "Start a chat")
    }
    @IBAction func btnActionOpenVideoPopUp(_ sender:UIButton){
       addCommonPopView(heading: "Start a video call")
    }
    @IBAction func btnActionOpenCallPopUp(_ sender:UIButton){
        addCommonPopView(heading: "Start a call")
    }
    func addCommonPopView(heading:String) {
        let commanPopUp = CommanPopUp.init(frame: parent!.view.bounds)
        commanPopUp.lbl_heading.text = heading
        commanPopUp.u2uHomeVM = u2uHomeVM
        commanPopUp.delegate = self
        commanPopUp.loadView()
        parent?.view.addSubview(commanPopUp)
    }
}
extension U2UChatHomeVC:CommanPopUpDelegate{
    func selectUser(user: Friend, type: String) {
        switch type {
        case "Start a chat":
            chatCalled(friend: user)
        case "Start a call":
            voiceCalled(friend: user)
        default:
            videoCalled(friend: user)
        }
    }
    func chatCalled(friend:Friend)  {
        isMessage = true
        friendAction(friend: friend)
    }
    func voiceCalled(friend:Friend)  {
        isAudio = true
        isMessage = false
        friendAction(friend: friend)
    }
    func videoCalled(friend:Friend)  {
        isAudio = false
        isMessage = false
        friendAction(friend: friend)
    }
    func friendAction(friend: Friend) {
        if friend.chatroom.count > 4{
            let chat_dic = ["chat_id":friend.chatroom,"chat_name":friend.chat_name,"token":U2UProxy.shared.u2uUser.token]
            u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
            self.chatFriend = friend
            self.isSocketAvailable()
        }else{
            createRoomAndNavigate(friend: friend)
        }
    }
    func createRoomAndNavigate(friend:Friend)  {
        u2uHomeVM.createOrGetRoom(viewController: self, recepient_id: friend.j_id) {
            self.chatFriend = friend
            self.isSocketAvailable()
        }
    }
    func isSocketAvailable() {
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.friend = self.chatFriend
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        u2uChatDetailsVC.isAudio = self.isAudio
        u2uChatDetailsVC.isMessage = self.isMessage
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension U2UChatHomeVC:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}
