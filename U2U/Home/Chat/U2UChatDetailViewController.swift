///
//  U2UChatDetailViewController.swift
//  Wespher
//
//  Created by mac on 30/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.

import UIKit
import MobileCoreServices
import AVKit
import Photos
import AVKit
import QuickLook
import AudioToolbox
import JitsiMeet
import Realm
import RealmSwift

extension U2UChatDetailViewController : Chat_Update{
    func localUpdate() {
        tblList.reloadData()
    }
}
class U2UChatDetailViewController: UIViewController,UIPopoverPresentationControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var txtMessage : NextGrowingTextView!
    @IBOutlet weak var tblList : UITableView!
    @IBOutlet weak var heightBottom : NSLayoutConstraint!
    @IBOutlet weak var lblHeader : UILabel!
    @IBOutlet weak var btnMic : UIButton!
    @IBOutlet weak var lblType : UILabel!
    @IBOutlet weak var btnCall : UIButton!
    @IBOutlet weak var imgCall : UIImageView!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var viewHeaderChat: UIView!
    @IBOutlet weak var viewHeaderSelectMessage: UIView!
    @IBOutlet weak var viewQuoteMessage: UIView!
    @IBOutlet weak var lblQuotedMessage: UILabel!
    @IBOutlet weak var lblQuotedMessageDeatail: UILabel!
    
    //MARK:- Variable
    var imagePicker: ImagePicker!
    var audioObj : AudioRecorderAndPlay?
    var arrData : [String]?
    let imgPicker = UIImagePickerController()
    var urlPreviewForFile : NSURL?
    var isCallingMoreThen : Bool? = false
    var pipViewCoordinator: PiPViewCoordinator?
    var jitsiMeetView: JitsiMeetView?
    var isjoined: Bool? = false
    var gameTimer: Timer?
    var timerCallDisconnect: Timer?
    var isSelected:Bool = false
    var arrSelectedCell:[[String:Any]] = []
    var text:String = ""
    var dateAndTime = ""
    var time = ""
    var userName = ""
    var indexpath:IndexPath?
    var arrIdForDelete:[String] = []
    var arrIdSenderDelete:[String] = []
    var arr:[IndexPath] = []
    var vC:String = "chatDetailVC"
    var quotedID = "0"
    var withMessage = ""
    
    // New U2U Variables
    var u2uChatDetailsVM = U2UChatDetailsViewModel()
    var isAudio = false
    var isMessage = true
    var receiverCallPopUp:CallingPopUp!
    var dialCallPopUp:CallingPopUp!
    var call_chatID = ""
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewQuoteMessage.isHidden = true
        viewHeaderSelectMessage.isHidden = true
        let userdefault = UserDefaults.standard
        userdefault.setValue(false, forKey: "isbotCalled")
        userdefault.synchronize()
        tblList.backgroundColor = UIColor.white
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
            } else {
            }
        }
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                } else {}
            })
        }
        u2uChatDetailsVM.chat_delegate = self
        viewSetUp()
        u2uChatDetailsVM.sendMessageToScoket(msgBody: txtMessage.textView.text!, localID: "1", messageType: MSG_Type.blank, chat_id: nil)
        readTasksAndUpdateUI()
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
    }
    
    func readTasksAndUpdateUI(){
        do{
            let uiRealm = try Realm()
            //lists = uiRealm.objects(Chat_Room.self)
            u2uChatDetailsVM.local_chat = uiRealm.object(ofType: Chat_Room.self, forPrimaryKey: u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id)
            if u2uChatDetailsVM.local_chat == nil{
                u2uChatDetailsVM.local_chat = Chat_Room()
                u2uChatDetailsVM.local_chat.chat_room = u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    realmObj.add(u2uChatDetailsVM.local_chat)
                    u2uChatDetailsVM.messages = u2uChatDetailsVM.local_chat.messages
                }
            }else{
                for message in u2uChatDetailsVM.local_chat.messages{
                    if message.chat_name == nil{
                        u2uChatDetailsVM.sendMessageToScoket_unsentMsg(message: message)
                    }
                }
                u2uChatDetailsVM.messages = u2uChatDetailsVM.local_chat.messages
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    func viewSetUp() {
        if let frnd = u2uChatDetailsVM.friend{
            lblHeader.text = "\(frnd.firstname ?? "") \(frnd.lastname ?? "")"
            lblType.text = ""
            imgUser.sd_setImage(with: URL(string: frnd.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
        }
        if let grp = u2uChatDetailsVM.group{
            if grp.type == "1"{
                let friend_detail = grp.chatroom_members.filter{$0.user_id != U2UProxy.shared.u2uUser.user_id}[0]
                lblHeader.text = friend_detail.user_name
                lblType.text = ""
            }else{
                lblHeader.text = grp.chatroom_name
                lblType.text = ""
            }
            imgUser.sd_setImage(with: URL(string: grp.chatroom_group_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
        }
        u2uChatDetailsVM.u2uHomeVM?.u2uSocketVM.socket_delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isMessage{
            callToFriend()
        }
    }
    func cleanUp() {
            jitsiMeetView?.leave()
            jitsiMeetView?.removeFromSuperview()
            jitsiMeetView = nil
            pipViewCoordinator = nil
    }
    func callToFriend() {
        u2uChatDetailsVM.sendMessageToScoket(msgBody: txtMessage.textView.text!, localID: "1", messageType: MSG_Type.callDial, chat_id: nil)
        dialCallPopUp = CallingPopUp(frame: view.bounds)
        dialCallPopUp.isAudio = isAudio
        dialCallPopUp.isCalling = true
        dialCallPopUp.friend = u2uChatDetailsVM.friend
        dialCallPopUp.lbl_receiverName.text = lblHeader.text
        dialCallPopUp.socket_delegate = self
        dialCallPopUp.callBegen()
        view.addSubview(dialCallPopUp)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }
    //MARK:- Action
    @IBAction func actionCancelQuoteView(_ sender: Any){
        viewQuoteMessage.isHidden = true
    }

    //MARK:- Action Methods
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
            }
        }
        return nil
    }
    @IBAction func actionHeaderSelectMessageDelete(_ sender: Any){}
    @IBAction func actionHeaderSelectMessageStar(_ sender: Any) {}
    @IBAction func actionHeaderSelectMessageForward(_ sender: Any) {}
    @IBAction func actionHeaderSelectMessageCopy(_ sender: Any) {}
    @IBAction func actionHeaderSelectMessageCancel(_ sender: Any) {}
    @IBAction func btnActionSendChat(_ sender:UIButton) {
        if txtMessage.textView.text.count > 0{
            u2uChatDetailsVM.sendMessageToScoket(msgBody: txtMessage.textView.text!, localID: "1", messageType: MSG_Type.message, chat_id: nil)
        }
    }
    @IBAction func btnActionOpenVideo(_ sender:UIButton) {
        if self.audioObj != nil {
            self.audioObj?.removeFromSuperview()
            self.audioObj = nil
        }
        self.imgPicker.sourceType = .savedPhotosAlbum
        self.imgPicker.delegate = self
        self.imgPicker.mediaTypes = [kUTTypeMovie as String]
        self.present(self.imgPicker, animated: true, completion: nil)
    }
    @IBAction func btnActionOpenPicker(_ sender:UIButton) {
        if self.audioObj != nil {
            self.audioObj?.removeFromSuperview()
            self.audioObj = nil
        }
        if sender.tag == 0 {
            sender.tag = 1
            UIView.animate(withDuration: 0.3) {
                self.heightBottom.constant = 180
            }
        }else{
            sender.tag = 0
            UIView.animate(withDuration: 0.3) {
                self.heightBottom.constant = 0
            }
        }
    }
    @IBAction func btnActionBack(_ sender:UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnActionOpenCamera() {
        if self.audioObj != nil {
            self.audioObj?.removeFromSuperview()
            self.audioObj = nil
        }
        self.imagePicker.present(from: self.view)
    }
    @IBAction func btnActionOpenPDF() {
        let doc = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTF)], in: .import)
        //doc.delegate = self
        doc.modalPresentationStyle = .formSheet
        self.present(doc, animated: true, completion: nil)
    }
    @IBAction func btnActionOpenRecording(_ sender:UIButton) {
        self.audioObj = nil
        if audioObj == nil {
            audioObj = AudioRecorderAndPlay(frame: CGRect(x:UIScreen.main.bounds.width, y:UIScreen.main.bounds.height-50 , width:UIScreen.main.bounds.width , height:50))
            audioObj?.delegate = self
            self.view.addSubview(audioObj!)
            var bottomSafeArea:CGFloat = 0
            if #available(iOS 11.0, *) {
                bottomSafeArea = view.safeAreaInsets.bottom
            }
            UIView.animate(withDuration: 0.3) {
                self.audioObj?.frame = CGRect(x:0, y:UIScreen.main.bounds.height-(50+bottomSafeArea) , width:UIScreen.main.bounds.width , height:50)
            }
        }
    }
    @IBAction fileprivate func btnActionCallVideo(_ sender:UIButton) {
        callToFriend()
    }
}
extension U2UChatDetailViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String {
            if mediaType  == "public.image" {
                self.dismiss(animated: true, completion: nil)
            }
            if mediaType == "public.movie" {
                self.dismiss(animated: true, completion: nil)
                let imageURL = info[UIImagePickerController.InfoKey.mediaURL]
                self.cropVideo(sourceURL1: imageURL as! NSURL, statTime: 0.0, endTime: 20.0)
            }
        }
    }
    func cropVideo(sourceURL1: NSURL, statTime:Float, endTime:Float) {
        let manager = FileManager.default
        guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
        let asset = AVAsset(url: sourceURL1 as URL)
        _ = Float(asset.duration.value) / Float(asset.duration.timescale)
        var outputURL = documentDirectory.appendingPathComponent(FileDirectoryConstant.output)
        do {
            try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(sourceURL1.lastPathComponent ?? "").mp4")
        } catch  {
        }
        try? manager.removeItem(at: outputURL)
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mp4
        let timeRange = CMTimeRange(start: CMTime(seconds: Double(statTime), preferredTimescale: 1000),
                                    end: CMTime(seconds: Double(endTime), preferredTimescale: 1000))
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status {
            case .completed:
                do {
                    let videoData = try Data(contentsOf: outputURL)
                    let fileUrl = "\(U2U_ApiManager.imgPrefix)\(self.u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id ?? "")/\(U2UProxy.shared.u2uUser.username)/\(U2UProxy.shared.randomString(length: 13)).mp4"
                    FileDirectory.sharedInstance.saveFileFromURL(fileData: videoData, fileName: "\(fileUrl.split(separator: "/").last!)")
                    self.u2uChatDetailsVM.sendImageMessageToScoket(fileUrl: fileUrl, localID: "1", messageType: "user_media_vid", chat_id: self.u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id!, voiceData: videoData)
                } catch {
                }
            case .failed:
                break
            case .cancelled:
                break
            default: break
            }
        }
    }
}
extension U2UChatDetailViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {}
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {}
}
extension U2UChatDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uChatDetailsVM.messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let msg = u2uChatDetailsVM.messages[indexPath.row]
        if msg.type == "usermsg" || msg.type == "1"{
            if msg.chat_name == nil{
                let cell = self.tblList.dequeueReusableCell(withIdentifier: "TextMeCell") as! TextMeCell
                cell.indexPath = indexPath
                cell.u2uChatDetailsVM = u2uChatDetailsVM
                cell.messageDetails = msg
                return cell
                }else{
                if msg.userid == U2UProxy.shared.u2uUser.user_id{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "TextMeCell") as! TextMeCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    return cell
                }else{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "TextSenderCell") as! TextSenderCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    return cell
                }
            }
        }else if msg.type == "user_media_voice_note" || msg.type == "8"{
            if msg.chat_name == nil{
                let cell = self.tblList.dequeueReusableCell(withIdentifier: "AudioTableViewMeCell") as! AudioTableViewMeCell
                cell.indexPath = indexPath
                cell.u2uChatDetailsVM = u2uChatDetailsVM
                cell.messageDetails = msg
                cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                return cell
            }else{
                if msg.userid == U2UProxy.shared.u2uUser.user_id{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "AudioTableViewMeCell") as! AudioTableViewMeCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }else{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "AudioTableViewSenderCell") as! AudioTableViewSenderCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }
            }
        }else if msg.type == "user_media_img"{
            if msg.chat_name == nil{
                let cell = self.tblList.dequeueReusableCell(withIdentifier: "ImageMeCell") as! ImageMeCell
                cell.indexPath = indexPath
                cell.u2uChatDetailsVM = u2uChatDetailsVM
                cell.messageDetails = msg
                cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                return cell
            }else{
                if msg.userid == U2UProxy.shared.u2uUser.user_id{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "ImageMeCell") as! ImageMeCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }else{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "ImageSenderCell") as! ImageSenderCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }
            }
        }else if msg.type == "user_media_vid"{
            if msg.chat_name == nil{
                let cell = self.tblList.dequeueReusableCell(withIdentifier: "VideoTableViewMeCell") as! VideoTableViewMeCell
                cell.indexPath = indexPath
                cell.u2uChatDetailsVM = u2uChatDetailsVM
                cell.messageDetails = msg
                cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                cell.btnViewVideo.addTarget(self, action: #selector(self.btnActionShowVideo(_:)), for: UIControl.Event.touchUpInside)
                return cell
            }else{
                if msg.userid == U2UProxy.shared.u2uUser.user_id{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "VideoTableViewMeCell") as! VideoTableViewMeCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    cell.btnViewVideo.addTarget(self, action: #selector(self.btnActionShowVideo(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }else{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "VideoTableViewSenderCell") as! VideoTableViewSenderCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    cell.btnViewVideo.addTarget(self, action: #selector(self.btnActionShowVideo(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }
            }
        }else if msg.type == "user_media_file"{
            if msg.chat_name == nil{
                let cell = self.tblList.dequeueReusableCell(withIdentifier: "PDFTableViewMeCell") as! PDFTableViewMeCell
                cell.indexPath = indexPath
                cell.u2uChatDetailsVM = u2uChatDetailsVM
                cell.messageDetails = msg
                cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                cell.btnViewPdf.addTarget(self, action: #selector(self.btnActionOpenPdfFile(_:)), for: UIControl.Event.touchUpInside)
                return cell
            }else{
                if msg.userid == U2UProxy.shared.u2uUser.user_id{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "PDFTableViewMeCell") as! PDFTableViewMeCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    cell.btnViewPdf.addTarget(self, action: #selector(self.btnActionOpenPdfFile(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }else{
                    let cell = self.tblList.dequeueReusableCell(withIdentifier: "PDFTableViewSenderCell") as! PDFTableViewSenderCell
                    cell.indexPath = indexPath
                    cell.u2uChatDetailsVM = u2uChatDetailsVM
                    cell.messageDetails = msg
                    cell.btnDownload.addTarget(self, action: #selector(self.downloadFiles(_:)), for: UIControl.Event.touchUpInside)
                    cell.btnViewPdf.addTarget(self, action: #selector(self.btnActionOpenPdfFile(_:)), for: UIControl.Event.touchUpInside)
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    @objc func btnActionShowVideo(_ sender:UIButton) {
        if let media_url = sender.layer.value(forKey: "media_url") as? String{
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let playerItem = AVPlayerItem(url: URL(fileURLWithPath: documentsPath.appendingFormat("/\(FileDirectoryConstant.directoryName)/\(media_url.split(separator: "/").last!)")))
            let player = AVPlayer(playerItem: playerItem)
            let vc = AVPlayerViewController()
            vc.player = player
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            vc.player?.play()
        }
    }
    @objc func btnActionOpenPdfFile(_ sender:UIButton) {
        if let media_url = sender.layer.value(forKey: "media_url") as? String{
            let previewController = QLPreviewController()
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let destinationUrl = documentsDirectoryURL.appendingPathComponent("\(FileDirectoryConstant.directoryName)/\(media_url.split(separator: "/").last!)")
            previewController.dataSource = self
            self.urlPreviewForFile = destinationUrl as NSURL
            self.present(previewController, animated: true, completion: nil)
        }
    }
    @objc func downloadFiles(_ sender:UIButton) {
        sender.loadingIndicator(true)
        if let media_url = sender.layer.value(forKey: "media_url") as? String{
            let url = URL(string: "\(U2U_ApiManager.downloadingUrl)\(media_url.dropFirst())")
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler: {
                data, response, error in
                if error == nil {
                    if let response = response as? HTTPURLResponse {
                        if response.statusCode == 200 {
                            if let data = data {
                                let urlRes = response.url!.absoluteString.replacingOccurrences(of: "\(U2U_ApiManager.downloadingUrl)/", with: "./")
                                FileDirectory.sharedInstance.saveFileFromURL(fileData: data, fileName: "\(urlRes.split(separator: "/").last!)")
                                //let indexPath = IndexPath(item: MessageListDao.sharedInstance.arrMessageDao.firstIndex(where: {$0.message == urlRes})!, section: 0)
                                DispatchQueue.main.async {
                                    //MessageListDao.sharedInstance.arrMessageDao[indexPath.row].isDownloading = false
                                    self.tblList.reloadData()
                                    
                                }
                            }
                        }
                    }
                } else {
                }
            })
            task.resume()
        }
    }
}
extension U2UChatDetailViewController:Socket_Connection{
    @objc func messageRecevied(message: Realm_Message) {
        let arr = u2uChatDetailsVM.messages.filter{$0.time == message.time}
        if arr.count > 0{
            let index = u2uChatDetailsVM.messages.firstIndex(where: { $0.time == message.time })
            //let index = u2uChatDetailsVM.messages.firstIndex(of: arr[0])
            if let indx = index{
                do{
                    let realmObj = try Realm()
                    try! realmObj.write { () -> Void in
                        u2uChatDetailsVM.messages[indx] = message
                        realmObj.add(u2uChatDetailsVM.local_chat)
                        self.tblList.reloadData()
                    }
                }catch{
                    print("error \(error.localizedDescription)")
                }
            }
        }
            else{
            do{
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    u2uChatDetailsVM.messages.append(message)
                    realmObj.add(u2uChatDetailsVM.local_chat)
                    self.tblList.reloadData()
                }
            }catch{
                print("error \(error.localizedDescription)")
            }
        }
    }
    func audioRecevied(message: Realm_Message) {
        let arr = u2uChatDetailsVM.messages.filter{$0.time == message.time}
        if arr.count > 0{
            let index = u2uChatDetailsVM.messages.firstIndex(where: { $0.time == message.time })
            //let index = u2uChatDetailsVM.messages.firstIndex(of: arr[0])
            if let indx = index{
                do{
                    let realmObj = try Realm()
                    try! realmObj.write { () -> Void in
                        u2uChatDetailsVM.messages[indx] = message
                        realmObj.add(u2uChatDetailsVM.local_chat)
                        self.tblList.reloadData()
                    }
                }catch{
                    print("error \(error.localizedDescription)")
                }
            }
        }
            else{
            do{
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    u2uChatDetailsVM.messages.append(message)
                    realmObj.add(u2uChatDetailsVM.local_chat)
                    self.tblList.reloadData()
                }
            }catch{
                print("error \(error.localizedDescription)")
            }
        }
    }
    func callReceived(message: Message) {
        call_chatID = message.chat_id
        receiverCallPopUp = CallingPopUp(frame: view.bounds)
        receiverCallPopUp.isAudio = isAudio
        receiverCallPopUp.isCalling = false
        receiverCallPopUp.friend = u2uChatDetailsVM.friend
        receiverCallPopUp.lbl_receiverName.text = lblHeader.text
        receiverCallPopUp.socket_delegate = self
        receiverCallPopUp.callBegen()
        view.addSubview(receiverCallPopUp)
    }
    func acceptCall() {
        u2uChatDetailsVM.sendMessageToScoket(msgBody: txtMessage.textView.text!, localID: "1", messageType: MSG_Type.callAccept, chat_id: call_chatID)
        removeCallPopUp()
        conferenceSettings()
    }
    func callAccepted(message: Message) {
        conferenceSettings()
    }
    func callRejected(message: Message) {
        removeCallPopUp()
        cleanUp()
    }
    func conferenceSettings() {
        cleanUp()
        let jitsiMeetView = JitsiMeetView()
        jitsiMeetView.delegate = self
        self.jitsiMeetView = jitsiMeetView
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            builder.serverURL = URL(string: "https://vc.wespher.com")
            builder.room = self.u2uChatDetailsVM.u2uHomeVM?.chat_room?.chat_id
            builder.audioMuted = false
            builder.audioOnly = self.isAudio
            builder.videoMuted = false
            builder.token = JitsiToken
            builder.welcomePageEnabled = false
        }
        jitsiMeetView.join(options)
        pipViewCoordinator = PiPViewCoordinator(withView: jitsiMeetView)
        pipViewCoordinator?.configureAsStickyView(withParentView: view)

        jitsiMeetView.alpha = 0
        pipViewCoordinator?.show()
    }
    func rejectCall(isCalling: Bool) {
        u2uChatDetailsVM.sendMessageToScoket(msgBody: txtMessage.textView.text!, localID: "1", messageType: MSG_Type.callCancel, chat_id: nil)
        removeCallPopUp()
    }
    func callCanceled() {
        removeCallPopUp()
    }
    func removeCallPopUp() {
        if receiverCallPopUp != nil{
            receiverCallPopUp.removeFromSuperview()
            receiverCallPopUp = nil
        }
        if dialCallPopUp != nil{
            dialCallPopUp.removeFromSuperview()
            dialCallPopUp = nil
        }
    }
}
extension U2UChatDetailViewController: JitsiMeetViewDelegate {
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        DispatchQueue.main.async {
            self.pipViewCoordinator?.hide() { _ in
                self.cleanUp()
                self.u2uChatDetailsVM.sendMessageToScoket(msgBody: self.txtMessage.textView.text!, localID: "1", messageType: MSG_Type.callEnd, chat_id: nil)
                self.removeCallPopUp()
            }
        }
    }
    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        DispatchQueue.main.async {
            self.pipViewCoordinator?.enterPictureInPicture()
        }
    }
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
    }
}

extension U2UChatDetailViewController: AudioRecorderProtocol {
    func sendChat(data: Data) {
        let fileUrl = "\(U2U_ApiManager.imgPrefix)\(u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id ?? "")/\(U2UProxy.shared.u2uUser.username)/\(U2UProxy.shared.randomString(length: 13)).wav"
        FileDirectory.sharedInstance.saveFileFromURL(fileData: data, fileName: "\(fileUrl.split(separator: "/").last!)")
        u2uChatDetailsVM.sendVoiceMessageToScoket(fileUrl: fileUrl, localID: "1", messageType: "8", chat_id: u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id!, voiceData: data)
    }
}
extension U2UChatDetailViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?, imageName: String, dataSize: Data?) {
        if image == nil{
            return
        }
        let fileUrl = "\(U2U_ApiManager.imgPrefix)\(u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id ?? "")/\(U2UProxy.shared.u2uUser.username)/\(U2UProxy.shared.randomString(length: 13)).png"
        FileDirectory.sharedInstance.saveFileFromURL(fileData: dataSize, fileName: "\(fileUrl.split(separator: "/").last!)")
        let imageData = image!.pngData()
        u2uChatDetailsVM.sendImageMessageToScoket(fileUrl: fileUrl, localID: "1", messageType: "user_media_img", chat_id: u2uChatDetailsVM.u2uHomeVM!.chat_room!.chat_id!, voiceData: imageData!)
    }
    
}
extension U2UChatDetailViewController: QLPreviewControllerDataSource,QLPreviewControllerDelegate {
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return urlPreviewForFile!
    }
    
    func previewControllerWillDismiss(_ controller: QLPreviewController) {
    }
    
}
extension U2UChatDetailViewController : AVPlayerViewControllerDelegate {
    
    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func playerViewControllerWillStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        playerViewController.view.removeFromSuperview()
    }
    
}
