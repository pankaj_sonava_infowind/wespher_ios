//
//  MessageAndCallSettingViewController.swift
//  VKApp
//
//  Created by mac on 26/11/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class MessageAndCallSettingViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet fileprivate weak var switchChatNotification : UISwitch!
    @IBOutlet fileprivate weak var switchTicketNotification : UISwitch!
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.value(forKey: ResponseData.chatNotification) != nil {
            if UserDefaults.standard.bool(forKey: ResponseData.chatNotification) == true {
                switchChatNotification.isOn = true
            } else {
                switchChatNotification.isOn = false
            }
        }
        
        if UserDefaults.standard.value(forKey: ResponseData.ticketNotification) != nil {
            if UserDefaults.standard.bool(forKey: ResponseData.ticketNotification) == true {
                switchTicketNotification.isOn = true
            } else {
                switchTicketNotification.isOn = false
            }
        }
    }
    
    @IBAction fileprivate func changeSwitchChatVal(_ sender:UISwitch) {
        let val = UserDefaults.standard
        if sender.isOn {
            val.set(true, forKey: ResponseData.chatNotification)
            val.synchronize()
        } else {
            val.set(false, forKey: ResponseData.chatNotification)
            val.synchronize()
        }
    }
    
    @IBAction fileprivate func changeSwitchTicketVal(_ sender:UISwitch) {
        let val = UserDefaults.standard
        if sender.isOn {
            val.set(true, forKey: ResponseData.ticketNotification)
            val.synchronize()
        } else {
            val.set(true, forKey: ResponseData.ticketNotification)
            val.synchronize()
        }
    }
    
    //MARK:- Action methods
    //    @IBAction fileprivate func btnActionOpenChatScreen(_ sender:UIButton) {
    //        let objChatAndTicketSetting = self.storyboard?.instantiateViewController(withIdentifier: "ChatSettingViewController") as! ChatSettingViewController
    //        self.navigationController?.pushViewController(objChatAndTicketSetting, animated: true)
    //    }
    
    //    @IBAction fileprivate func btnActionOpenTicketScreen(_ sender:UIButton) {
    //        let objChatAndTicketSetting = self.storyboard?.instantiateViewController(withIdentifier: "TicketSettingViewController") as! TicketSettingViewController
    //        self.navigationController?.pushViewController(objChatAndTicketSetting, animated: true)
    //    }
    
    @IBAction fileprivate func btnActionBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
