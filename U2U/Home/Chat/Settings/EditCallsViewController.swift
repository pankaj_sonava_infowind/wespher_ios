//
//  EditCallsViewController.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class EditCallsViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblEditCalls         : UITableView!
    @IBOutlet weak var imgSelectAll         : UIImageView!
    @IBOutlet weak var viewSearch           : UIView!
    @IBOutlet weak var lblNoOfCalls         : UILabel!
    @IBOutlet weak var lblEditHeader        : UILabel!
    @IBOutlet weak var txtSearch            : UITextField!
    @IBOutlet weak var btn_all: UIButton!
    
    //MARK:- Variables
    var strSearch : String! = ""
    var selectedCall = [String]()
    var u2uHomeVM:U2UHomeViewModel!
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.isHidden = true
        txtSearch.attributedPlaceholder = NSAttributedString(string: "search",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
    }
    
    //MARK:- Action Methdos
    @IBAction func btnActionBackI(_ sender:UIButton) {

        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func btnActionDelete(_ sender :UIButton) {
//        var deletedID : String?
//        for obj in CallListDao.sharedInstance.arrCallDao {
//            if obj.isSelected ?? false{
//                if deletedID == nil {
//                    deletedID =  obj.id ?? statusValues.isEmptyValue
//                } else {
//                    deletedID =  "\(deletedID ?? statusValues.isEmptyValue ),\(obj.id ?? statusValues.isEmptyValue)"
//                }
//            }
//        }
//        if deletedID != nil {
//            self.callingAPIForDeleteCalls(ids: deletedID ?? statusValues.isEmptyValue)
//        }else {
//            self.view.makeToast(ErrorMsg.select_any_call)
//        }
    }
    
    @IBAction fileprivate func btnActionSelectAllMessages(_ sender:UIButton) {
       if sender.isSelected {
            sender.isSelected = false
            imgSelectAll.image = UIImage(named: "unselected")
            selectedCall.removeAll()
        }else{
            sender.isSelected = true
            imgSelectAll.image = UIImage(named: "allSelected")
            selectedCall = u2uHomeVM!.allCalls.map{$0.id}
        }
        self.tblEditCalls.reloadData()
    }
    
    @IBAction fileprivate func btnActionShowHideSearch(_ sender:UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.viewSearch.isHidden = false
            self.lblEditHeader.isHidden = true
            self.lblNoOfCalls.isHidden = true
            self.txtSearch.text = ""
        } else {
            sender.tag = 0
            self.viewSearch.isHidden = true
            self.lblEditHeader.isHidden = false
            self.lblNoOfCalls.isHidden = false
            self.txtSearch.text = ""
        }
        self.tblEditCalls.reloadData()
    }
    
    //MARK:- API's Calling
    func callingAPIForDeleteCalls(ids:String) {
//        self.callingApiForDeleteCallLog(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue, calls: ids) { (isSuccess, error) in
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                    CallListDao.sharedInstance.arrCallDao = CallListDao.sharedInstance.arrCallDao.filter({$0.isSelected == false})
//                    self.tblEditCalls.reloadData()
//                }
//            }
//        }
    }
    
}

//MARK:- Table view delegate and datasource methods
extension EditCallsViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblNoOfCalls.text = "\(u2uHomeVM.allCalls.count) calls"
        return u2uHomeVM.allCalls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblEditCalls.dequeueReusableCell(withIdentifier: "EditCallsTableViewCell") as! EditCallsTableViewCell
        cell.prepareLayout(objChat: u2uHomeVM.allCalls[indexPath.row])
        cell.lblDot.layer.cornerRadius = cell.lblDot.frame.width/2
        if selectedCall.contains(u2uHomeVM.allCalls[indexPath.row].id){
            cell.imgSelected.image = UIImage(named: "allSelected")
        }else{
            cell.imgSelected.image = UIImage(named: "unselected")
        }
        cell.btnSelected.tag = indexPath.row
        cell.btnSelected.addTarget(self, action: #selector(self.btnActionSelectAndUnselect(_:)), for: UIControl.Event.touchUpInside)
        
        cell.lblDot.clipsToBounds = true
        return cell
    }
    
    @objc func btnActionSelectAndUnselect(_ sender:UIButton) {
        let index = sender.tag
        let chatroom_id = u2uHomeVM.allCalls[index].id!
        if selectedCall.contains(chatroom_id){
            let index = selectedCall.firstIndex(of: chatroom_id)
            selectedCall.remove(at: index!)
        }else{
            selectedCall.append(chatroom_id)
        }
        tblEditCalls.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedCall.count == u2uHomeVM!.onlineUsers.count{
            btn_all.isSelected = true
            imgSelectAll.image = UIImage(named: "allSelected")
        }else{
            btn_all.isSelected = false
            imgSelectAll.image = UIImage(named: "unselected")
        }
    }
    
}

//MARK:- Textfield delegate methods
extension EditCallsViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strSearch = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        self.tblEditCalls.reloadData()
        return true
    }
    
}
