//
//  EditChatViewController.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class EditChatViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewHeaderOptions            : UIView!
    @IBOutlet weak var viewHeaderOptionMenu         : UIView!
    @IBOutlet weak var viewSearch                   : UIView!
    @IBOutlet weak var tblEditChatView              : UITableView!
    @IBOutlet weak var imgSelectAll                 : UIImageView!
    @IBOutlet weak var lblNoOfCalls                 : UILabel!
    @IBOutlet weak var lblEditHeader                : UILabel!
    @IBOutlet weak var txtSearch                    : UITextField!
    @IBOutlet weak var lblArchive                   : UILabel!
    @IBOutlet weak var btn_all: UIButton!
    
    //MARK:- Variables
    var strSearch : String! = ""
    var isArchive : Bool?
    var u2uHomeVM:U2UHomeViewModel?
    let editChatVM = EditChatViewModel()
    var selectedGroups = [String]()
    
    //MARK:- Override methods
    override func viewDidLoad(){
        super.viewDidLoad()
        
        self.prepareLayout()
        //self.prepareDB()
        
        if isArchive ?? false {
            self.callingApiForGetArchiveChatList()
            self.lblArchive.text = "Unarchive"
        } else {
            //self.showloader()
            self.callingApiForGetSupportList()
            self.lblArchive.text = "Archive"
        }
        
    }
    
    //MARK:- Prepaer UI's
    func prepareLayout(){
        viewSearch.isHidden = true
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search...",attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        viewHeaderOptions.isHidden = true
    }
    
    //MARK:- Prepaer Database
//    func prepareDB(){
//        ChatListDao.sharedInstance.arrChatDao = []
//        DBManager.sharedInstance.fetchRecordChatList { (isSuccess) in
//            if isSuccess {
//                self.tblEditChatView.reloadData()
//            }
//        }
//    }
    func getOnlineUser() {
        self.u2uHomeVM!.onlineUsers.removeAll()
        self.tblEditChatView.reloadData()
        u2uHomeVM!.getOnlineUser(viewController: self) {
            DispatchQueue.main.async {
                self.tblEditChatView.reloadData()
            }
        }
    }
    //MARK:- Aciton Methods
    @IBAction func btnActionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func btnActionPinned(_ sender :UIButton){
        if selectedGroups.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select friend", viewController: self)
            return
        }
        let ids = U2UProxy.shared.json(from: selectedGroups) ?? ""
//        selectedGroups.removeAll()
//        btn_all.isSelected = false
//        imgSelectAll.image = UIImage(named: "unselected")
        editChatVM.pinnedRequest(viewController: self, chat_id: ids) {
            DispatchQueue.main.async {
                self.getOnlineUser()
            }
        }
    }
    
    @IBAction fileprivate func btnActionSelectAllMessages(_ sender:UIButton){
        if sender.isSelected {
            sender.isSelected = false
            imgSelectAll.image = UIImage(named: "unselected")
            selectedGroups.removeAll()
        }else{
            sender.isSelected = true
            imgSelectAll.image = UIImage(named: "allSelected")
            selectedGroups = u2uHomeVM!.onlineUsers.map{$0.chatroom_id}
        }
        tblEditChatView.reloadData()
    }
    
    @IBAction fileprivate func btnActionStarred(_ sender :UIButton) {
        if selectedGroups.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select friend", viewController: self)
            return
        }
        let ids = U2UProxy.shared.json(from: selectedGroups) ?? ""
//        selectedGroups.removeAll()
//        btn_all.isSelected = false
//        imgSelectAll.image = UIImage(named: "unselected")
        editChatVM.staredRequest(viewController: self, chat_id: ids) {
            DispatchQueue.main.async {
                self.getOnlineUser()
            }
        }
    }
    
    @IBAction fileprivate func btnActionShowHideSearch(_ sender:UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.viewSearch.isHidden = false
            self.lblEditHeader.isHidden = true
            self.lblNoOfCalls.isHidden = true
            self.txtSearch.text = ""
        } else {
            sender.tag = 0
            self.viewSearch.isHidden = true
            self.lblEditHeader.isHidden = false
            self.lblNoOfCalls.isHidden = false
            self.txtSearch.text = ""
        }
        self.tblEditChatView.reloadData()
    }
    
    @IBAction func btnActionOptions(_ sender:UIButton) {
        self.viewHeaderOptions.isHidden = false
    }
    
    @IBAction func btnActionHideOptions(_ sender:UIButton){
        self.viewHeaderOptions.isHidden = true
    }
    
    @IBAction func btnActionOptionNotification(_ sender:UIButton){
        self.viewHeaderOptions.isHidden = true
        self.view.makeToast("Coming soon")
    }
    
    @IBAction func btnActionOptionArchive(_ sender:UIButton){
//        self.viewHeaderOptions.isHidden = true
//        var strIDs : String?
//        if isArchive ?? false {
//            for v in ChatListDao.sharedInstance.arrArchiveChatDao{
//                if v.isSelected ?? false {
//                    if strIDs == nil {
//                        strIDs = v.chatroom_id ?? statusValues.isEmptyValue
//                    } else {
//                        strIDs = "\(strIDs ?? statusValues.isEmptyValue),\(v.chatroom_id ?? statusValues.isEmptyValue)"
//                    }
//                }
//            }
//            if strIDs == nil {
//                self.view.makeToast(ErrorMsg.select_any_chat)
//            } else {
//                self.showloader()
//                self.callingAPIForRemoveToArchive(ids: strIDs ?? statusValues.isEmptyValue)
//            }
//        } else {
//            for v in ChatListDao.sharedInstance.arrChatDao {
//                if v.isSelected ?? false {
//                    if strIDs == nil {
//                        strIDs = v.chatroom_id ?? statusValues.isEmptyValue
//                    } else {
//                        strIDs = "\(strIDs ?? statusValues.isEmptyValue),\(v.chatroom_id ?? statusValues.isEmptyValue)"
//                    }
//                }
//            }
//            if strIDs == nil {
//                self.view.makeToast(ErrorMsg.select_any_chat)
//            } else {
//                self.showloader()
//                self.callingAPIForAddToArchive(ids: strIDs ?? statusValues.isEmptyValue)
//            }
//        }
    }
    
    @IBAction func btnActionOptionDelete(_ sender:UIButton){
//        self.viewHeaderOptions.isHidden = true
//        var strIDs : String?
//        if isArchive ?? false {
//            for v in ChatListDao.sharedInstance.arrArchiveChatDao{
//                if v.isSelected ?? false {
//                    if strIDs == nil {
//                        strIDs = v.chatroom_id ?? statusValues.isEmptyValue
//                    } else {
//                        strIDs = "\(strIDs ?? statusValues.isEmptyValue),\(v.chatroom_id ?? statusValues.isEmptyValue)"
//                    }
//                }
//            }
//            if strIDs == nil {
//                self.view.makeToast(ErrorMsg.select_any_chat)
//            } else {
//                self.showloader()
//                self.callingAPIForRemoveToArchive(ids: strIDs ?? statusValues.isEmptyValue)
//            }
//        } else {
//            for v in ChatListDao.sharedInstance.arrChatDao {
//                if v.isSelected ?? false {
//
//                    if strIDs == nil {
//                        strIDs = v.chatroom_id ?? statusValues.isEmptyValue
//                    } else {
//                        strIDs = "\(strIDs ?? statusValues.isEmptyValue),\(v.chatroom_id ?? statusValues.isEmptyValue)"
//                    }
//                }
//            }
//            if strIDs == nil {
//                self.view.makeToast(ErrorMsg.select_any_chat)
//            } else {
//                self.showloader()
//                self.callingAPIForAddToArchive(ids: strIDs ?? statusValues.isEmptyValue)
//            }
//        }
    }
    
    //MARK:- Calling API's
    func callingApiForGetSupportList(){
//        ChatListDao.sharedInstance.arrChatDao = []
//        self.getAllSupportListTest(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue)
//        self.callingApiForGetArchiveChatList()
//        self.callingApiForGetSupportUsers(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue) { (isSuccess, error) in
//            self.hideLoader()
//            DispatchQueue.main.async {
//                self.tblEditChatView.reloadData()
//            }
//        }
    }
    
    func callingApiForGetArchiveChatList(){
//        self.showloader()
//        ChatListDao.sharedInstance.arrArchiveChatDao = []
//        self.callingApiForGetAllArchiveChats(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue) { (isSuccess, error) in
//            self.hideLoader()
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.tblEditChatView.reloadData()
//                }
//            }else{
//
//            }
//        }
    }
    
    func callingAPIForStarredUnstarredMsg(ids:String) {
//        self.callingApiForStarredSupportUsers(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue , chats: ids) { (isSuccess, error) in
//            self.hideLoader()
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.view.makeToast(SucessMessage.success_update)
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.view.makeToast(ErrorMsg.apiError)
//                }
//            }
//        }
    }
    
    func callingAPIForPinnedMsg(ids:String) {
//        self.callingApiForPinnedSupportUsers(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue , chats: ids) { (isSuccess, error) in
//            self.callingApiForGetSupportList()
//            self.hideLoader()
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                    self.view.makeToast(SucessMessage.success_update)
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.view.makeToast(ErrorMsg.apiError)
//                }
//            }
//        }
    }
    
    func callingAPIForAddToArchive(ids:String) {
//        self.callingApiForAddChatArchive(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue , chats: ids) { (isSuccess, error) in
//            self.hideLoader()
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                    self.view.makeToast(SucessMessage.success_update)
//                    self.callingApiForGetSupportList()
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.view.makeToast(ErrorMsg.apiError)
//                }
//            }
//        }
    }
    
    func callingAPIForRemoveToArchive(ids:String) {
//        self.callingApiForRemoveChatArchive(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue , chats: ids) { (isSuccess, error) in
//            self.hideLoader()
//            if isSuccess {
//                DispatchQueue.main.async {
//                    self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                    self.view.makeToast(SucessMessage.success_update)
//                    self.callingApiForGetArchiveChatList()
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.view.makeToast(ErrorMsg.apiError)
//                }
//            }
//        }
    }
    
    func callingAPIForDeleteChat(ids:String) {
//        self.callingApiForDeleteChat(user_id: UserDao.sharedInstance.w_id ?? statusValues.isEmptyValue , chats: ids) { (isSuccess, error) in
//            self.hideLoader()
//            if isSuccess {
//                if self.isArchive ?? false{
//                    DispatchQueue.main.async {
//                        self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                        self.view.makeToast(SucessMessage.success_delete)
//                        self.callingApiForGetArchiveChatList()
//                    }
//                } else {
//                    DispatchQueue.main.async {
//                        self.imgSelectAll.image = #imageLiteral(resourceName: "unselected")
//                        self.view.makeToast(SucessMessage.success_delete)
//                        self.callingApiForGetSupportList()
//                    }
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.view.makeToast(ErrorMsg.apiError)
//                }
//            }
//        }
    }
}

//MARK:- Tableview Delegate and datasource methods
extension EditChatViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        lblNoOfCalls.text = "\(u2uHomeVM?.onlineUsers.count ?? 0) chats"
        return u2uHomeVM?.onlineUsers.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblEditChatView.dequeueReusableCell(withIdentifier: "EditChatListCell") as! EditChatListCell
        cell.prepareLayout(objChatDao: u2uHomeVM!.onlineUsers[indexPath.row])
        cell.lblDot.layer.cornerRadius = cell.lblDot.frame.width/2
        cell.lblDot.clipsToBounds = true
        cell.viewCount.layer.cornerRadius = cell.viewCount.frame.width/2
        cell.viewCount.clipsToBounds = true
        if selectedGroups.contains(u2uHomeVM!.onlineUsers[indexPath.row].chatroom_id){
            cell.imgSelected.image = UIImage(named: "allSelected")
        }else{
            cell.imgSelected.image = UIImage(named: "unselected")
        }
        cell.btnSelected.tag = indexPath.row
        cell.btnSelected.addTarget(self, action: #selector(self.btnActionSelectAndUnselect(_:)), for: UIControl.Event.touchUpInside)
        return cell
    }
    
    @objc func btnActionSelectAndUnselect(_ sender:UIButton) {
        let index = sender.tag
        let chatroom_id = u2uHomeVM!.onlineUsers[index].chatroom_id!
        if selectedGroups.contains(chatroom_id){
            let index = selectedGroups.firstIndex(of: chatroom_id)
            selectedGroups.remove(at: index!)
        }else{
            selectedGroups.append(chatroom_id)
        }
        tblEditChatView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedGroups.count == u2uHomeVM!.onlineUsers.count{
            btn_all.isSelected = true
            imgSelectAll.image = UIImage(named: "allSelected")
        }else{
            btn_all.isSelected = false
            imgSelectAll.image = UIImage(named: "unselected")
        }
    }
    
}

//MARK:- Textfield delegate and datasource methods
extension EditChatViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strSearch = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        self.tblEditChatView.reloadData()
        return true
    }
    
}




    
