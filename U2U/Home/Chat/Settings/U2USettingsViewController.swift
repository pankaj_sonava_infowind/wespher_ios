//
//  U2USettingsViewController.swift
//  VKApp
//
//  Created by mac on 26/11/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class U2USettingsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Action methods
    @IBAction func btnActionOpenMessageAndChatScreen(_ sender:UIButton) {
        let objChatAndTicketSetting = self.storyboard?.instantiateViewController(withIdentifier: "MessageAndCallSettingViewController")
        self.navigationController?.pushViewController(objChatAndTicketSetting!, animated: true)
    }
    
    @IBAction func btnActionOpenInAppScreen(_ sender:UIButton) {
        let objInappSettingsViewController = self.storyboard?.instantiateViewController(withIdentifier: "InappSettingsViewController")
        self.navigationController?.pushViewController(objInappSettingsViewController!, animated: true)
    }
    
    @IBAction func btnActionBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
