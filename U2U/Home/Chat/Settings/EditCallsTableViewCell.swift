//
//  EditCallsTableViewCell.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class EditCallsTableViewCell: UITableViewCell {
    
    //MARK:- Outlets methods
    @IBOutlet  weak var imgUser          : UIImageView!
    @IBOutlet  weak var imgCallType      : UIImageView!
    @IBOutlet  weak var lblName          : UILabel!
    @IBOutlet  weak var lblDate          : UILabel!
    @IBOutlet  weak var lblTime          : UILabel!
    @IBOutlet  weak var imgPin           : UIImageView!
    @IBOutlet  weak var imgSelected      : UIImageView!
    @IBOutlet weak var lblDot                       : UILabel!
    @IBOutlet weak var btnSelected                  : UIButton!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Preparelayout
    func prepareLayout(objChat:Call) {
        if let strNew = objChat.profile_pic {
            if !strNew.isEmpty {
                       self.imgUser.loadImageUsingCacheWithUrlString(strNew)
                   }
               }
        self.lblName.text = objChat.friend_name
        //self.imgCallType.image = objChat.imgStatus
        lblDate.text = ""
        lblTime.text = ""
        if let time = objChat.time{
            if !time.isEmpty{
                let date = Date(timeIntervalSince1970: Double(time)!)
                Date_Formatter.dateFormat = "hh:mm a"
                lblTime.text = Date_Formatter.string(from: date)
                Date_Formatter.dateFormat = "dd/MMM/yyyy"
                lblDate.text = Date_Formatter.string(from: date)
            }
        }
        if (objChat.time?.contains(":"))! {
            self.lblTime.text = "\(self.UTCToLocalW(date: objChat.time!))"
        } else {
            let objTime = self.getTimeW(str: objChat.time ?? "")
            self.lblTime.text = objTime
        }
    }
    
}
