//
//  InappSettingsViewController.swift
//  VKApp
//
//  Created by mac on 26/11/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class InappSettingsViewController: UIViewController {
    
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- Action Methods
    @IBAction fileprivate func btnActionBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
