//
//  EditChatListCell.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class EditChatListCell: UITableViewCell {
    
    //MARK:- Outlet
    @IBOutlet weak var imgUser              : UIImageView!
    @IBOutlet weak var imgType              : UIImageView!
    @IBOutlet weak var imgAsRead            : UIImageView!
    @IBOutlet weak var lblName              : UILabel!
    @IBOutlet weak var lblLastMessage       : UILabel!
    @IBOutlet weak var lblLastSeen          : UILabel!
    @IBOutlet weak var imgPin               : UIImageView!
    @IBOutlet weak var imgStared            : UIImageView!
    @IBOutlet weak var lblDot               : UILabel!
    @IBOutlet weak var viewCount            : UIView!
    @IBOutlet weak var imgSelected          : UIImageView!
    @IBOutlet weak var btnSelected          : UIButton!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Prepeare Layout
    func prepareLayout(objChatDao:Group) {
        if let strNew = objChatDao.profile_pic {
                   if strNew != "" {
                       self.imgUser.loadImageUsingCacheWithUrlString(strNew)
                   }
               }
        
        self.lblName.text = objChatDao.chatroom_name
        self.lblLastMessage.text = objChatDao.message
        self.lblLastSeen.text = objChatDao.time ?? ""
        
        let dateForChat = self.getDate(str: objChatDao.time ?? "")
        if dateForChat.split(separator: "-").count > 1 {
            let dateNew = convertDateFromString(strDate: dateForChat)
            self.lblLastSeen.text = "\(dateNew ?? "")"
        }
        else {
            self.lblLastSeen.text = "\(dateForChat)"
        }
        
        self.viewCount.isHidden = true
        if objChatDao.pinned == "0" {
            imgPin.isHidden = true
        } else {
            imgPin.isHidden = false
        }
        if objChatDao.is_read == 1 {
            imgAsRead.image = #imageLiteral(resourceName: "doubleCheck")
        } else {
            imgAsRead.image = #imageLiteral(resourceName: "imgSingleCheck")
        }
        if objChatDao.starred == "1" {
            imgStared.image = #imageLiteral(resourceName: "starred")
        } else {
            imgStared.image = nil
        }
        if  objChatDao.message_type == MessageTypeW.user_media_img {
            self.lblLastMessage.text = ""
            self.lblLastMessage.addImage(imageName: "imgThumb")
        }
        else if  objChatDao.message_type == MessageTypeW.user_media_vid {
            self.lblLastMessage.text = ""
            self.lblLastMessage.addImage(imageName: "videoIC")
        }
        else  if  objChatDao.message_type == MessageTypeW.user_media_file {
            self.lblLastMessage.text = ""
            self.lblLastMessage.addImage(imageName: "fileThumb")
        }
        else  if  objChatDao.message_type == ParamNameW.user_media_music {
            self.lblLastMessage.text = ""
            self.lblLastMessage.addImage(imageName: "musicThumb")
        }
        else  if objChatDao.message_type ==  MessageTypeW.user_media_voice_note {
            self.lblLastMessage.text = ""
            self.lblLastMessage.addImage(imageName: "microphone")
        }
//        if objChatDao.isSelected ?? false {
//            imgSelected.image = UIImage(named: "allSelected")
//        } else {
//            imgSelected.image = UIImage(named: "unselected")
//        }
        if objChatDao.is_online == "1" {
            lblDot.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        } else {
            lblDot.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
    }
    
    func getDate(str:String) -> String {
        if str == "" {
            return ""
        }
        let date = Date(timeIntervalSince1970: Double(str)!)
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = DateFormateType.yyyy_MM_dd
        let strDate = dateFormatter.string(from: date)
        let splitAPIDate = strDate.split(separator: " ")
        
        let dateMainStr = dateFormatter.string(from: Date())
        let splitMainDate = dateMainStr.split(separator: " ")
        let seconDateCom = "\(splitMainDate[0])"
        
        let firstDateCom = "\(splitAPIDate[0])"
        if firstDateCom == seconDateCom {
            return DateFormateType.Today
        }
        return firstDateCom
    }
    
    func convertDateFromString(strDate:String) -> String? {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = DateFormateType.yyyy_MM_dd
        let dateFinal = dateFormate.date(from: strDate)
        dateFormate.dateFormat = DateFormateType.MMM_d
        if dateFinal != nil {
            let strDateFinal = dateFormate.string(from: dateFinal!)
            return strDateFinal
        }else{
            return ""
        }
    }
    
}

