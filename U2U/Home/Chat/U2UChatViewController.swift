//
//  U2UChatViewController.swift
//  GroupTestSample
//
//  Created by mac on 02/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit
import SVProgressHUD

class U2UChatViewController: UIViewController {
   
    //MARK:- Outlet's
    @IBOutlet weak var txtSearch                : UITextField!
    @IBOutlet weak var btnMenu                  : UIButton!
    @IBOutlet weak var viewSearch               : UIView!
    @IBOutlet weak var viewStack                : UIView!
    @IBOutlet weak var viewCornor               : UIView!
    @IBOutlet weak var viewCllection            : UIView!
    @IBOutlet weak var viewOptions              : UIView!
    @IBOutlet weak var collectionDepartment     : UICollectionView!
    @IBOutlet weak var tblChatView              : UITableView!
    @IBOutlet weak var constraintTbl            : NSLayoutConstraint!
    @IBOutlet weak var viewArchiveChats         : UIView!
    @IBOutlet weak var constraintArchive        : NSLayoutConstraint!
    @IBOutlet weak var lblChatArchiveCount      : UILabel!
    
    @IBOutlet weak var loader_chatData: UIActivityIndicatorView!
    //MARK:- Variables
    var strSearch                               = ""
    
    var u2uHomeVM:U2UHomeViewModel!
    var selectGroup:Group!
    var onlineUser_local = [Group]()
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()

        let parentVC = self.parent?.parent as! U2UHomeVC
        u2uHomeVM = parentVC.u2uHomeVM
        getOnlineUser()
        tblChatView.backgroundColor = UIColor.white
        /*DBManager.sharedInstance.fetchRecordChatList { (isSuccess) in
            if isSuccess {
                self.tblChatView.reloadData()
            }
        }*/
        self.setupLayout()
        self.setupCollection()
        self.constraintArchive.constant = 0
        self.viewArchiveChats.layer.cornerRadius = 20
        self.viewArchiveChats.clipsToBounds = true
    }
    func getOnlineUser() {
        if u2uHomeVM.onlineUsers.count == 0{
            loader_chatData.startAnimating()
            u2uHomeVM.getOnlineUser(viewController: self) {
                var numberOfOnlineUser = 0
                if self.u2uHomeVM.onlineUsers.count > 4{
                    numberOfOnlineUser = 4
                }else{
                    numberOfOnlineUser = self.u2uHomeVM.onlineUsers.count
                }
                DispatchQueue.main.async {
                    self.loader_chatData.stopAnimating()
                    self.constraintTbl.constant = self.tblChatView.rowHeight * CGFloat(numberOfOnlineUser)
                    self.onlineUser_local = self.u2uHomeVM.onlineUsers
                    self.tblChatView.reloadData()
                    self.getFriends()
                }
            }
        }else{
            var numberOfOnlineUser = 0
            if self.u2uHomeVM.onlineUsers.count > 4{
                numberOfOnlineUser = 4
            }else{
                numberOfOnlineUser = self.u2uHomeVM.onlineUsers.count
            }
            self.onlineUser_local = self.u2uHomeVM.onlineUsers
            self.constraintTbl.constant = self.tblChatView.rowHeight * CGFloat(numberOfOnlineUser)
            self.tblChatView.reloadData()
            self.getFriends()
        }
    }
    func getFriends() {
        if u2uHomeVM.friends.count == 0{
            loader_chatData.startAnimating()
            u2uHomeVM.getUserFriends(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_chatData.stopAnimating()
                    self.collectionDepartment.reloadData()
                }
            }
        }else{
            self.collectionDepartment.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.tblChatView.reloadData()
    }
    
    //MARK:- Setup views
    fileprivate func setupLayout() {
        //self.viewCllection.isHidden = true
        self.view.updateConstraintsIfNeeded()
        viewOptions.isHidden = true
        self.tblChatView.reloadData()
        viewSearch.layer.borderColor = #colorLiteral(red: 0.272651583, green: 0.6321746707, blue: 0.7174183726, alpha: 1)
        viewSearch.layer.borderWidth = 0.5
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search or start a new chat",attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        //elf.shadowOnviewWithcornerRadius(currentView: viewCornor)
    }
    
    fileprivate func setupCollection() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.itemSize = CGSize(width: self.collectionDepartment.frame.size.width/2 - 12, height: 163 )
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        collectionDepartment!.collectionViewLayout = layout
    }
    //MARK:- Action Methods
    @IBAction func btnActionOpenDepartments(_ sender:UIButton) {
        
    }
    @IBAction func btnActionShowHideOptions(_ sender:UIButton) {
        self.txtSearch.text = ""
        self.tblChatView.reloadData()
        if sender.tag == 0 {
            self.viewOptions.isHidden = false
        } else {
            self.viewOptions.isHidden = true
        }
    }
    
    @IBAction fileprivate func btnActionShowHideSearch(_ sender:UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.txtSearch.text = ""
        } else {
            self.txtSearch.text = ""
            self.tblChatView.reloadData()
            sender.tag = 0
        }
    }
    
    @IBAction fileprivate func btnActionOpenEditChat(_ sender:UIButton) {
        self.btnMenu.tag = 0
        self.viewOptions.isHidden = true
        let objEditChatVC = self.storyboard?.instantiateViewController(withIdentifier: "EditChatViewController") as! EditChatViewController
        objEditChatVC.isArchive = false
        objEditChatVC.u2uHomeVM = self.u2uHomeVM
        self.navigationController?.pushViewController(objEditChatVC, animated: true)
    }
    
    @IBAction fileprivate func btnActionStarred(_ sender:UIButton){
        self.viewOptions.isHidden = true
        self.txtSearch.text = ""
        u2uHomeVM.starred = "1"
        u2uHomeVM.onlineUsers.removeAll()
        getOnlineUser()
    }
    
    @IBAction fileprivate func btnActionAllMessages(_ sender:UIButton){
        self.viewOptions.isHidden = true
        self.txtSearch.text = ""
        u2uHomeVM.starred = "0"
        u2uHomeVM.onlineUsers.removeAll()
        getOnlineUser()
    }
    
    @IBAction fileprivate func btnActionSetting(_ sender:UIButton){
        self.viewOptions.isHidden = true
        self.txtSearch.text = ""
        //self.view.makeToast("Coming soon")
           let objTicketSettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "U2USettingsViewController") as!  U2USettingsViewController
        self.navigationController?.pushViewController(objTicketSettingsVC, animated: true)
    }
    
    @IBAction fileprivate func btnActionOpenArchiveChats(_ sender:UIButton) {
        self.btnMenu.tag = 0
        self.viewOptions.isHidden = true
        let objEditChatVC = self.storyboard?.instantiateViewController(withIdentifier: "EditChatViewController") as! EditChatViewController
        objEditChatVC.u2uHomeVM = self.u2uHomeVM
        objEditChatVC.isArchive = true
        self.navigationController?.pushViewController(objEditChatVC, animated: true)
    }
}

//MARK:- Tableview delegate and data source methods
extension U2UChatViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return onlineUser_local.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblChatView.dequeueReusableCell(withIdentifier: U2UClass_identifire.U2UChatListCell) as! U2UChatListCell
        cell.userDetails = onlineUser_local[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectGroup = onlineUser_local[indexPath.row]
        let chat_dic = ["chat_id":selectGroup.chatroom,"chat_name":selectGroup.chatroom_name,"token":U2UProxy.shared.u2uUser.token]
        self.u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.group = self.selectGroup
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension U2UChatViewController:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}
//MARK:- Collection view delegate and datasource methods
extension U2UChatViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return u2uHomeVM.friends.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionDepartment.dequeueReusableCell(withReuseIdentifier: U2UClass_identifire.U2UChatCollectionViewCell, for: indexPath) as! U2UChatCollectionViewCell
        cell.friendDetails = u2uHomeVM.friends[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: 163)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
//MARK:- TextField delegate methods
extension U2UChatViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strSearch = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        if strSearch.isEmpty{
            self.onlineUser_local = u2uHomeVM.onlineUsers
        }else{
            self.onlineUser_local = u2uHomeVM.onlineUsers.filter{$0.username.lowercased().contains(strSearch.lowercased())}
        }
        self.tblChatView.reloadData()
        self.collectionDepartment.reloadData()
        return true
    }
}
