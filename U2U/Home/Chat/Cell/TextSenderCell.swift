//
//  TextSenderCell.swift
//  Wespher
//
//  Created by mac on 30/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class TextSenderCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTextMessage : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var btnEmoji : UIButton!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    var messageDetails:Realm_Message?{
        didSet{
            lblTextMessage.text = messageDetails?.message
            imgUser.sd_setImage(with: URL(string: messageDetails?.img ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            
            //Date_Formatter.timeZone = TimeZone(abbreviation: "UTC")
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            
            Date_Formatter.dateFormat = "hh:mm a"
            lblTime.text = Date_Formatter.string(from: date)
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Prepare layout
    
}

//MARK:- Tableview cell
class TextMeCell : UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTextMessage : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var imgCheckUncheck : UIImageView!
    @IBOutlet weak var viewContain : UIView!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgSend : UIImageView!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var messageDetails:Realm_Message?{
        didSet{
            lblTextMessage.attributedText = messageDetails?.message?.htmlToAttributedString
            if messageDetails?.chat_name == nil{
                self.imgSend.image = UIImage(named: "time", in: Bundle(identifier: KBundleIdentifier), compatibleWith: nil)
            }else{
                self.imgSend.image = UIImage(named: "doubleCheck", in: Bundle(identifier: KBundleIdentifier), compatibleWith: nil)
            }
            //Date_Formatter.timeZone = TimeZone(abbreviation: "UTC")
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            
            Date_Formatter.dateFormat = "hh:mm a"
            lblTime.text = Date_Formatter.string(from: date)
        }
    }
    //MARK:- preparelayout
    
}
