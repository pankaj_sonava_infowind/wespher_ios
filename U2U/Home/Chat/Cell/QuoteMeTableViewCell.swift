//
//  QuoteMeTableViewCell.swift
//  VKApp
//
//  Created by mac on 20/01/20.
//  Copyright © 2020 Sushobhit. All rights reserved.
//

import UIKit

class QuoteMeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTextMessage : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var imgCheckUncheck : UIImageView!
    @IBOutlet weak var viewContain : UIView!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgSend : UIImageView!
    @IBOutlet weak var lblQuotedTextMessage: UILabel!
    @IBOutlet weak var lblQuotedUserNameTime: UILabel!
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool){
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
