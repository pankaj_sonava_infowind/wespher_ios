//
//  AudioTableViewCell.swift
//  Wespher
//
//  Created by mac on 05/08/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit
import AVFoundation

class AudioTableViewMeCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var lblUserTime : UILabel!
    @IBOutlet weak var viewDownload : UIView!
    @IBOutlet weak var btnDownload : UIButton!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var btnPlayAudio : UIButton!
    @IBOutlet fileprivate weak var playbackSlider : UISlider!
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    //MARK:- Variables
    var timeer: Timer?
    var audioPlayer: AVPlayer?
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    override func prepareForReuse() {
        DispatchQueue.main.async {
            self.btnPlayAudio.isSelected = false
            self.playbackSlider.value = 0.0
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.timeer?.invalidate()
            self.timeer = nil
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var messageDetails:Realm_Message?{
        didSet{
            btnDownload.tag = indexPath.row
            btnPlayAudio.tag = indexPath.row
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            Date_Formatter.dateFormat = "hh:mm a"
            lblUserTime.text = Date_Formatter.string(from: date)
                
            var media = ""
            if messageDetails!.media == nil{
                media = messageDetails!.message!
            }else{
                media = messageDetails!.media!
            }
            btnDownload.layer.setValue(media, forKey: "media_url")
            if let urlImg = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                if !urlImg.isEmpty {
                    self.btnDownload.loadingIndicator(false)
                    // self.imgChat.image = UIImage(contentsOfFile: urlImg)
                    self.viewDownload.isHidden = true
                    self.btnPlayAudio.isHidden = false
                } else {
                    self.viewDownload.isHidden = false
                    self.btnPlayAudio.isHidden = true
                }
            }
        }
    }
    @IBAction fileprivate func btnActionPlay(_ sender:UIButton) {
        if sender.isSelected == true {
                   sender.isSelected = false
                   audioPlayer?.pause()
               } else {
                   sender.isSelected = true
                   self.playbackSlider.value = 0
            var media = ""
            if messageDetails!.media == nil{
                media = messageDetails!.message!
            }else{
                media = messageDetails!.media!
            }
                   if let _ = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                       let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                       let playerItem = AVPlayerItem(url: URL(fileURLWithPath: documentsPath.appendingFormat("/\(FileDirectoryConstant.directoryName)/\(media.split(separator: "/").last!)")))
                       audioPlayer = AVPlayer(playerItem: playerItem)
                       audioPlayer?.play()
                   }
                   self.playbackSlider.addTarget(self, action: #selector(self.playbackSliderValueChanged(_:)), for: .valueChanged)
                   timeer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
               }
    }
    
    @objc func updateTime(_ timer: Timer) {
        if audioPlayer == nil {
            self.playbackSlider.value = 0.0
            return
        }
        self.playbackSlider.value = Float(CMTimeGetSeconds((audioPlayer?.currentTime())!)) / Float(CMTimeGetSeconds((self.audioPlayer?.currentItem?.asset.duration)!))
    }
    
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider) {
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        audioPlayer!.seek(to: targetTime)
        if audioPlayer!.rate == 0 {
            audioPlayer?.play()
        }
    }
    
}

class AudioTableViewSenderCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgChat : UIImageView!
    @IBOutlet weak var lblUserTime : UILabel!
    @IBOutlet weak var viewDownload : UIView!
    @IBOutlet weak var btnDownload : UIButton!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var btnPlayAudio : UIButton!
    @IBOutlet fileprivate weak var playbackSlider : UISlider!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCkeckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    var timeer: Timer?
    var audioPlayer: AVPlayer?
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        DispatchQueue.main.async {
            self.btnPlayAudio.isSelected = false
            self.playbackSlider.value = 0.0
            self.audioPlayer?.pause()
            self.audioPlayer = nil
            self.timeer?.invalidate()
            self.timeer = nil
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var messageDetails:Realm_Message?{
        didSet{
            btnDownload.tag = indexPath.row
            btnPlayAudio.tag = indexPath.row
            if let img = messageDetails?.img{
                self.imgUser.loadImageUsingCacheWithUrlString(img)
            }
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            
            Date_Formatter.dateFormat = "hh:mm a"
            lblUserTime.text = Date_Formatter.string(from: date)
            
            var media = ""
                if messageDetails!.media == nil{
                    media = messageDetails!.message!
                }else{
                    media = messageDetails!.media!
                }
            btnDownload.layer.setValue(media, forKey: "media_url")
            
                if let urlImg = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                    if !urlImg.isEmpty {
                        self.btnDownload.loadingIndicator(false)
                        // self.imgChat.image = UIImage(contentsOfFile: urlImg)
                        self.viewDownload.isHidden = true
                        self.btnPlayAudio.isHidden = false
                    } else {
                        self.viewDownload.isHidden = false
                        self.btnPlayAudio.isHidden = true
                    }
                }
        }
    }
    @IBAction fileprivate func btnActionPlay(_ sender:UIButton) {
        if sender.isSelected == true {
                   sender.isSelected = false
                   audioPlayer?.pause()
               } else {
                   sender.isSelected = true
                   self.playbackSlider.value = 0
            var media = ""
            if messageDetails!.media == nil{
                media = messageDetails!.message!
            }else{
                media = messageDetails!.media!
            }
                   if let _ = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                       let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                       let playerItem = AVPlayerItem(url: URL(fileURLWithPath: documentsPath.appendingFormat("/\(FileDirectoryConstant.directoryName)/\(media.split(separator: "/").last!)")))
                       audioPlayer = AVPlayer(playerItem: playerItem)
                       audioPlayer?.play()
                   }
                   self.playbackSlider.addTarget(self, action: #selector(self.playbackSliderValueChanged(_:)), for: .valueChanged)
                   timeer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
               }
    }
    
    @objc func updateTime(_ timer: Timer) {
        if audioPlayer == nil {
            self.playbackSlider.value = 0.0
            return
        }
        self.playbackSlider.value = Float(CMTimeGetSeconds((audioPlayer?.currentTime())!)) / Float(CMTimeGetSeconds((self.audioPlayer?.currentItem?.asset.duration)!))
    }
    
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider) {
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        audioPlayer!.seek(to: targetTime)
        if audioPlayer!.rate == 0 {
            audioPlayer?.play()
        }
    }
    
}
