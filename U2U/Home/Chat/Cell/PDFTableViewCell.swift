//
//  PDFTableViewCell.swift
//  Wespher
//
//  Created by mac on 05/08/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class PDFTableViewMeCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imgChat              : UIImageView!
    @IBOutlet weak var lblUserTime          : UILabel!
    @IBOutlet weak var viewContain          : UIView!
    @IBOutlet weak var viewDownload         : UIView!
    @IBOutlet weak var btnDownload          : UIButton!
    @IBOutlet weak var viewDate             : UIView!
    @IBOutlet weak var lblDate              : UILabel!
    @IBOutlet weak var btnViewPdf           : UIButton!
    @IBOutlet weak var imgSend              : UIImageView!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var messageDetails:Realm_Message?{
        didSet{
            btnDownload.tag = indexPath.row
            btnViewPdf.tag = indexPath.row
            if let img = messageDetails?.img{
                //self.imgUser.loadImageUsingCacheWithUrlString(img)
            }
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            
            Date_Formatter.dateFormat = "hh:mm a"
            lblUserTime.text = Date_Formatter.string(from: date)
            
            var media = ""
            if messageDetails!.media == nil{
                media = messageDetails!.message!
            }else{
                media = messageDetails!.media!
            }
            btnDownload.layer.setValue(media, forKey: "media_url")
            btnViewPdf.layer.setValue(media, forKey: "media_url")
            if let urlImg = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                if !urlImg.isEmpty {
                    self.btnDownload.loadingIndicator(false)
                    // self.imgChat.image = UIImage(contentsOfFile: urlImg)
                    self.viewDownload.isHidden = true
                } else {
                    self.viewDownload.isHidden = false
                }
            }
        }
    }
    
}

class PDFTableViewSenderCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var imgUser : UIImageView!
    @IBOutlet weak var imgChat : UIImageView!
    @IBOutlet weak var lblUserTime : UILabel!
    @IBOutlet weak var viewDownload : UIView!
    @IBOutlet weak var btnDownload : UIButton!
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var btnViewPdf : UIButton!
    
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var imgCheckBoxWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var imgStar: UIImageView!
    @IBOutlet weak var imgStarWidthConstant: NSLayoutConstraint!
    
    var isChecked:Bool = false
    var indexPath:IndexPath!
    var u2uChatDetailsVM:U2UChatDetailsViewModel!
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCheckBox.isHidden = true
        imgCheckBoxWidthConstant.constant = 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var messageDetails:Realm_Message?{
        didSet{
            btnDownload.tag = indexPath.row
            btnViewPdf.tag = indexPath.row
            if let img = messageDetails?.img{
                self.imgUser.loadImageUsingCacheWithUrlString(img)
            }
            let date = Date(timeIntervalSince1970: Double(messageDetails?.time ?? "0")!)
            Date_Formatter.dateFormat = "dd MMM yyy"
            
            let date_string = Date_Formatter.string(from: date)
            if indexPath.row == 0{
                lblDate.text = date_string
                viewDate.isHidden = false
            }else{
                let last_date = Date(timeIntervalSince1970: Double(u2uChatDetailsVM.messages[indexPath.row - 1].time ?? "0")!)
                let lastDate_string = Date_Formatter.string(from: last_date)
                if date_string == lastDate_string {
                    viewDate.isHidden = true
                    lblDate.text = ""
                }else{
                    viewDate.isHidden = false
                    lblDate.text = date_string
                }
            }
            
            Date_Formatter.dateFormat = "hh:mm a"
            lblUserTime.text = Date_Formatter.string(from: date)
            
            var media = ""
            if messageDetails!.media == nil{
                media = messageDetails!.message!
            }else{
                media = messageDetails!.media!
            }
            btnDownload.layer.setValue(media, forKey: "media_url")
            btnViewPdf.layer.setValue(media, forKey: "media_url")
            if let urlImg = FileDirectory.sharedInstance.getFileFromURL(fileName: "\(media.split(separator: "/").last!)"){
                if !urlImg.isEmpty {
                    self.btnDownload.loadingIndicator(false)
                    // self.imgChat.image = UIImage(contentsOfFile: urlImg)
                    self.viewDownload.isHidden = true
                } else {
                    self.viewDownload.isHidden = false
                }
            }
        }
    }
    
}
