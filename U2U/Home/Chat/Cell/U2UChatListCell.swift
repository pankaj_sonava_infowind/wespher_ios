//
//  U2UChatListCell.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class U2UChatListCell: UITableViewCell {
    
    //MARK:- Outlet
    @IBOutlet weak var lbl_numberOfUnreadMsg: UILabel!
    @IBOutlet weak var imgUser :                UIImageView!
    @IBOutlet weak var imgType :                UIImageView!
    @IBOutlet weak var imgAsRead :              UIImageView!
    @IBOutlet weak var imgPin :                 UIImageView!
    @IBOutlet weak var imgStared :              UIImageView!
    @IBOutlet weak var lblName :                UILabel!
    @IBOutlet weak var lblLastMessage :         UILabel!
    @IBOutlet weak var lblLastSeen :            UILabel!
    @IBOutlet weak var lblDot : 	            UILabel!
    @IBOutlet weak var viewCount :              UIView!
    
    var isselected : Bool = false
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblDot.layer.cornerRadius = self.lblDot.frame.width/2
        self.lblDot.clipsToBounds = true
        self.viewCount.layer.cornerRadius = self.viewCount.frame.width/2
        self.viewCount.clipsToBounds = true
    }
    var userDetails:Group?{
        didSet{
            imgUser.sd_setImage(with: URL(string: userDetails!.chatroom_group_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            if userDetails?.is_online == "1"{
                lblDot.backgroundColor = UIColor.light_header
            }else{
                lblDot.backgroundColor = UIColor.gray
            }
            lblName.text = userDetails?.username
            lblLastMessage.text = userDetails?.message
            if userDetails?.is_read == 1{
                imgAsRead.image = UIImage(named: "doubleCheck")
            }else{
                imgAsRead.image = UIImage(named: "imgSingleCheck")
            }
            if userDetails?.pinned == "1"{
                imgPin.isHidden = false
            }else{
                imgPin.isHidden = true
            }
            if userDetails?.starred == "1"{
                imgStared.isHidden = false
            }else{
                imgStared.isHidden = true
            }
            if userDetails?.unread_num == 0{
                lbl_numberOfUnreadMsg.isHidden = true
            }else{
                lbl_numberOfUnreadMsg.isHidden = false
                lbl_numberOfUnreadMsg.text = "\(userDetails?.unread_num ?? 0)"
            }
            lblLastSeen.text = ""
            if let time = userDetails?.time{
                if !time.isEmpty{
                    let date = Date(timeIntervalSince1970: Double(time)!)
                    Date_Formatter.dateFormat = "hh:mm a"
                    lblLastSeen.text = Date_Formatter.string(from: date)
                }
            }
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}
