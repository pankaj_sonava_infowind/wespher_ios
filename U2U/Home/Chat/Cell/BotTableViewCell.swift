//
//  BotTableViewCell.swift
//  RockStarDaddy
//
//  Created by mac on 06/11/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class BotTableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var viewOption1 : UIView!
    @IBOutlet weak var viewOption2 : UIView!
    @IBOutlet weak var viewOption3 : UIView!
    @IBOutlet weak var viewOption4 : UIView!
    
    @IBOutlet weak var lblOption1 : UILabel!
    @IBOutlet weak var lblOption2 : UILabel!
    @IBOutlet weak var lblOption3 : UILabel!
    @IBOutlet weak var lblOption4 : UILabel!
    
    @IBOutlet weak var btnOption1 : UIButton!
    @IBOutlet weak var btnOption2 : UIButton!
    @IBOutlet weak var btnOption3 : UIButton!
    @IBOutlet weak var btnOption4 : UIButton!
    
    var isChecked:Bool = false

    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Preparelayout
}
