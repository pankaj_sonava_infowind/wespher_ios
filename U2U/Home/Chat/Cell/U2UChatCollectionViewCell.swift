//
//  U2UChatCollectionViewCell.swift
//  RockStarDaddy
//
//  Created by mac on 01/10/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class U2UChatCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Outlet
    @IBOutlet fileprivate weak var imgDepartment : UIImageView!
    @IBOutlet fileprivate weak var lblDepartmentName : UILabel!
    @IBOutlet fileprivate weak var lblDepartmentDescription : UILabel!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var friendDetails:Friend?{
        didSet{
            imgDepartment.sd_setImage(with: URL(string: friendDetails!.profile_pic ?? ""), placeholderImage: UIImage(named: "User"), options: .fromCacheOnly, context: nil)
            lblDepartmentName.text = friendDetails?.firstname ?? "" + (friendDetails?.lastname ?? "")
            lblDepartmentDescription.text = ""
        }
    }
}
