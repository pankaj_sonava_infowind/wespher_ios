//
//  U2UCallsTableViewCell.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class  U2UCallsTableViewCell: UITableViewCell  {
    
    //MARK:- Outlets methods
    @IBOutlet fileprivate weak var imgUser              : UIImageView!
    @IBOutlet fileprivate weak var imgCallType          : UIImageView!
    @IBOutlet fileprivate weak var imgPin               : UIImageView!
    @IBOutlet fileprivate weak var lblName              : UILabel!
    @IBOutlet fileprivate weak var lblDate              : UILabel!
    @IBOutlet fileprivate weak var lblTime              : UILabel!
    @IBOutlet weak var lblDot                           : UILabel!
    @IBOutlet weak var btnVIdeoCall                     : UIButton!
    @IBOutlet weak var btnAudioCall                     : UIButton!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var callDetails:Call?{
        didSet{
            imgUser.sd_setImage(with: URL(string: callDetails!.profile_pic ?? "")) { (image, error, casch, url) in
                if let imgg = image{
                    DispatchQueue.main.async {
                        self.imgUser.image = imgg
                    }
                }else{
                    DispatchQueue.main.async {
                        self.imgUser.image = UIImage(named: "User")
                    }
                }
            }
            lblName.text = callDetails?.friend_name
            lblDate.text = ""
            lblTime.text = ""
            if let time = callDetails?.time{
                if !time.isEmpty{
                    let date = Date(timeIntervalSince1970: Double(time)!)
                    Date_Formatter.dateFormat = "hh:mm a"
                    lblTime.text = Date_Formatter.string(from: date)
                    Date_Formatter.dateFormat = "dd/MMM/yyyy"
                    lblDate.text = Date_Formatter.string(from: date)
                }
            }
            if callDetails?.isonline == "1"{
                lblDot.backgroundColor = UIColor.green
            }else{
                lblDot.backgroundColor = UIColor.gray
            }
        }
    }
}

