//
//  U2UEditCallsTableViewCell.swift
//  GroupTestSample
//
//  Created by mac on 20/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class U2UEditCallsTableViewCell: UITableViewCell {
    
    //MARK:- Outlets methods
    @IBOutlet fileprivate weak var imgUser          : UIImageView!
    @IBOutlet fileprivate weak var imgCallType      : UIImageView!
    @IBOutlet fileprivate weak var lblName          : UILabel!
    @IBOutlet fileprivate weak var lblDate          : UILabel!
    @IBOutlet fileprivate weak var lblTime          : UILabel!
    @IBOutlet fileprivate weak var imgPin           : UIImageView!
    @IBOutlet fileprivate weak var imgSelected      : UIImageView!
    @IBOutlet weak var lblDot                       : UILabel!
    @IBOutlet weak var btnSelected                  : UIButton!
    
    //MARK:- Override methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
