//
//  CallingPopUp.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 17/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class CallingPopUp: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var btn_rejectCall: UIButton!
    @IBOutlet weak var btn_acceptCall: UIButton!
    @IBOutlet weak var lbl_receiverName: UILabel!
    @IBOutlet weak var lbl_dialing: UILabel!
    
    var isCalling = false
    var isAudio = false
    var friend:Friend?
    var socket_delegate:Socket_Connection?
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setUpView() {
        let bundle = Bundle(identifier: KBundleIdentifier)
        bundle?.loadNibNamed("CallingPopUp", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    func callBegen(){
        if isCalling{
            btn_acceptCall.isHidden = true
            lbl_dialing.text = "Calling..."
        }else{
            btn_acceptCall.isHidden = false
            lbl_dialing.text = "Incomming call..."
        }
        //lbl_receiverName.text = friend?.firstname
    }
    @IBAction func rejectBtnAction(_ sender: UIButton) {
        socket_delegate?.rejectCall?(isCalling: isCalling)
    }
    @IBAction func acceptBtnAction(_ sender: UIButton) {
        socket_delegate?.acceptCall?()
    }
    
}
