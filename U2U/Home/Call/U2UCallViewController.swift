//
//  U2UCallViewController.swift
//  GroupTestSample
//
//  Created by mac on 02/07/19.
//  Copyright © 2019 Sushobhit. All rights reserved.
//

import UIKit

class U2UCallViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblCallList              : UITableView!
    @IBOutlet weak var txtSearch                : UITextField!
    @IBOutlet weak var viewCllection            : UIView!
    @IBOutlet weak var viewOptions              : UIView!
    @IBOutlet weak var viewSearch               : UIView!
    @IBOutlet weak var viewStack                : UIView!
    @IBOutlet weak var viewCornor               : UIView!
    
    @IBOutlet weak var collectionDepartment     : UICollectionView!
    @IBOutlet weak var constraintTbl            : NSLayoutConstraint!
    @IBOutlet weak var loader_callData: UIActivityIndicatorView!
    
    //MARK:- Variables
    var strSearch : String! = ""
    var user_call = [Call]()
    var u2uHomeVM:U2UHomeViewModel!
    //MARK:- Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewOptions.isHidden = true
        self.collectionDepartment.reloadData()
        viewSearch.layer.borderColor = #colorLiteral(red: 0.272651583, green: 0.6321746707, blue: 0.7174183726, alpha: 1)
        viewSearch.layer.borderWidth = 0.5
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search or start a new call",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        layout.itemSize = CGSize(width: self.collectionDepartment.frame.size.width/2 - 12, height: 163 )
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        collectionDepartment!.collectionViewLayout = layout
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didSwipeOnView(gestureRecognizer:)))
        self.view.addGestureRecognizer(panGesture)
        let parentVC = self.parent?.parent as! U2UHomeVC
        u2uHomeVM = parentVC.u2uHomeVM
        getUserCall()
    }
    func getUserCall() {
        if u2uHomeVM.allCalls.count == 0{
            loader_callData.startAnimating()
            u2uHomeVM.getUserCalls(viewController: self) {
                var numberOfOnlineUser = 0
                if self.u2uHomeVM.allCalls.count > 4{
                    numberOfOnlineUser = 4
                }else{
                    numberOfOnlineUser = self.u2uHomeVM.allCalls.count
                }
                DispatchQueue.main.async {
                    self.loader_callData.stopAnimating()
                    self.constraintTbl.constant = self.tblCallList.rowHeight * CGFloat(numberOfOnlineUser)
                    self.user_call = self.u2uHomeVM.allCalls
                    self.tblCallList.reloadData()
                    self.getFriends()
                }
            }
        }else{
            var numberOfOnlineUser = 0
            if self.u2uHomeVM.allCalls.count > 4{
                numberOfOnlineUser = 4
            }else{
                numberOfOnlineUser = self.u2uHomeVM.onlineUsers.count
            }
            self.user_call = self.u2uHomeVM.allCalls
            self.constraintTbl.constant = self.tblCallList.rowHeight * CGFloat(numberOfOnlineUser)
            self.tblCallList.reloadData()
            self.getFriends()
        }
    }
    func getFriends() {
        if u2uHomeVM.friends.count == 0{
            loader_callData.startAnimating()
            u2uHomeVM.getUserFriends(viewController: self) {
                DispatchQueue.main.async {
                    self.loader_callData.stopAnimating()
                    self.collectionDepartment.reloadData()
                }
            }
        }else{
            self.collectionDepartment.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblCallList.reloadData()
    }
    @objc func didSwipeOnView(gestureRecognizer: UIPanGestureRecognizer) {
        if(gestureRecognizer.state == .ended) {
            /*if case .Left = gestureRecognizer.horizontalDirection(target: self.view) {
                DispatchQueue.main.async(execute: {
                    let animation = CATransition()
                    animation.type = CATransitionType.reveal
                    animation.subtype = CATransitionSubtype.fromRight
                    animation.duration = 0.5
                    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                    self.view.layer.add(animation, forKey: nil)
                    self.view.removeFromSuperview()
                    self.perform(#selector(self.navigateView), with: nil, afterDelay: 0.0)
                })
            }*/
        }
    }
    @objc func navigateView() {
        // self.delegate?.rightAllDir()
    }
    //MARK:- Action Methods
    @IBAction func btnActionShowHideOptions(_ sender:UIButton) {
        self.txtSearch.text = ""
        self.tblCallList.reloadData()
        if sender.tag == 0 {
            self.viewOptions.isHidden = false
        } else {
            self.viewOptions.isHidden = true
        }
    }
    @IBAction fileprivate func btnActionShowHideSearch(_ sender:UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            self.txtSearch.text = ""
        } else {
            self.txtSearch.text = ""
            self.tblCallList.reloadData()
            sender.tag = 0
        }
    }
    @IBAction fileprivate func btnActionOpenEditCall(_ sender:UIButton) {
        self.txtSearch.text = ""
        self.tblCallList.reloadData()
        self.viewOptions.isHidden = true
        let objEditCallVC = self.storyboard?.instantiateViewController(withIdentifier: "EditCallsViewController") as! EditCallsViewController
        objEditCallVC.u2uHomeVM = u2uHomeVM
        self.navigationController?.pushViewController(objEditCallVC, animated: true)
    }
    
    @IBAction fileprivate func btnClearCallLog(_ sender:UIButton) {}
    
    @IBAction fileprivate func btnActionSetting(_ sender:UIButton){
        self.viewOptions.isHidden = true
        self.txtSearch.text = ""
        //self.view.makeToast("Coming soon")
        
        let objTicketSettingsVC = self.storyboard?.instantiateViewController(withIdentifier: "U2USettingsViewController") as! U2USettingsViewController
        self.navigationController?.pushViewController(objTicketSettingsVC, animated: true)
    }
    
    //MARK:- API's Calling
    func callingAPIForGetLog() {}
    
    func callingAPIForDeleteAllCalls(ids:String) {}
    
}

//MARK:- Table view delegate and datasource methods
extension U2UCallViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user_call.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblCallList.dequeueReusableCell(withIdentifier: U2UClass_identifire.U2UCallTableViewCell) as! U2UCallsTableViewCell
        
        cell.lblDot.layer.cornerRadius = cell.lblDot.frame.width/2
        cell.btnAudioCall.tag = indexPath.row
        cell.btnVIdeoCall.tag = indexPath.row
        cell.btnVIdeoCall.addTarget(self, action: #selector(self.btnActionVideo(_:)), for: UIControl.Event.touchUpInside)
        cell.btnAudioCall.addTarget(self, action: #selector(self.btnActionAudio(_:)), for: UIControl.Event.touchUpInside)
        cell.lblDot.clipsToBounds = true
        
        cell.callDetails = user_call[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tblCallList.deselectRow(at: indexPath, animated: false)
    }
    
    @objc func btnActionVideo(_ sender:UIButton) {
        //self.view.makeToast(ErrorMsg.in_progress)
    }
    
    @objc func btnActionAudio(_ sender:UIButton) {
        //self.view.makeToast(ErrorMsg.in_progress)
    }
    
}

//MARK:- Textfield deleate methods
extension U2UCallViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        strSearch = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        if strSearch.isEmpty{
            self.user_call = u2uHomeVM.allCalls
        }else{
            self.user_call = u2uHomeVM.allCalls.filter{$0.friend_name.lowercased().contains(strSearch.lowercased())}
        }
        self.tblCallList.reloadData()
        self.collectionDepartment.reloadData()
        return true
    }
    
}

//MARK:- Collection view delegate and datasource methods
extension U2UCallViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return u2uHomeVM.friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionDepartment.dequeueReusableCell(withReuseIdentifier: U2UClass_identifire.U2UCallCollectionViewCell, for: indexPath) as! U2UCallCollectionViewCell
        cell.friendDetails = u2uHomeVM.friends[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 2
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        
        return CGSize(width: size, height: 163)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}

