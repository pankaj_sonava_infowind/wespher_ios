//
//  MyFriendsViewController.swift
//  VKChatU2U
//
//  Created by mac on 08/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SVProgressHUD

class MyFriendsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var loader_friends: UIActivityIndicatorView!
    @IBOutlet weak var lbl_numberCount: UILabel!
    @IBOutlet weak var lbl_myFriend: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var lbl_noFriend: UILabel!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var u2uHomeVM = U2UHomeViewModel()
    var chatFriend:Friend?
    var isAudio = false
    var isMessage = true
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberCount.text = "\(u2uHomeVM.friends.count) contacts"
    }
    func getMoreFriend(page:Int) {
        u2uHomeVM.page = page
        loader_friends.startAnimating()
        u2uHomeVM.getUserFriends(viewController: self) {
            DispatchQueue.main.async {
                self.loader_friends.stopAnimating()
                self.lbl_numberCount.text = "\(self.u2uHomeVM.friends.count) contacts"
                self.tblView.reloadData()
                if self.u2uHomeVM.friends.count == 0{
                    self.lbl_noFriend.isHidden = false
                }else{
                    self.lbl_noFriend.isHidden = true
                }
            }
        }
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func searchBtnAction(_ sender: UIButton) {
        lbl_myFriend.isHidden = true
        lbl_numberCount.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionEditMyFriends(_ sender: Any){
        let editMyFriendVC = self.storyboard?.instantiateViewController(withIdentifier: "EditFriendViewController") as! EditFriendViewController
        editMyFriendVC.delegate = self
        editMyFriendVC.u2uHomeVM.friends = u2uHomeVM.friends
        self.navigationController?.pushViewController(editMyFriendVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return u2uHomeVM.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tblView.dequeueReusableCell(withIdentifier: "FriendCell") as! FriendsTableViewCell
        cell.friendDetails = u2uHomeVM.friends[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.friends.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreFriend(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
}
extension MyFriendsViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String) {
        DispatchQueue.main.async {
            self.u2uHomeVM.friends.removeAll()
            self.tblView.reloadData()
            self.u2uHomeVM.search = search
            self.u2uHomeVM.contact_LoadMore = true
            self.getMoreFriend(page: 1)
        }
    }
}
extension MyFriendsViewController:FriendAction{
    func tapOnAudioCall(friend: Friend) {
        isAudio = true
        isMessage = false
        friendAction(friend: friend)
    }
    func tapOnVideoCall(friend: Friend) {
        isAudio = false
        isMessage = false
        friendAction(friend: friend)
    }
    func tapOnChat(friend: Friend) {
        isMessage = true
        friendAction(friend: friend)
//        u2uHomeVM.createOrGetRoom(viewController: self, recepient_id: friend.j_id) {
//            self.chatFriend = friend
//            if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
//                DispatchQueue.main.async {
//                    self.navigateToChatScreen()
//                }
//            }else{
//                SVProgressHUD.show(withStatus: "Wait...")
//                self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
//            }
//        }
    }
    func friendAction(friend: Friend) {
        if friend.chatroom.count > 4{
            let chat_dic = ["chat_id":friend.chatroom,"chat_name":friend.chat_name,"token":U2UProxy.shared.u2uUser.token]
            u2uHomeVM.chat_room = ChatRoom(dic: chat_dic as [String : Any])
            self.chatFriend = friend
            self.isSocketAvailable()
        }else{
            createRoomAndNavigate(friend: friend)
        }
    }
    func createRoomAndNavigate(friend:Friend)  {
        u2uHomeVM.createOrGetRoom(viewController: self, recepient_id: friend.j_id) {
            self.chatFriend = friend
            self.isSocketAvailable()
        }
    }
    func isSocketAvailable() {
        if self.u2uHomeVM.u2uSocketVM.socket?.isConnected ?? false{
            DispatchQueue.main.async {
                self.navigateToChatScreen()
            }
        }else{
            SVProgressHUD.show(withStatus: "Wait...")
            self.u2uHomeVM.u2uSocketVM.socket = self.u2uHomeVM.connectSoket(viewController: self)
        }
    }
    func slectFriend(firend_id: String, index: Int) {
        
    }
    func navigateToChatScreen()  {
        let u2uChatDetailsVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UChatDetailViewController") as! U2UChatDetailViewController
        u2uChatDetailsVC.u2uChatDetailsVM.friend = self.chatFriend
        u2uChatDetailsVC.u2uChatDetailsVM.u2uHomeVM = self.u2uHomeVM
        u2uChatDetailsVC.isAudio = self.isAudio
        u2uChatDetailsVC.isMessage = self.isMessage
        self.navigationController?.pushViewController(u2uChatDetailsVC, animated: true)
    }
}
extension MyFriendsViewController:Socket_Connection{
    func socketConnected() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            self.navigateToChatScreen()
        }
    }
    func socketConnectionFailed(error: Error?) {
        U2UProxy.shared.showOnlyAlert(message: error?.localizedDescription ?? "", viewController: self)
    }
}
extension MyFriendsViewController:EditFriend{
    func updateFriendList() {
        refreshData(search: txt_search.text!)
    }
}
