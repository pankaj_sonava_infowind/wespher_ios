//
//  MainViewController.swift
//  VKChatU2U
//
//  Created by mac on 30/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class MainViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionViewFeatured: UICollectionView!
    @IBOutlet weak var collectionViewSuggested: UICollectionView!
    
    //var arr:[RestaurantModel] = []
    
    //MARK : viewLifecycle
    override func viewDidLoad(){
        super.viewDidLoad()
        //scrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height+100)
        
        //  tableView.isScrollEnabled = false;
        navigationController?.navigationBar.isHidden = true
        
        tableView.tableFooterView = UIView()
        
        data()
    }
    func data(){
        let obj = RestaurantModel.init(restaurantName:"The Yogesh Pub", restaurantType: "Indian Restaurent |", restaurantStatus: "Open")
        RestaurantModel.sharedInstance.arr.append(obj)
        let obj1 = RestaurantModel.init(restaurantName:"The Yogesh Pub", restaurantType: "Indian Restaurent |", restaurantStatus: "Close")
        RestaurantModel.sharedInstance.arr.append(obj1)
        let obj2 = RestaurantModel.init(restaurantName:"The Yogesh Pub", restaurantType: "Indian Restaurent |", restaurantStatus: "Open")
        RestaurantModel.sharedInstance.arr.append(obj2)
        let obj3 = RestaurantModel.init(restaurantName:"The Yogesh Pub", restaurantType: "Indian Restaurent |", restaurantStatus: "Close")
        RestaurantModel.sharedInstance.arr.append(obj3)
        let obj4 = RestaurantModel.init(restaurantName:"The Yogesh Pub", restaurantType: "Indian Restaurent |", restaurantStatus: "Open")
        RestaurantModel.sharedInstance.arr.append(obj4)
    }
    
    @IBAction func actionEditProvider(_ sender: Any)
    {
//        let editProviderCon = self.storyboard?.instantiateViewController(withIdentifier: "EditMyProviderViewController") as! EditMyProviderViewController
//        self.navigationController?.pushViewController(editProviderCon, animated: true)
    }
    @IBAction func actionEditFeaturedProvider(_ sender: Any)
    {
//        let editProviderCon = self.storyboard?.instantiateViewController(withIdentifier: "EditMyProviderViewController") as! EditMyProviderViewController
//        editProviderCon.idEdit = 1
//        self.navigationController?.pushViewController(editProviderCon, animated: true)
    }
    
    @IBAction func actionEditSuggestedProvider(_ sender: Any)
    {
//        let editProviderCon = self.storyboard?.instantiateViewController(withIdentifier: "EditMyProviderViewController") as! EditMyProviderViewController
//        editProviderCon.idEdit = 2
//        self.navigationController?.pushViewController(editProviderCon, animated: true)
    }
    @IBAction func actionSeeaAllMyProvider(_ sender: Any)
    {
//        let myProviderVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProviderViewController") as! MyProviderViewController
//
//        self.navigationController?.pushViewController(myProviderVC, animated: true)
    }
    @IBAction func actionSeeMoreFeaturedProvider(_ sender: Any)
    {
//        let myProviderVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProviderViewController") as! MyProviderViewController
//        myProviderVC.id = 1
//
//        self.navigationController?.pushViewController(myProviderVC, animated: true)
    }
    
    @IBAction func actionSeeMoreSuggestedProvider(_ sender: Any)
    {
//        let myProviderVC = self.storyboard?.instantiateViewController(withIdentifier: "MyProviderViewController") as! MyProviderViewController
//        myProviderVC.id = 2
//
//        self.navigationController?.pushViewController(myProviderVC, animated: true)
    }
    //MARK : TableviewMethods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return RestaurantModel.sharedInstance.arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantTableViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = RestaurantModel.sharedInstance.arr[indexPath.row]
        
        cell.lblRName.text = obj.restaurantName
        cell.lblRType.text = obj.restaurantType
        
        let rStatus = obj.restaurantStatus
        if rStatus == "Close"
        {
            cell.lblRStatus.textColor = .red
            cell.lblRStatus.text = obj.restaurantStatus
        }
        else
        {
            cell.lblRStatus.text = obj.restaurantStatus
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantCell") as! RestaurantTableViewCell
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
//        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
//        providerDetailVC.fromView = "provider"
//
//        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    
    //MARK : CollectionviewMethods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return RestaurantModel.sharedInstance.arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == self.collectionViewFeatured
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCell", for: indexPath) as! RestaurantCollectionViewCell
            
            let obj = RestaurantModel.sharedInstance.arr[indexPath.row]
            
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            cell.lblRName.text = obj.restaurantName
            cell.lblRType.text = obj.restaurantType
            
            let rStatus = obj.restaurantStatus
            if rStatus == "Close"
            {
                cell.lblRStatus.textColor = .red
                cell.lblRStatus.text = obj.restaurantStatus
            }
            else
            {
                cell.lblRStatus.text = obj.restaurantStatus
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedCell", for: indexPath) as! RestaurantCollectionViewCell
            
            let obj = RestaurantModel.sharedInstance.arr[indexPath.row]
            
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1
            
            cell.lblRName.text = obj.restaurantName
            cell.lblRType.text = obj.restaurantType
            
            let rStatus = obj.restaurantStatus
            if rStatus == "Close"
            {
                cell.lblRStatus.textColor = .red
                cell.lblRStatus.text = obj.restaurantStatus
            }
            else
            {
                cell.lblRStatus.text = obj.restaurantStatus
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let providerDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderDetailViewController") as! ProviderDetailViewController
//        providerDetailVC.fromView = "provider"
//
//        self.navigationController?.pushViewController(providerDetailVC, animated: true)
    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    //    {
    //        return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height/1)
    //    }
    
    //    func collectionView(_ collectionView: UICollectionView,
    //                        layout collectionViewLayout: UICollectionViewLayout,
    //                        sizeForItemAt indexPath: IndexPath) -> CGSize {
    //
    //    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
