//
//  U2UHomeVC.swift
//  VKChatU2U
//
//  Created by mac on 31/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import UIKit

class U2UHomeVC: UIViewController{
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var sideMenuBtn: UIButton!
    @IBOutlet weak var searchbtn: UIButton!
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnFriend: UIButton!
    @IBOutlet weak var btnProvider: UIButton!
    @IBOutlet weak var btnGroup: UIButton!
    @IBOutlet weak var btnSettings: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var isHidden:Bool = false
    
    @IBOutlet weak var leadingC_ContainerView: NSLayoutConstraint!
    
    let u2uHomeVM = U2UHomeViewModel()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        //tableView.tableFooterView = UIView()

        self.navigationController?.navigationBar.isHidden = true
        let loginData = UserDefaults.standard.value(forKey: "LoginData") as! [String:Any]
        U2UProxy.shared.u2uUser = U2UUser.init(dic: loginData)
        actionHome(btnHome)
    }
    @IBAction func actionSideManuHideShow(_ sender: Any){
        if isHidden == false{
            sideView.isHidden = true
            isHidden = true
            leadingC_ContainerView.constant = -80
            return
        }
        if isHidden == true{
            sideView.isHidden = false
            isHidden = false
            leadingC_ContainerView.constant = 0
            return
        }
    }
    @IBAction func bacBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionHome(_ sender: UIButton){
        sender.setImage(UIImage(named:"HomeBlue"), for: [])
        
        //btnHome.imageView?.image = UIImage(named: "HomeBlue")
        btnProvider.imageView?.image = UIImage(named: "Provider")
        btnGroup.imageView?.image = UIImage(named: "Group")
        btnFriend.imageView?.image = UIImage(named: "Friend")

        let friendVC = self.storyboard?.instantiateViewController(withIdentifier: "U2UChatHomeVC") as! U2UChatHomeVC
        ViewEmbedder.embed(vc: friendVC, parent: self, container: containerView)
    }
    
    @IBAction func actionFriend(_ sender: UIButton){
        sender.setImage(UIImage(named:"FriendBlue"), for: [])
        btnHome.imageView?.image = UIImage(named: "Home")
        btnProvider.imageView?.image = UIImage(named: "Provider")
        btnGroup.imageView?.image = UIImage(named: "Group")
       // btnFriend.imageView?.image = UIImage(named: "FriendBlue")
        let friendVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
        ViewEmbedder.embed(vc: friendVC, parent: self, container: containerView)
        
    }
    @IBAction func actionProvider(_ sender: UIButton){
        sender.setImage(UIImage(named:"ProviderBlue"), for: [])

        btnHome.imageView?.image = UIImage(named: "Home")
        btnProvider.imageView?.image = UIImage(named: "ProviderBlue")
        btnGroup.imageView?.image = UIImage(named: "Group")
        btnFriend.imageView?.image = UIImage(named: "Friend")
        let friendVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        ViewEmbedder.embed(vc: friendVC, parent: self, container: containerView)
    }
    @IBAction func actionGroup(_ sender: UIButton){
        sender.setImage(UIImage(named:"GroupBlue"), for: [])

        btnHome.imageView?.image = UIImage(named: "Home")
        btnProvider.imageView?.image = UIImage(named: "Provider")
        btnGroup.imageView?.image = UIImage(named: "GroupBlue")
        btnFriend.imageView?.image = UIImage(named: "Friend")
        let groupVC = self.storyboard?.instantiateViewController(withIdentifier: "GroupViewController") as! GroupViewController
        ViewEmbedder.embed(vc: groupVC, parent: self, container: containerView)
    }
    
    @IBAction func actionBtn5(_ sender: UIButton){
        
    }
}
