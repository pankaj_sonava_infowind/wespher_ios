//
//  FileDirectory.swift
//  VKApp
//
//  Created by mac on 20/12/18.
//  Copyright © 2018 Sushobhit. All rights reserved.
//

import Foundation

class FileDirectory {
    
    static let sharedInstance = FileDirectory()
    
    func createDirectory(){
        let documentsPath1 = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let logsPath = documentsPath1.appendingPathComponent("WespherSDK")
        print("Test")
//        appendingPathComponent(FileDirectoryConstant.directoryName)
        do {
            try FileManager.default.createDirectory(atPath: logsPath!.path, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }
    
    func saveFileFromURL(fileData:Data?,fileName:String) {//
        //let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let dataPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(FileDirectoryConstant.directoryName)//.absoluteString
        if !FileManager.default.fileExists(atPath: dataPath.absoluteString) {
            do{
                try FileManager.default.createDirectory(at: dataPath, withIntermediateDirectories: true, attributes: nil)
                //try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil) //Create folder if not
            }catch{
                print(error.localizedDescription)
            }
        }
        let fileURL = dataPath.appendingPathComponent(fileName)//Your image name
        if let data = fileData {
            do {
                try data.write(to: fileURL, options: .atomic)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func renameFileData(newFileName:String,oldFileName:String) {
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("\(FileDirectoryConstant.directoryName)/\(oldFileName)")
            let destinationPath = documentDirectory.appendingPathComponent("\(FileDirectoryConstant.directoryName)/\(newFileName)")
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
        } catch {
        }
    }
    
    func getFileFromURL(fileName:String) -> String? {
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("/\(FileDirectoryConstant.directoryName)/\(fileName)")
        if fileManager.fileExists(atPath: imagePAth){
            return imagePAth
        } else {
            return ""
        }
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func deleteFileFromURL() {
    }
    
    func existFileFromURL() {
    }
    
}


//class FileDirectory {
//
//    static let sharedInstance = FileDirectory()
//
//    func createDirectory(){
//        let documentsPath1 = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
//        let logsPath = documentsPath1.appendingPathComponent("VKApp")
//        do {
//            try FileManager.default.createDirectory(atPath: logsPath!.path, withIntermediateDirectories: true, attributes: nil)
//        }
//        catch let error as NSError {
//            NSLog("Unable to create directory \(error.debugDescription)")
//        }
//    }
//
//    func saveFileFromURL(fileData:Data?,fileName:String){
//        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//        let documentsDirectory = paths[0]
//        let dataPath = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("VKApp").absoluteString
//        if !FileManager.default.fileExists(atPath: dataPath) {
//            try? FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: true, attributes: nil) //Create folder if not
//        }
//        let fileURL = URL(fileURLWithPath:dataPath).appendingPathComponent(fileName)//Your image name
//        print(fileURL)
//        if let data = fileData {
//            do {
//                try data.write(to: fileURL, options: .atomic)
//            } catch {
//                print("error:", error)
//            }
//        }
//    }
//
//    func renameFileData(newFileName:String,oldFileName:String){
//        do {
//            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//            let documentDirectory = URL(fileURLWithPath: path)
//            let originPath = documentDirectory.appendingPathComponent("VKApp/\(oldFileName)")
//            let destinationPath = documentDirectory.appendingPathComponent("VKApp/\(newFileName)")
//            try FileManager.default.moveItem(at: originPath, to: destinationPath)
//        } catch {
//            print(error)
//        }
//    }
//
//    func getFileFromURL(fileName:String) -> String?{
//        let fileManager = FileManager.default
//        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("/VKApp/\(fileName)")
//        if fileManager.fileExists(atPath: imagePAth){
//            return imagePAth
//        } else {
//            return ""
//        }
//    }
//
//    func getDirectoryPath() -> String {
//        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//        let documentsDirectory = paths[0]
//        print(documentsDirectory)
//        return documentsDirectory
//    }
//
//    func deleteFileFromURL(){
//    }
//
//    func existFileFromURL(){
//    }
//
//}
