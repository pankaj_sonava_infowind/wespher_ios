//
//  Message.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 02/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation

class Message:NSObject {
    let type:String!; let name:String?; let message:String?; let userid:String!
    let Do:Int!; let chat_id:String!; let chat_name:String?; let roomtype:String!
    let time:String!; let timestamp:String!; let k:String?; let img:String?
    let msg_id:String!; let local_position:String?; let local_id:String!; let ticket_id:String!
    let sendpremadeid:Int?; let qatype:String?; let qaid:String?; let is_bot:String!
    let quoted_msg_id:String!; let quoted_msg:String!; let quoted_msgTime:String!; let quoted_uid:String!
    let quoted_uname:String!; let edit_status:String!; let edit_msg_id:Int!
    let deleted_bulk_msg_id:[Any]?; let forword_msg_status:Int!
    
    init(messageDic:[String:Any]) {
        self.type = messageDic["type"] as? String
        self.name = messageDic["name"] as? String
        self.message = messageDic["message"] as? String
        self.userid = messageDic["userid"] as? String
        self.Do = messageDic["do"] as? Int
        self.chat_id = messageDic["chat_id"] as? String
        self.chat_name = messageDic["chat_name"] as? String
        self.roomtype = messageDic["roomtype"] as? String
        self.time = messageDic["time"] as? String
        self.timestamp = messageDic["timestamp"] as? String
        self.k = messageDic["k"] as? String
        self.img = messageDic["img"] as? String
        self.msg_id = messageDic["msg_id"] as? String
        self.local_position = messageDic["local_position"] as? String
        self.local_id = messageDic["local_id"] as? String
        self.ticket_id = messageDic["ticket_id"] as? String
        self.sendpremadeid = messageDic["sendpremadeid"] as? Int
        self.qatype = messageDic["qatype"] as? String
        self.qaid = messageDic["qaid"] as? String
        self.is_bot = messageDic["is_bot"] as? String
        self.quoted_msg_id = messageDic["quoted_msg_id"] as? String
        self.quoted_msg = messageDic["quoted_msg"] as? String
        self.quoted_msgTime = messageDic["quoted_msgTime"] as? String
        self.quoted_uid = messageDic["quoted_uid"] as? String
        self.quoted_uname = messageDic["quoted_uname"] as? String
        self.edit_status = messageDic["edit_status"] as? String
        self.edit_msg_id = messageDic["edit_msg_id"] as? Int
        self.deleted_bulk_msg_id = messageDic["deleted_bulk_msg_id"] as? [Any]
        self.forword_msg_status = messageDic["forword_msg_status"] as? Int
    }
}
