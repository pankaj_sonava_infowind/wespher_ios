//
//  RealmMessageModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 23/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class Chat_Room: Object {
    dynamic var chat_room = ""
    dynamic var messages = List<Realm_Message>()
    override class func primaryKey() -> String? {
        return "chat_room"
    }
}

@objcMembers class Realm_Message: Object {
    dynamic var type:String?; dynamic var name:String?; dynamic var message:String?; dynamic var userid:String!
    dynamic var Do:Int!; dynamic var chat_id:String!; dynamic var chat_name:String?; dynamic var roomtype:String!
    dynamic var time:String!; dynamic var timestamp:String!; dynamic var k:String?; dynamic var img:String?
    dynamic var msg_id:String!; dynamic var local_position:String?; dynamic var local_id:String!; dynamic var ticket_id:String!
    dynamic var sendpremadeid:Int?; dynamic var qatype:String?; dynamic var qaid:String?; dynamic var is_bot:String!
    dynamic var quoted_msg_id:String!; dynamic var quoted_msg:String!; dynamic var quoted_msgTime:String!; dynamic var quoted_uid:String!
    dynamic var quoted_uname:String!; dynamic var edit_status:String!; dynamic var edit_msg_id:Int!
    dynamic var deleted_bulk_msg_id:String?; dynamic var forword_msg_status:Int!
    dynamic var local_file:String?
    dynamic var media:String?
    
    func assignValue(messageDic:[String:Any]) {
        self.type = messageDic["type"] as? String ?? messageDic["msgtype"] as? String
        self.name = messageDic["name"] as? String
        self.message = messageDic["message"] as? String
        self.userid = messageDic["userid"] as? String ?? messageDic["iduser"] as? String
        self.Do = messageDic["do"] as? Int
        self.chat_id = messageDic["chat_id"] as? String ?? messageDic["room"] as? String
        self.chat_name = messageDic["chat_name"] as? String
        self.roomtype = messageDic["roomtype"] as? String ?? messageDic["rmtype"] as? String
        self.time = messageDic["time"] as? String
        self.timestamp = messageDic["timestamp"] as? String
        self.k = messageDic["k"] as? String
        self.img = messageDic["img"] as? String
        self.msg_id = messageDic["msg_id"] as? String
        self.local_position = messageDic["local_position"] as? String
        self.local_id = messageDic["local_id"] as? String
        self.ticket_id = messageDic["ticket_id"] as? String
        self.sendpremadeid = messageDic["sendpremadeid"] as? Int
        self.qatype = messageDic["qatype"] as? String
        self.qaid = messageDic["qaid"] as? String ?? messageDic["quoted_id"] as? String
        self.is_bot = messageDic["is_bot"] as? String
        self.quoted_msg_id = messageDic["quoted_msg_id"] as? String
        self.quoted_msg = messageDic["quoted_msg"] as? String
        self.quoted_msgTime = messageDic["quoted_msgTime"] as? String
        self.quoted_uid = messageDic["quoted_uid"] as? String
        self.quoted_uname = messageDic["quoted_uname"] as? String
        self.edit_status = messageDic["edit_status"] as? String
        self.edit_msg_id = messageDic["edit_msg_id"] as? Int
        self.deleted_bulk_msg_id = messageDic["deleted_bulk_msg_id"] as? String
        self.forword_msg_status = messageDic["forword_msg_status"] as? Int
        self.local_file = messageDic["local_file"] as? String
        self.media = messageDic["media"] as? String
    }
}
