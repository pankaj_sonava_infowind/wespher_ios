//
//  RestaurentClass.swift
//  VKChatU2U
//
//  Created by mac on 30/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import UIKit

class RestaurantModel{
    static var sharedInstance = RestaurantModel()
    var arr:[RestaurantModel] = []
    
    //var imgView:UIImage?
    var restaurantName:String = ""
    var restaurantType:String = ""
    var restaurantStatus:String = ""
    //var messageImg:UIImage?
    
    init() {
        
    }
    
    init(restaurantName:String,restaurantType:String,restaurantStatus:String)
    {
        self.restaurantName = restaurantName
        self.restaurantType = restaurantType
        self.restaurantStatus = restaurantStatus
    }
    
}
