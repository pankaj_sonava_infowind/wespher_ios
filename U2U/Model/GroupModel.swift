//
//  GroupModel.swift
//  VKChatU2U
//
//  Created by mac on 06/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Group {
    let username:String!
    let firstname:String?
    let lastname:String?
    let ID:String!
    let profile_pic:String?
    let is_online:String!
    let pinned:String!
    let starred:String!
    let chatroom:String!
    let chatroom_id:String!
    let chatroom_name:String!
    let chatroom_owner_id:String!
    let chatroom_owner_name:String!
    let chatroom_created_time:String!
    let chatroom_humano:String!
    let chatroom_group_pic:String!
    let type:String!
    let is_bot:Int!
    let fabid:Int!
    let is_open:Int!
    let is_dept:Int!
    let dept_id:Int!
    let chatroom_members:[Group_member]!
    let message:String!
    let message_type:String!
    let message_is_deleted:String!
    let time:String!
    let unread_num:Int!
    let is_read:Int!
    let liked:String!
    init(dic:[String:JSON]) {
        self.username = dic["username"]?.string!
        self.firstname = dic["firstname"]?.string
        self.lastname = dic["lastname"]?.string
        self.ID = dic["ID"]?.string!
        self.profile_pic = dic["profile_pic"]?.string
        self.is_online = dic["is_online"]?.string
        self.pinned = dic["pinned"]?.string
        self.starred = dic["starred"]?.string
        self.chatroom = dic["chatroom"]?.string
        self.chatroom_id = dic["chatroom_id"]?.string
        self.chatroom_name = dic["chatroom_name"]?.string
        self.chatroom_owner_id = dic["chatroom_owner_id"]?.string
        self.chatroom_owner_name = dic["chatroom_owner_name"]?.string
        self.chatroom_created_time = dic["chatroom_created_time"]?.string
        self.chatroom_humano = dic["chatroom_humano"]?.string
        self.chatroom_group_pic = U2U_ApiManager.image_base_url + (dic["chatroom_group_pic"]?.string ?? "")
        self.type = dic["type"]?.string
        self.is_bot = dic["is_bot"]?.int
        self.fabid = dic["fabid"]?.int
        self.is_open = dic["is_open"]?.int
        self.is_dept = dic["is_dept"]?.int
        self.dept_id = dic["dept_id"]?.int
        var grp_member = [Group_member]()
        if let members = dic["chatroom_members"]?.array{
            for member in members{
                let mem_dic = member.dictionary
                grp_member.append(Group_member.init(dic: mem_dic!))
            }
        }
        self.chatroom_members = grp_member
        self.message = dic["message"]?.string
        self.message_type = dic["message_type"]?.string
        self.message_is_deleted = dic["message_is_deleted"]?.string
        self.time = dic["time"]?.string
        self.unread_num = dic["unread_num"]?.int
        self.is_read = dic["is_read"]?.int
        self.liked = dic["liked"]?.string
    }
}
struct Group_member {
    let user_name:String!
    let user_id:String!
    let status:String!
    
    init(dic:[String:JSON]) {
        self.user_name = dic["user_name"]?.string
        self.user_id = dic["user_id"]?.string
        self.status = dic["status"]?.string
    }
}
