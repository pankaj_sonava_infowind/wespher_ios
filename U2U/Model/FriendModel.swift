//
//  FriendModel.swift
//  VKChatU2U
//
//  Created by mac on 03/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Friend {
    let username:String!
    let firstname:String?
    let lastname:String?
    let ID:String!
    let profile_pic:String?
    let j_id:String!
    let id:String!
    let target_id:String!
    let created:String!
    let actor_id:String!
    let chatroom:String!
    let chatroom_id:String!
    let chat_name:String!
    var pined:Int!
    var stared:Int!
    var online:String!
    init(dic:[String:JSON]) {
        self.username = dic["username"]?.string!
        self.firstname = dic["firstname"]?.string
        self.lastname = dic["lastname"]?.string
        self.ID = dic["ID"]?.string!
        self.profile_pic = dic["profile_pic"]?.string
        self.j_id = dic["j_id"]?.string!
        self.id = dic["id"]?.string!
        self.target_id = dic["target_id"]?.string!
        self.created = dic["created"]?.string!
        self.actor_id = dic["actor_id"]?.string!
        self.chatroom = dic["chatroom"]?.string!
        self.chatroom_id = dic["chatroom_id"]?.string!
        self.chat_name = dic["chat_name"]?.string!
        if let pin = dic["pined"]?.int{
            self.pined = pin
        }
        if let pin = dic["pined"]?.string{
            self.pined = Int(pin)!
        }
        if let star = dic["stared"]?.int{
            self.stared = star
        }
        if let star = dic["stared"]?.string{
            self.stared = Int(star)!
        }
        if let onln = dic["online"]?.int{
            self.online = "\(onln)"
        }
        if let onln = dic["online"]?.string{
            self.online = onln
        }
    }
}
