//
//  CallModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Call {
    let id:String!
    let room:String?
    let room_id:String?
    let time:String!
    let friend_id:String?
    let friend_username:String!
    let friend_name:String!
    let calltype:String!
    let callstatus:String!
    let isonline:String!
    let is_open:Int!
    let profile_pic:String!
    
    init(dic:[String:JSON]) {
        self.id = dic["id"]?.string!
        self.room = dic["room"]?.string
        self.room_id = dic["room_id"]?.string
        self.time = dic["time"]?.string!
        self.profile_pic = dic["profile_pic"]?.string
        self.friend_id = dic["friend_id"]?.string
        self.friend_username = dic["friend_username"]?.string
        self.friend_name = dic["friend_name"]?.string
        self.calltype = dic["calltype"]?.string
        self.callstatus = dic["callstatus"]?.string
        self.isonline = dic["isonline"]?.string
        self.is_open = dic["is_open"]?.int
    }
}
