//
//  ContactModel.swift
//  VKChatU2U
//
//  Created by mac on 30/12/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Contact {
    let username:String!
    let firstname:String?
    let lastname:String?
    let ID:String!
    let id:String?
    let profile_pic:String?
    let j_id:String!
    let is_online:Int!
    var is_request:Int!
    init(dic:[String:JSON]) {
        self.username = dic["username"]?.string!
        self.firstname = dic["firstname"]?.string
        self.lastname = dic["lastname"]?.string
        self.ID = dic["ID"]?.string!
        self.profile_pic = dic["profile_pic"]?.string
        self.j_id = dic["j_id"]?.string!
        self.is_online = dic["is_online"]?.int
        self.is_request = dic["is_request"]?.int
        self.id = dic["id"]?.string
    }
}
