//
//  U2UUserModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation

struct U2UUser {
    let user_id:String
    let member_id:String
    let username:String
    let token:String
    let page_id:String
    init(dic:[String:Any]) {
        self.user_id = dic["user_id"] as? String ?? ""
        self.username = dic["username"] as? String ?? ""
        self.member_id = dic["member_id"] as? String ?? ""
        self.token = dic["token"] as? String ?? ""
        self.page_id = dic["page_id"] as? String ?? ""
    }
}
