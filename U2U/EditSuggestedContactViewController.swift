//
//  EditSuggestedContactViewController.swift
//  VKChatU2U
//
//  Created by mac on 09/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class EditSuggestedContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var lbl_numberOfcontact: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var txt_search: UITextField!
    @IBOutlet weak var loader_Contact: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noContact: UILabel!
    @IBOutlet weak var tblViewEditSuggestedContacts: UITableView!
    @IBOutlet weak var imgSelectedUnselectedAll: UIImageView!
    @IBOutlet weak var viewSearch: UIView!
    
    @IBOutlet weak var btn_all: UIButton!
    var selectedContacts = [String]()
    
    var u2uHomeVM = U2UHomeViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSearch.isHidden = true
        txt_search.placeHolderColor(text: "Search", color: .white)
        lbl_numberOfcontact.text = "\(u2uHomeVM.suggestedContacts.count) contacts"
        tblViewEditSuggestedContacts.tableFooterView = UIView()

       self.navigationController?.navigationBar.isHidden = true
    }
    func getMoreSuggestedContact(page:Int) {
        u2uHomeVM.page = page
        loader_Contact.startAnimating()
        u2uHomeVM.getUserSuggestedContacts(viewController: self) {
            DispatchQueue.main.async {
                self.loader_Contact.stopAnimating()
                self.lbl_numberOfcontact.text = "\(self.u2uHomeVM.suggestedContacts.count) contacts"
                self.tblViewEditSuggestedContacts.reloadData()
                if self.u2uHomeVM.suggestedContacts.count == 0{
                    self.lbl_noContact.isHidden = false
                }else{
                    self.lbl_noContact.isHidden = true
                }
            }
        }
    }
    @IBAction func pinBtnAction(_ sender: UIButton) {
            if selectedContacts.count == 0{
                U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select contact", viewController: self)
                return
            }
            var unPinnedArray = [String]()
            var pinnedArray = [String]()
            
            for i in 0..<u2uHomeVM.suggestedContacts.count {
                let frnd = u2uHomeVM.suggestedContacts[i]
                for j in  0..<selectedContacts.count {
                    if selectedContacts[j] == frnd.ID {
    //                    if frnd.pined == 1{
    //                        pinnedArray.append(frnd.id)
    //                    }else{
    //                        unPinnedArray.append(frnd.id)
    //                    }
                        break
                    }
                }
            }
            //selectedContacts.removeAll()
        }
    
    @IBAction func addFriendBtnAction(_ sender: UIButton) {
        if selectedContacts.count == 0{
            U2UProxy.shared.showOnlyAlert(title: "Alert!", message: "Please select contact", viewController: self)
            return
        }
        let ids = selectedContacts.joined(separator: ",")
        u2uHomeVM.sendFriendRequest(viewController: self, target_user_id: ids) { (status) in
            DispatchQueue.main.async {
                if status == "success"{
                    self.refreshData(search: self.txt_search.text!)
                }
            }
        }
    }
    @IBAction func notificationBtnAction(_ sender: UIButton) {
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        lbl_title.isHidden = true
        lbl_numberOfcontact.isHidden = true
        viewSearch.isHidden = false
    }
    @IBAction func actionBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSelectedUnselectedAll(_ sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
            imgSelectedUnselectedAll.image = UIImage(named: "unselected")
            selectedContacts.removeAll()
        }else{
            sender.isSelected = true
            imgSelectedUnselectedAll.image = UIImage(named: "allSelected")
            selectedContacts = u2uHomeVM.suggestedContacts.map{$0.ID}
        }
        tblViewEditSuggestedContacts.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return u2uHomeVM.suggestedContacts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tblViewEditSuggestedContacts.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! AllChildTableViewCell
        cell.imgSelectUnselectCell.tag = indexPath.row
        cell.selectedContacts = selectedContacts
        cell.contactDetals = u2uHomeVM.suggestedContacts[indexPath.row]
        cell.delegate = self
        if indexPath.row == u2uHomeVM.suggestedContacts.count-1{
            if u2uHomeVM.contact_LoadMore{
                getMoreSuggestedContact(page: u2uHomeVM.page + 1)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
}
extension EditSuggestedContactViewController:ContactSelection{
    func selectContact(contact_id: String, index: Int) {
        if selectedContacts.contains(contact_id){
            let index = selectedContacts.firstIndex(of: contact_id)
            selectedContacts.remove(at: index!)
        }else{
            selectedContacts.append(contact_id)
        }
        tblViewEditSuggestedContacts.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
        if selectedContacts.count == u2uHomeVM.suggestedContacts.count{
            btn_all.isSelected = true
            imgSelectedUnselectedAll.image = UIImage(named: "allSelected")
        }else{
            btn_all.isSelected = false
            imgSelectedUnselectedAll.image = UIImage(named: "unselected")
        }
    }
}
extension EditSuggestedContactViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        refreshData(search: textField.text!)
    }
    func refreshData(search:String)  {
        u2uHomeVM.suggestedContacts.removeAll()
        tblViewEditSuggestedContacts.reloadData()
        u2uHomeVM.search = search
        u2uHomeVM.contact_LoadMore = true
        getMoreSuggestedContact(page: 1)
    }
}
