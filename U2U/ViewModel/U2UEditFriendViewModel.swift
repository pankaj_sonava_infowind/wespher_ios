//
//  U2UEditFriendViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 05/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD

class U2UEditFriendViewModel: NSObject {
    func unFriendRequest(viewController:UIViewController,request_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "request_id":request_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.unFriend_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
    func unPinnedRequest(viewController:UIViewController,request_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "request_ids":request_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.unPinned_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
    func pinnedRequest(viewController:UIViewController,request_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "request_ids":request_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.pinned_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
}
