//
//  U2UEditGroupViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 08/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD

class U2UEditGroupViewModel: NSObject {

    func pinnedRequest(viewController:UIViewController,chat_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "chats":chat_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.pinnedGroup_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
    func likeRequest(viewController:UIViewController,group_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "group_id":group_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.like_group_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
    func dislikeRequest(viewController:UIViewController,group_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "group_id":group_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.dislike_group_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = 0
            if let dic = json?.dictionary{
                if let sts = dic["stat"]?.int{
                    status = sts
                }
                if status == 1{
                    callback()
                }else{
                    U2UProxy.shared.showOnlyAlert(message: dic["error"]?.string ?? "", viewController: viewController)
                }
            }else{
                U2UProxy.shared.showOnlyAlert(message: "Something went wrong", viewController: viewController)
            }
        }
    }
}
