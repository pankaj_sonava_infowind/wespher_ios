//
//  U2UChatDetailsViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 02/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

protocol Chat_Update {
    func localUpdate()
}
class U2UChatDetailsViewModel: NSObject {

    var u2uHomeVM:U2UHomeViewModel?
    var friend:Friend?
    var group:Group?
    var messages = List<Realm_Message>()
    var local_chat : Chat_Room!
    var chat_delegate:Chat_Update?
    
    func sendMessageToScoket(msgBody:String,localID:String,messageType:String,chat_id:String?) {
        let date = Date()//.toLocalTime()
        let timeInterval = date.timeIntervalSince1970
        let dateTimeSpent = Int(timeInterval)
        let strUserName = U2UProxy.shared.u2uUser.username
        var strChatRoom = u2uHomeVM?.chat_room?.chat_id
        if let chtId = chat_id{
            strChatRoom = chtId
        }
        let dictionary : [String:Any] =
            [Socket_Constant.msgtype:messageType,Socket_Constant.name:strUserName ,Socket_Constant.message:msgBody,Socket_Constant.iduser:U2UProxy.shared.u2uUser.user_id,Socket_Constant.room:strChatRoom ??   "",Socket_Constant.rmtype:Room_Type.rmtype,Socket_Constant.time:"\(dateTimeSpent)",Socket_Constant.token:U2UProxy.shared.u2uUser.token,Socket_Constant.local_id:localID,Socket_Constant.quoted_id:"0"]
        
        // Local store
        if !msgBody.isEmpty{
            let message = Realm_Message()
            message.assignValue(messageDic: dictionary)
            do{
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    local_chat.messages.append(message)
                    realmObj.add(local_chat)
                    self.chat_delegate?.localUpdate()
                }
            }catch{
                print("error \(error.localizedDescription)")
            }
        }
        if let theJSONData = try?  JSONSerialization.data( withJSONObject: dictionary, options: .prettyPrinted),
            let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
            print(theJSONText)
            u2uHomeVM?.u2uSocketVM.socket?.write(string: theJSONText)
        }
    }
    func sendMessageToScoket_unsentMsg(message:Realm_Message) {
            let dictionary : [String:Any] =
                [Socket_Constant.msgtype:message.type!,Socket_Constant.name:message.name! ,Socket_Constant.message:message.message!,Socket_Constant.iduser:message.userid!,Socket_Constant.room:message.chat_id ??   "",Socket_Constant.rmtype:message.roomtype!,Socket_Constant.time:message.time!,Socket_Constant.token:U2UProxy.shared.u2uUser.token,Socket_Constant.local_id:message.local_id!,Socket_Constant.quoted_id:message.qaid!]
            
            if let theJSONData = try?  JSONSerialization.data( withJSONObject: dictionary, options: .prettyPrinted),
                let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
                print(theJSONText)
                u2uHomeVM?.u2uSocketVM.socket?.write(string: theJSONText)
            }
        }
    
    // Send voice message
    func sendVoiceMessageToScoket(fileUrl:String,localID:String,messageType:String,chat_id:String?,voiceData:Data) {
        let date = Date()//.toLocalTime()
        let timeInterval = date.timeIntervalSince1970
        let dateTimeSpent = Int(timeInterval)
        let strUserName = U2UProxy.shared.u2uUser.username
        var strChatRoom = u2uHomeVM?.chat_room?.chat_id
        if let chtId = chat_id{
            strChatRoom = chtId
        }//
        let dictionary : [String:Any] =
            [Socket_Constant.msgtype:messageType,Socket_Constant.name:strUserName ,Socket_Constant.message:fileUrl,Socket_Constant.iduser:U2UProxy.shared.u2uUser.user_id,Socket_Constant.room:strChatRoom ??   "",Socket_Constant.rmtype:Room_Type.rmtype,Socket_Constant.time:"\(dateTimeSpent)",Socket_Constant.token:U2UProxy.shared.u2uUser.token,Socket_Constant.local_id:localID,Socket_Constant.quoted_id:"0"]
        
        // Local store
        if !fileUrl.isEmpty{
            let message = Realm_Message()
            message.assignValue(messageDic: dictionary)
            do{
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    local_chat.messages.append(message)
                    realmObj.add(local_chat)
                    self.chat_delegate?.localUpdate()
                }
            }catch{
                print("error \(error.localizedDescription)")
            }
        }
        myVoiceUploadRequest(voice: voiceData, timeSt: "\(dateTimeSpent)", chat_id: chat_id!, fileUrl: fileUrl, localDic: dictionary)
    }
    func myVoiceUploadRequest(voice:Data,timeSt:String,chat_id:String,fileUrl:String,localDic:[String:Any]) {
        let param = [
            "voice-recorder-token"  : U2UProxy.shared.u2uUser.token,
            "voice-recorder-userid"    : U2UProxy.shared.u2uUser.user_id,
            "voice-recorder-username"    : U2UProxy.shared.u2uUser.username,
            "voice-recorder-room" : chat_id,
            "timestamp" : timeSt
        ]
        let vc = kAppDelegate.window?.rootViewController
        U2U_ApiManager.shared.hitRequestWithData(method: "POST", end_point: U2U_ApiManager.uploadVoice_endpoint, param: param, dataKye: "voice-recorder-file", data: voice, viewcontroller: vc!) { (result) in
            
            if let voiceArr = result?["file"].string {
                let voiceUrl = voiceArr
                let voiceDataStr = voiceUrl.chopPrefix(3)
                let date = Date()//.toLocalTime()
                let timeInterval = date.timeIntervalSince1970
                let dateTimeSpent = Int(timeInterval)
                let timeStamp = "VK_\(dateTimeSpent).wav"
                var dictionary = localDic
                let message = Realm_Message()
                message.assignValue(messageDic: dictionary)

                DispatchQueue.main.async {
                    let index = self.messages.firstIndex(where: { $0.time == timeSt })
                    if let indx = index{
                        do{
                            let realmObj = try Realm()
                            try! realmObj.write { () -> Void in
                                self.messages[indx] = message
                                realmObj.add(self.local_chat)
                            }
                        }catch{
                            print("error \(error.localizedDescription)")
                        }
                    }
                }
                FileDirectory.sharedInstance.renameFileData(newFileName: "\(voiceDataStr.split(separator: "/").last ?? "")", oldFileName: "\(fileUrl.split(separator: "/").last ?? "")")
                dictionary[Socket_Constant.share_media] = voiceDataStr
                dictionary[Socket_Constant.file] = "voice_note"
                dictionary[Socket_Constant.file_names] = timeStamp
                dictionary[Socket_Constant.k] = 0
                dictionary.removeValue(forKey: "message")
                if let theJSONData = try?  JSONSerialization.data( withJSONObject: dictionary, options: .prettyPrinted),
                    let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
                    print(theJSONText)
                    self.u2uHomeVM?.u2uSocketVM.socket?.write(string: theJSONText)
                }
            }
        }
    }
    // Send voice message
    func sendImageMessageToScoket(fileUrl:String,localID:String,messageType:String,chat_id:String?,voiceData:Data) {
        let date = Date()//.toLocalTime()
        let timeInterval = date.timeIntervalSince1970
        let dateTimeSpent = Int(timeInterval)
        let strUserName = U2UProxy.shared.u2uUser.username
        var strChatRoom = u2uHomeVM?.chat_room?.chat_id
        if let chtId = chat_id{
            strChatRoom = chtId
        }//
        let dictionary : [String:Any] =
            [Socket_Constant.msgtype:messageType,Socket_Constant.name:strUserName ,Socket_Constant.message:fileUrl,Socket_Constant.iduser:U2UProxy.shared.u2uUser.user_id,Socket_Constant.room:strChatRoom ??   "",Socket_Constant.rmtype:Room_Type.rmtype,Socket_Constant.time:"\(dateTimeSpent)",Socket_Constant.token:U2UProxy.shared.u2uUser.token,Socket_Constant.local_id:localID,Socket_Constant.quoted_id:"0"]
        
        // Local store
        if !fileUrl.isEmpty{
            let message = Realm_Message()
            message.assignValue(messageDic: dictionary)
            do{
                let realmObj = try Realm()
                try! realmObj.write { () -> Void in
                    local_chat.messages.append(message)
                    realmObj.add(local_chat)
                    self.chat_delegate?.localUpdate()
                }
            }catch{
                print("error \(error.localizedDescription)")
            }
        }
        if messageType == "user_media_vid"{
            
        }else{
            myImgUploadRequest(voice: voiceData, timeSt: "\(dateTimeSpent)", chat_id: chat_id!, fileUrl: fileUrl, localDic: dictionary)
        }
    }
    func myImgUploadRequest(voice:Data,timeSt:String,chat_id:String,fileUrl:String,localDic:[String:Any]) {
        let param = [
            "share-photo-token"  : U2UProxy.shared.u2uUser.token,
            "share-photo-userid"    : U2UProxy.shared.u2uUser.user_id,
            "share-photo-username"    : U2UProxy.shared.u2uUser.username,
            "share-photo-room" : chat_id,
            "timestamp" : timeSt
        ]
        let vc = kAppDelegate.window?.rootViewController
        U2U_ApiManager.shared.hitRequestWithData(method: "POST", end_point: U2U_ApiManager.uploadImage_endPoint, param: param, dataKye: "share-photo[]", data: voice, viewcontroller: vc!) { (result) in
            
            if let voiceArr = result?["file"].array {
                if let voiceUrl = voiceArr.first?.string{
                    if voiceUrl.isEmpty{
                        return
                    }
                    let voiceDataStr = voiceUrl.chopPrefix(3)
                    let timeStamp = "VK_text.png"
                    var dictionary = localDic
                    let message = Realm_Message()
                    message.assignValue(messageDic: dictionary)

                    DispatchQueue.main.async {
                        let index = self.messages.firstIndex(where: { $0.time == timeSt })
                        if let indx = index{
                            do{
                                let realmObj = try Realm()
                                try! realmObj.write { () -> Void in
                                    self.messages[indx] = message
                                    realmObj.add(self.local_chat)
                                }
                            }catch{
                                print("error \(error.localizedDescription)")
                            }
                        }
                    }
                    //let shared_img = UIImage(data: voice) ?? UIImage()
                    FileDirectory.sharedInstance.renameFileData(newFileName: "\(voiceDataStr.split(separator: "/").last ?? "")", oldFileName: "\(fileUrl.split(separator: "/").last ?? "")")
                    dictionary[Socket_Constant.msgtype] = "8"
                    dictionary[Socket_Constant.share_media] = voiceDataStr
                    dictionary[Socket_Constant.file] = "img"
                    dictionary[Socket_Constant.file_names] = timeStamp
                    dictionary[Socket_Constant.k] = 0
                    dictionary.removeValue(forKey: "message")
                    if let theJSONData = try?  JSONSerialization.data( withJSONObject: dictionary, options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
                        print(theJSONText)
                        self.u2uHomeVM?.u2uSocketVM.socket?.write(string: theJSONText)
                    }
                }
            }
        }
    }
    func myVidUploadRequest(voice:Data,timeSt:String,chat_id:String,fileUrl:String,localDic:[String:Any]) {
        let param = [
            "share-video-token"  : U2UProxy.shared.u2uUser.token,
            "share-video-userid"    : U2UProxy.shared.u2uUser.user_id,
            "share-video-username"    : U2UProxy.shared.u2uUser.username,
            "share-video-room" : chat_id,
            "timestamp" : timeSt
        ]
        let vc = kAppDelegate.window?.rootViewController
        U2U_ApiManager.shared.hitRequestWithData(method: "POST", end_point: U2U_ApiManager.uploaadVideo_endPoint, param: param, dataKye: "share-video[]", data: voice, viewcontroller: vc!) { (result) in
            
            if let voiceArr = result?["file"].array {
                if let voiceUrl = voiceArr.first?.string{
                    if voiceUrl.isEmpty{
                        return
                    }
                    let voiceDataStr = voiceUrl.chopPrefix(3)
                    let date = Date()//.toLocalTime()
                    let timeInterval = date.timeIntervalSince1970
                    let dateTimeSpent = Int(timeInterval)
                    let timeStamp = "VK_\(dateTimeSpent).mp4"
                    var dictionary = localDic
                    let message = Realm_Message()
                    message.assignValue(messageDic: dictionary)

                    DispatchQueue.main.async {
                        let index = self.messages.firstIndex(where: { $0.time == timeSt })
                        if let indx = index{
                            do{
                                let realmObj = try Realm()
                                try! realmObj.write { () -> Void in
                                    self.messages[indx] = message
                                    realmObj.add(self.local_chat)
                                }
                            }catch{
                                print("error \(error.localizedDescription)")
                            }
                        }
                    }
                    //let shared_img = UIImage(data: voice) ?? UIImage()
                    FileDirectory.sharedInstance.renameFileData(newFileName: "\(voiceDataStr.split(separator: "/").last ?? "")", oldFileName: "\(fileUrl.split(separator: "/").last ?? "")")
                    dictionary[Socket_Constant.msgtype] = "8"
                    dictionary[Socket_Constant.share_media] = voiceDataStr
                    dictionary[Socket_Constant.file] = "vid"
                    dictionary[Socket_Constant.file_names] = timeStamp
                    dictionary[Socket_Constant.k] = 0
                    dictionary.removeValue(forKey: "message")
                    if let theJSONData = try?  JSONSerialization.data( withJSONObject: dictionary, options: .prettyPrinted),
                        let theJSONText = String(data: theJSONData, encoding: String.Encoding.utf8) {
                        print(theJSONText)
                        self.u2uHomeVM?.u2uSocketVM.socket?.write(string: theJSONText)
                    }
                }
            }
        }
    }
}

