//
//  U2USocketVM.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 01/06/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON

@objc protocol Socket_Connection {
    @objc optional func socketConnected()
    @objc optional func socketConnectionFailed(error:Error?)
    @objc optional func messageRecevied(message:Realm_Message)
    @objc optional func callReceived(message:Message)
    @objc optional func acceptCall()
    @objc optional func rejectCall(isCalling:Bool)
    @objc optional func callCanceled()
    @objc optional func callAccepted(message:Message)
    @objc optional func callRejected(message:Message)
    @objc optional func audioRecevied(message:Realm_Message)
}
 class U2USocketViewModel {
    var socket : WebSocket?
    var socket_delegate:Socket_Connection?
    func connectSocekt() -> WebSocket? {
        socket = WebSocket(url: URL(string: U2U_ApiManager.socket_url)!)
        self.socket?.delegate = self
        self.socket?.connect()
        return socket
    }
    func disConnectSocket() {
        socket?.disconnect()
        socket = nil
    }
    func sendMessage(jsonText:String) {
        self.socket?.write(string: jsonText)
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
            }
        }
        return nil
    }
}
extension U2USocketViewModel:WebSocketDelegate{
    func websocketDidConnect(socket: WebSocketClient) {
        print("Socket connected")
        //sendMessage(jsonText: "{}")
        socket_delegate?.socketConnected?()
    }
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("Failed to connect socket = " + (error?.localizedDescription ?? ""))
        socket_delegate?.socketConnectionFailed?(error: error)
    }
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        if let socketDict =  self.convertToDictionary(text: text) {
            print("Socket dictionary :- \(socketDict)")
            if let msg_type = socketDict["type"] as? String{
                if msg_type == Socket_Constant.usermsg{
                    if !(socketDict["message"] as! String).isEmpty{
                        let relM_msg = Realm_Message()
                        relM_msg.assignValue(messageDic: socketDict)
                        socket_delegate?.messageRecevied?(message: relM_msg)
                    }
                    }else if msg_type == MSG_Type.callsent{
                    if let id = socketDict["userid"] as? String{
                        if id != U2UProxy.shared.u2uUser.user_id{
                            socket_delegate?.callReceived?(message: Message.init(messageDic: socketDict))
                        }
                    }
                }else if msg_type == MSG_Type.callCanceled{
                    socket_delegate?.callCanceled?()
                }else if msg_type == MSG_Type.callAccepted{
                    socket_delegate?.callAccepted?(message: Message.init(messageDic: socketDict))
                }else if msg_type == MSG_Type.callEnded{
                    socket_delegate?.callRejected?(message: Message.init(messageDic: socketDict))
                }else if msg_type == Socket_Constant.voice_note{
                    let relM_msg = Realm_Message()
                    relM_msg.assignValue(messageDic: socketDict)
                    socket_delegate?.audioRecevied?(message: relM_msg)
                }else if msg_type == Socket_Constant.media_img{
                    let relM_msg = Realm_Message()
                    relM_msg.assignValue(messageDic: socketDict)
                    socket_delegate?.audioRecevied?(message: relM_msg)
                }else if msg_type == Socket_Constant.media_vid || msg_type == Socket_Constant.media_file{
                    let relM_msg = Realm_Message()
                    relM_msg.assignValue(messageDic: socketDict)
                    socket_delegate?.audioRecevied?(message: relM_msg)
                }
            }
        }
    }
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
}
struct ChatRoom {
    var chat_id:String!
    var token:String!
    var chat_name:String?
    init(dic:[String:JSON]) {
        self.chat_id = (dic["chat_id"]?.string) ?? ""
        self.chat_name = dic["chat_name"]?.string
        self.token = (dic["token"]?.string) ?? ""
    }
    init(dic:[String:Any]) {
        self.chat_id = dic["chat_id"] as? String
        self.chat_name = dic["chat_name"] as? String
        self.token = dic["token"] as? String
    }
}
