//
//  U2UHomeViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 19/05/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import Starscream

class U2UHomeViewModel: NSObject {
    var page = 1
    var search = ""
    var contacts = [Contact]()
    var contact_LoadMore = true
    var suggestedContacts = [Contact]()
    var friends = [Friend]()
    var friendRequest = [Friend]()
    var friendSentRequest = [Friend]()
    var groups = [Group]()
    var featuredGroups = [Group]()
    var suggestedGroups = [Group]()
    var onlineUsers = [Group]()
    var allCalls = [Call]()
    var chat_room:ChatRoom?
    var u2uSocketVM = U2USocketViewModel()
    var starred = "0"
    
    func getOnlineUser(viewController:UIViewController,callback:@escaping () -> Void) {
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "starred":starred]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.onlineUser_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let users = dic["users"]?.array{
                    for user in users{
                        self.onlineUsers.append(Group.init(dic: user.dictionary!))
                    }
                }
            }
            callback()
        }
    }
    func getUserCalls(viewController:UIViewController,callback:@escaping () -> Void) {
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.userCalls_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let calls = dic["data"]?.array{
                    for call in calls{
                        self.allCalls.append(Call.init(dic: call.dictionary!))
                    }
                }
            }
            callback()
        }
    }
    func getUserFriends(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page":"\(page)",
                      "search":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.friendList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
//                if self.page == 1{
//                    self.friends.removeAll()
//                }
                if let users = dic["users"]?.array{
                    for user in users{
                        self.friends.append(Friend.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.friends.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getUserFriendReq(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page":"\(page)",
                      "search":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.friendRequestList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
//                if self.page == 1{
//                    self.friendRequest.removeAll()
//                }
                if let users = dic["users"]?.array{
                    for user in users{
                        self.friendRequest.append(Friend.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.friendRequest.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getUserSentFriendReq(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page":"\(page)",
                      "search":search,
                      "sorting":sorting]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.sentFriendReq_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let users = dic["users"]?.array{
                    for user in users{
                        self.friendSentRequest.append(Friend.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.friendSentRequest.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getUserContacts(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "espage_id":U2UProxy.shared.u2uUser.page_id,
                     "page":"\(page)",
                     "search_contacts":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.contactList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
//                if self.page == 1{
//                    self.contacts.removeAll()
//                }
                if let users = dic["users"]?.array{
                    for user in users{
                        self.contacts.append(Contact.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.contacts.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getUserSuggestedContacts(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "page_id":U2UProxy.shared.u2uUser.page_id,
                    "page":"\(page)",
                    "search_contacts":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.suggestedContactList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
//                if self.page == 1{
//                    self.suggestedContacts.removeAll()
//                }
                if let users = dic["users"]?.array{
                    for user in users{
                        self.suggestedContacts.append(Contact.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.suggestedContacts.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func sendFriendRequest(viewController:UIViewController,target_user_id:String,callback:@escaping (_ status:String) -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "page_id":U2UProxy.shared.u2uUser.page_id,
                    "target_user_id":"\(target_user_id)"]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.sendFriendRequest_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = ""
            if let dic = json?.dictionary{
                if let sts = dic["check_stat"]?.string{
                    status = sts
                }
                callback(status)
            }
        }
    }
    func acceptFriendRequest(viewController:UIViewController,request_id:String,callback:@escaping (_ status:String) -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "request_id":request_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.acceptRequest_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = ""
            if let dic = json?.dictionary{
                status = (dic["check_stat"]?.string)!
                callback(status)
            }
        }
    }
    func rejectFriendRequest(viewController:UIViewController,request_id:String,callback:@escaping (_ status:String) -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "request_id":request_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.rejectRequest_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = ""
            if let dic = json?.dictionary{
                status = (dic["check_stat"]?.string)!
                callback(status)
            }
        }
    }
    
    // Group Tab Services
    var filter = ""
    var sorting = ""
    func getGroups(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "page":"\(page)",
                     "search":search,
                     "filter":filter,
                     "sorting":sorting]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.groupList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let users = dic["users"]?.array{
//                    if self.page == 1{
//                        self.groups.removeAll()
//                    }
                    for user in users{
                        self.groups.append(Group.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.groups.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getfeaturedGroups(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "page":"\(page)",
                     "search":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.featuredGroupList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let users = dic["users"]?.array{
//                    if self.page == 1{
//                        self.featuredGroups.removeAll()
//                    }
                    for user in users{
                        self.featuredGroups.append(Group.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.featuredGroups.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    func getSuggestedGroups(viewController:UIViewController,callback:@escaping () -> Void) {
        if !contact_LoadMore{
            return
        }
        //SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "page":"\(page)",
                     "search":search]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.suggestedGroupList_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                if let users = dic["users"]?.array{
//                    if self.page == 1{
//                        self.suggestedGroups.removeAll()
//                    }
                    for user in users{
                        self.suggestedGroups.append(Group.init(dic: user.dictionary!))
                    }
                }
                let total = dic["total"]?.int ?? 0
                if self.suggestedGroups.count >= total{
                    self.contact_LoadMore = false
                }
            }
            callback()
        }
    }
    // Get Room ID For Chatting
    func createOrGetRoom(viewController:UIViewController,recepient_id:String,callback:@escaping () -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["ui":U2UProxy.shared.u2uUser.user_id,
                     "uid":recepient_id,
                     "token":U2UProxy.shared.u2uUser.token,
                     "page_id":U2UProxy.shared.u2uUser.page_id,
                     "ticket_id":"0"]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.createRoom_endPoint, param: param, viewcontroller: viewController) { (json) in
            if let dic = json?.dictionary{
                self.chat_room = ChatRoom.init(dic: dic)
            }
            callback()
        }
    }
    func joinGroup(viewController:UIViewController,chat_id:String,callback:@escaping (_ status:String) -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        let param = ["user_id":U2UProxy.shared.u2uUser.user_id,
                    "chatid":chat_id]
        U2U_ApiManager.shared.hitRequest(method: HTTP_Method.post, end_point: U2U_ApiManager.joinGroup_endPoint, param: param, viewcontroller: viewController) { (json) in
            var status = ""
            if let dic = json?.dictionary{
                status = (dic["stat"]?.string)!
                callback(status)
            }
        }
    }
    // Connect To Socket
    func connectSoket(viewController:UIViewController) -> WebSocket? {
        u2uSocketVM.socket_delegate = viewController as? Socket_Connection
        return u2uSocketVM.connectSocekt()
    }
}
