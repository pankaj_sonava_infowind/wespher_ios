//
//  UserModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 29/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct User {
    let email:String
    let username:String
    let firstname:String?
    let lastname:String?
    init(dic:[String:JSON]) {
        self.email = dic["email"]?.string ?? ""
        self.username = dic["username"]?.string ?? ""
        self.firstname = (dic["firstname"]?.string)
        self.lastname = (dic["lastname"]?.string)
    }
    init(dic:[String:Any]) {
        self.email = dic["email"] as? String ?? ""
        self.username = dic["username"] as? String ?? ""
        self.firstname = dic["firstname"] as? String
        self.lastname = dic["lastname"] as? String
    }
}

struct User_Details {
    let user_id:String
    let member_id:String
    let username:String
    let token:String
    let page_id:String
    
    init(dic:[String:JSON]) {
        self.user_id = dic["user_id"]?.string ?? ""
        self.username = dic["username"]?.string ?? ""
        self.member_id = dic["member_id"]?.string ?? ""
        self.token = dic["token"]?.string ?? ""
        self.page_id = dic["page_id"]?.string ?? ""
    }
}
