//
//  Country_Model.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 28/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

struct Country {
    let name:String?
    let dial_code:String
    let code:String
    init(dic:[String:JSON]) {
        self.name = dic["name"]?.string
        self.dial_code = (dic["dial_code"]?.string) ?? ""
        self.code = (dic["code"]?.string) ?? ""
    }
}
