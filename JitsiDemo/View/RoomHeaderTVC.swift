//
//  RoomHeaderTVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 27/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class RoomHeaderTVC: UITableViewCell {
    @IBOutlet weak var lbl_date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
