//
//  RoomTVC.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class RoomTVC: UITableViewCell {

    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_timeBefore: UILabel!
    @IBOutlet weak var lbl_roomChar: UILabel!
    @IBOutlet weak var lbl_roomName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var room:Room?{
        didSet{
            lbl_roomName.text = room?.roomName
            if room!.roomName!.count > 1{
                lbl_roomChar.text = String(room!.roomName!.prefix(2)).uppercased()
            }else{
                lbl_roomChar.text = room?.roomName?.uppercased()
            }
            let hours = Date().hours(from: room!.startDate)
            if hours > 24{
                let dateStr = Proxy.shared.getDateStr(formate: "dd MMM yy hh:mm a", date: room!.startDate)
                lbl_timeBefore.text = dateStr
            }else{
                if hours < 1{
                    let min = Date().minutes(from: room!.startDate)
                    if min < 1{
                        let sec = Date().seconds(from: room!.startDate)
                        lbl_timeBefore.text = "\(sec) second ago"
                    }else{
                        lbl_timeBefore.text = "\(min) minute ago"
                    }
                }else{
                    lbl_timeBefore.text = "\(hours) hour ago"
                }
            }
            
            let duration = room!.endDate.seconds(from: room!.startDate)
            let hours_duration = duration/3600
            let left_seconds = duration % 3600
            let minutes_duration = left_seconds/60		 
            let second_duration = left_seconds % 60
            
            var duration_str = ""
            if hours_duration > 0{
                duration_str = "\(hours_duration):\(minutes_duration):\(second_duration)"
            }else{
                if minutes_duration > 0{
                    duration_str = "\(minutes_duration):\(second_duration)"
                }else{
                    duration_str = "\(second_duration)" + " sec"
                }
            }
            lbl_duration.text = duration_str
            
        }
    }
}

