//
//  SignUpViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 28/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class SignUpViewModel: NSObject {

    var countryList = [Country]()
    var genderArr = ["Male","Female"]
    
    func getCountryList(viewcontroller:UIViewController,callback:@escaping () -> Void){
        SVProgressHUD.show(withStatus: "Wait...")
        APIManager.shared.hitRequest(method: HTTP_Method.get, end_point: APIManager.countryList_endPoint, param: nil, viewcontroller: viewcontroller) { (json) in
            if let dic = json.dictionary{
                if let country_list = dic["data"]?.array{
                    for countryDic in country_list {
                        let cntry = Country(dic: countryDic.dictionary!)
                        self.countryList.append(cntry)
                    }
                }
                callback()
            }
        }
    }
    func registerUser(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void)  {
        SVProgressHUD.show(withStatus: "Wait...")
        APIManager.shared.hitRequest(method: HTTP_Method.post, end_point: APIManager.signUp_endPoint, param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json.dictionary{
                callback(dic)
            }
        }
    }
}
