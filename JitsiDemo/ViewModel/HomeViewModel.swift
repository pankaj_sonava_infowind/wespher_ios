//
//  HomeViewModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 29/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class HomeViewModel: NSObject {

    func getSDK_Details(viewController:UIViewController,parameter:[String:String],callback:@escaping (_ result:[String:JSON]) -> Void) {
        SVProgressHUD.show(withStatus: "Wait...")
        APIManager.shared.hitRequest(method: HTTP_Method.post, end_point: "", param: parameter, viewcontroller: viewController) { (json) in
            if let dic = json.dictionary{
                callback(dic)
            }
        }
        
    }
    
}
