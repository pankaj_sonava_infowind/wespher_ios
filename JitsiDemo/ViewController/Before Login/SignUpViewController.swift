//
//  SignUpViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 20/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol RegistrationCompleted {
    func userRegistered(emali:String,password:String)
}

class SignUpViewController: UIViewController {

    @IBOutlet weak var btn_country: UIButton!
    @IBOutlet weak var btn_dob: UIButton!
    @IBOutlet weak var btn_gender: UIButton!
    @IBOutlet weak var txt_lastName: UITextField!
    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_username: UITextField!
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var picker_view: UIPickerView!
    @IBOutlet weak var bottom_bottomView: NSLayoutConstraint!
    @IBOutlet weak var date_picker: UIDatePicker!
    
    var delegate:RegistrationCompleted?
    var selectedGender = ""{
        didSet{
            if selectedGender == "Male"{
                selectedGender = "1"
            }else{
                selectedGender = "2"
            }
        }
    }
    var selectedCountry:Country!
    var selectedDate:String!
    
    let signUpVM = SignUpViewModel()
    var selectedBtn = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetUp()
        getCountryList()
    }
    func viewSetUp() {
        txt_name.placeHolderColor(text: "Name", color: .white)
        txt_password.placeHolderColor(text: "Password *", color: .white)
        txt_username.placeHolderColor(text: "Username *", color: .white)
        txt_email.placeHolderColor(text: "Email *", color: .white)
        txt_lastName.placeHolderColor(text: "Last name", color: .white)
    }
    func getCountryList()  {
        signUpVM.getCountryList(viewcontroller: self) {
            
        }
    }
    @IBAction func logInBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func selectGenderBtnAction(_ sender: UIButton) {
        selectedBtn = sender
        showBottomView()
    }
    @IBAction func selectDOBbtnAction(_ sender: UIButton) {
        selectedBtn = sender
        showBottomView()
    }
    @IBAction func selectCountryBtnAction(_ sender: UIButton) {
        selectedBtn = sender
        showBottomView()
    }
    func showBottomView() {
        if selectedBtn == btn_dob{
            picker_view.isHidden = true
            date_picker.isHidden = false
            animateBottomView(constant: 0)
        }else{
            picker_view.isHidden = false
            date_picker.isHidden = true
            if selectedBtn == btn_gender{
                picker_view.reloadAllComponents()
                animateBottomView(constant: 0)
            }else{
                if signUpVM.countryList.count == 0{
                    signUpVM.getCountryList(viewcontroller: self) {
                        self.picker_view.reloadAllComponents()
                        self.animateBottomView(constant: 0)
                    }
                }else{
                    picker_view.reloadAllComponents()
                    animateBottomView(constant: 0)
                }
            }
        }
    }
    func animateBottomView(constant:CGFloat)  {
        UIView.animate(withDuration: 0.3) {
            self.bottom_bottomView.constant = constant
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func signUpBtnAction(_ sender: UIButton) {
        let validation = isValidUserEntry()
        if !validation.0{
            Proxy.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = ["username":txt_username.text!,
                     "password":txt_password.text!,
                     "firstname":txt_name.text!,
                     "lastname":txt_lastName.text!,
                     "email":txt_email.text!,
                     "gender":selectedGender,
                     "country":selectedCountry.code,
                     "dob":selectedDate] as! [String : String]
        signUpVM.registerUser(viewController: self, parameter: param) { result in
            let status = result["status"]?.string
            if status == "success"{
                DispatchQueue.main.async {
                    Proxy.shared.showAlertOneAction(title: Alert_Msg.Done, okayTitle: Alert_Msg.Ok, message: result["message"]?.string ?? "", viewController: self) {
                        self.delegate?.userRegistered(emali: self.txt_email.text!, password: self.txt_password.text!)
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    Proxy.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
        
    }
    @IBAction func donePickerBtnAction(_ sender: UIButton) {
        animateBottomView(constant: -350)
        if selectedBtn == btn_dob {
            let date = date_picker.date
            let dateStr = Proxy.shared.getDateStr(formate: "dd MMM yyyy", date: date)
            btn_dob.setTitle(dateStr, for: .normal)
            selectedDate = Proxy.shared.getDateStr(formate: "yyyy-MM-dd", date: date)
        }else{
            if selectedBtn == btn_gender {
                btn_gender.setTitle(signUpVM.genderArr[picker_view.selectedRow(inComponent: 0)], for: .normal)
                selectedGender = signUpVM.genderArr[picker_view.selectedRow(inComponent: 0)]
            }else{
                selectedCountry = signUpVM.countryList[picker_view.selectedRow(inComponent: 0)]
                btn_country.setTitle(signUpVM.countryList[picker_view.selectedRow(inComponent: 0)].name, for: .normal)
            }
        }
    }
    @IBAction func cancelPickerBtnAction(_ sender: UIButton) {
        animateBottomView(constant: -350)
    }
    
    
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
    
        if txt_username.text!.isEmpty{
            return (false,Error_Alert.Missing_Username)
        }
        if txt_email.text!.isEmpty{
            return (false,Error_Alert.Missing_Email)
        }
        if !Proxy.shared.isValidEmail(txt_email.text!){
            return (false,Error_Alert.Invalid_Email)
        }
        if txt_password.text!.isEmpty {
            return (false,Error_Alert.Missing_Password)
        }
        if btn_gender.titleLabel!.text! == "Gender"{
            return (false,Error_Alert.Missing_Gender)
        }
        if btn_dob.titleLabel!.text! == "Date of birth"{
            return (false,Error_Alert.Missing_DOB)
        }
        if btn_country.titleLabel!.text! == "Country"{
            return (false,Error_Alert.Missing_Country)
        }
    
        return (true,"")
    }
}
extension SignUpViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if selectedBtn == btn_gender{
            return signUpVM.genderArr.count
        }else{
            return signUpVM.countryList.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if selectedBtn == btn_gender{
            return signUpVM.genderArr[row]
        }else{
            return signUpVM.countryList[row].name
        }
    }
}
