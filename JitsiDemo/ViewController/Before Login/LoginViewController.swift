//
//  LoginViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 20/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var txt_userName: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    let loginVM = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            viewSetUp()
        if Proxy.shared.isForReister{
            Proxy.shared.isForReister = false
            let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            signUpVC.delegate = self
            navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    func viewSetUp() {
        txt_userName.placeHolderColor(text: "Username *", color: .white)
        txt_password.placeHolderColor(text: "Password *", color: .white)
    }
    @IBAction func loginBtnAction(_ sender: Any) {
        let validation = isValidUserEntry()
        if !validation.0{
            Proxy.shared.showOnlyAlert(message: validation.1, viewController: self)
            return
        }
        let param = ["username":txt_userName.text!,
                     "password":txt_password.text!]
        loginVM.loginUser(viewController: self, parameter: param) { (result) in
            let status = result["status"]?.string
            if status == "success"{
                DispatchQueue.main.async {
                    if let data = result["data"]?.dictionary{
                        let srtingDic = ["lastname":data["lastname"]?.string,
                                         "username":data["username"]?.string,
                                         "firstname":data["firstname"]?.string,
                                         "email":data["email"]?.string]
                        UserDefaults.standard.setValue(srtingDic, forKey: "Login_Data")
                        let user = User.init(dic: data)
                        Proxy.shared.loginUser = user
                        Proxy.shared.isGuestAccess = false
                        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
                        let nav = UINavigationController(rootViewController: homeVC)
                        nav.navigationBar.isHidden = true
                        self.view.window?.rootViewController = nav
                    }
                }
            }else{
                DispatchQueue.main.async {
                    Proxy.shared.showOnlyAlert(message: result["message"]?.string ?? "", viewController: self)
                }
            }
        }
    }
    @IBAction func registerBtnAction(_ sender: Any) {
        let signUpVC = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        signUpVC.delegate = self
        navigationController?.pushViewController(signUpVC, animated: true)
    }
    @IBAction func forgotPasswordBtnAction(_ sender: Any) {
    }
    @IBAction func guestLoginBtnAction(_ sender: Any) {
        let homeVC = storyboard?.instantiateViewController(withIdentifier: "UserHomeViewController") as! UserHomeViewController
        navigationController?.pushViewController(homeVC, animated: true)
    }
    // Support Method
    func isValidUserEntry() -> (Bool,String) {
        if txt_userName.text!.isEmpty{
            return (false,Error_Alert.Missing_Username)
        }
        if txt_password.text!.isEmpty {
            return (false,Error_Alert.Missing_Password)
        }
        return (true,"")
    }
}
extension LoginViewController:RegistrationCompleted{
    func userRegistered(emali: String, password: String) {
        txt_userName.text = emali
        txt_password.text = password
    }
}
