//
//  ConferenceViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import JitsiMeet
import CoreData

class ConferenceViewController: UIViewController {

    @IBOutlet weak var height_createView: NSLayoutConstraint!
    @IBOutlet weak var txt_roomName: UITextField!
    @IBOutlet weak var tbl_rooms: UITableView!
    
    var pipViewCoordinator: PiPViewCoordinator?
    var jitsiMeetView: JitsiMeetView?
    var audioOnly = true
    var isAudioMute = false
    var isVideoMute = false
    var startDate:Date?
    var roomDetail:[String:Any]?
    var rooms = [Room]()
    var dateRooms = [String:[Room]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Proxy.shared.registerTableViewCell(identifier: "RoomTVC", tableView: tbl_rooms)
        Proxy.shared.registerTableViewCell(identifier: "RoomHeaderTVC", tableView: tbl_rooms)
        txt_roomName.placeHolderColor(text: "Enter Room Name", color: .white)
        fetchRoomDetails()
    }
    func fetchRoomDetails() {
        self.rooms.removeAll()
        let manageContext = kAppDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Rooms")
        do {
            let result = try manageContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                let roomName = data.value(forKey: "roomName") as! String
                let startDate = data.value(forKey: "startDate") as! Date
                let endDate = data.value(forKey: "endDate") as! Date
                let roomDic = ["roomName":roomName,
                               "startDate":startDate,
                               "endDate":endDate] as [String : Any]
                self.rooms.append(Room.init(dic: roomDic))
            }
            DispatchQueue.main.async {
                self.tbl_rooms.reloadData()
            }
            //self.manageSectionWiseRoom()
        } catch {
            print("failed")
        }
    }
    func manageSectionWiseRoom() {
        /*for room in self.rooms{
            
        }*/
    }//
    func conferenceSettings() {
        cleanUp()
        let jitsiMeetView = JitsiMeetView()
        jitsiMeetView.delegate = self
        self.jitsiMeetView = jitsiMeetView
        let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
            //builder.serverURL = URL(string: "https://meet.jit.si")
            builder.serverURL = URL(string: "https://vc.wespher.com")
            builder.room = self.txt_roomName.text!
            builder.audioMuted = self.isAudioMute
            builder.audioOnly = self.audioOnly
            builder.videoMuted = self.isVideoMute
            builder.token = JitsiToken
            builder.welcomePageEnabled = false
        }
        jitsiMeetView.join(options)
        pipViewCoordinator = PiPViewCoordinator(withView: jitsiMeetView)
        pipViewCoordinator?.configureAsStickyView(withParentView: view)

        // animate in
        jitsiMeetView.alpha = 0
        pipViewCoordinator?.show()
        //txt_roomName.text = ""
        height_createView.constant = 0
    }
    fileprivate func cleanUp() {
        jitsiMeetView?.removeFromSuperview()
        jitsiMeetView = nil
        pipViewCoordinator = nil
    }
    @IBAction func createBtnAction(_ sender: UIButton) {
        txt_roomName.resignFirstResponder()
        conferenceSettings()
    }
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func settingsBtnAction(_ sender: Any) {
        
        let settingVC = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        settingVC.delegate = self
        navigationController?.pushViewController(settingVC, animated: true)
    }
    @IBAction func changeVideoBtnAction(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            audioOnly = false
            sender.setImage(UIImage(named: "off-icon"), for: .normal)
        }else{
            sender.tag = 0
            sender.setImage(UIImage(named: "on-icon"), for: .normal)
            audioOnly = true
        }
    }
    func saveRoomDetails()  {
        if let rd = roomDetail{
            let room = Room.init(dic: rd)
            let manageContext = kAppDelegate.persistentContainer.viewContext
            let roomEntity = NSEntityDescription.entity(forEntityName: "Rooms", in: manageContext)
            let roomMD = NSManagedObject(entity: roomEntity!, insertInto: manageContext)
            roomMD.setValue(room.roomName!, forKey: "roomName")
            roomMD.setValue(room.startDate, forKey: "startDate")
            roomMD.setValue(room.endDate, forKey: "endDate")
            do {
                try manageContext.save()
                roomDetail = nil
                txt_roomName.text = ""
                fetchRoomDetails()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
}
extension ConferenceViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RoomTVC") as! RoomTVC
        cell.selectionStyle = .none
        cell.room = rooms[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        txt_roomName.text = rooms[indexPath.row].roomName
        conferenceSettings()
    }
}
extension ConferenceViewController: JitsiMeetViewDelegate {
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        if roomDetail != nil{
            roomDetail!["endDate"] = Date()
        }
        saveRoomDetails()
        DispatchQueue.main.async {
            self.pipViewCoordinator?.hide() { _ in
                self.cleanUp()
            }
        }
    }
    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        DispatchQueue.main.async {
            self.pipViewCoordinator?.enterPictureInPicture()
        }
    }
    func conferenceJoined(_ data: [AnyHashable : Any]!) {
        roomDetail = Dictionary()
        roomDetail!["roomName"] = txt_roomName.text!
        roomDetail!["startDate"] = Date()
    }
}
extension ConferenceViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text!
        if string.isEmpty{
            text = String(text.dropLast())
        }else{
            text = text + string
        }
        if text.count > 0 {
            height_createView.constant = 138
        }else{
            height_createView.constant = 0
        }
        textField.text = text
        return false
    }
}
extension ConferenceViewController: UpdateSettings{
    func updateSettings(isAuioMute: Bool, isVideoMute: Bool) {
        self.isAudioMute = isAuioMute
        self.isVideoMute = isVideoMute
    }
}
