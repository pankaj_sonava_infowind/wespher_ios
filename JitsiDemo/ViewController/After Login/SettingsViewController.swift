//
//  SettingsViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit

protocol UpdateSettings {
    func updateSettings(isAuioMute:Bool,isVideoMute:Bool)
}

class SettingsViewController: UIViewController {

    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_displayName: UITextField!
    
    var delegate:UpdateSettings?
    var isAudioMute = true
    var isVideoMute = true
    var isAdSetting = true
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_email.leftPadding(point: 10)
        txt_displayName.leftPadding(point: 10)
    }
    @IBAction func backBtnAction(_ sender: Any) {
        delegate?.updateSettings(isAuioMute: isAudioMute, isVideoMute: isVideoMute)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func isAudioMute(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isAudioMute = false
            sender.setImage(UIImage(named: "off-icon"), for: .normal)
        }else{
            sender.tag = 0
            sender.setImage(UIImage(named: "on-icon"), for: .normal)
            isAudioMute = true
        }
    }
    @IBAction func isVideoMute(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isVideoMute = false
            sender.setImage(UIImage(named: "off-icon"), for: .normal)
        }else{
            sender.tag = 0
            sender.setImage(UIImage(named: "on-icon"), for: .normal)
            isVideoMute = true
        }
    }
    @IBAction func isShowAdvanceSettings(_ sender: UIButton) {
        if sender.tag == 0{
            sender.tag = 1
            isAdSetting = false
            sender.setImage(UIImage(named: "off-icon"), for: .normal)
        }else{
            sender.tag = 0
            sender.setImage(UIImage(named: "on-icon"), for: .normal)
            isAdSetting = true
        }
    }
}
