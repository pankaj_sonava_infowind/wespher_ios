//
//  SideMenuViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Wesphenger

class SideMenuViewController: UIViewController {

    @IBOutlet weak var lbl_register2: UILabel!
    @IBOutlet weak var lbl_register1: UILabel!
    @IBOutlet weak var btn_register: UIButton!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_logout: UIButton!
    var nav:UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewSetup()
    }
    func viewSetup(){
        if Proxy.shared.isGuestAccess{
            btn_logout.isHidden = true
            btn_login.isHidden = false
            btn_register.isHidden = false
        }else{
            btn_logout.isHidden = false
            lbl_register1.isHidden = true
            lbl_register2.isHidden = true
            btn_login.isHidden = true
            btn_register.isHidden = true
        }
    }
    @IBAction func userToStaff(_ sender: Any) {
        if Proxy.shared.isGuestAccess {
            self.navigateToLogin()
        }else{
            if Proxy.shared.user_sdk != nil{
                let login_data = ["login_token":Proxy.shared.user_sdk.token,
                                  "provider_id":Proxy.shared.user_sdk.page_id,
                                  "user_id":Proxy.shared.user_sdk.user_id,
                                  "username":Proxy.shared.user_sdk.username,
                                  "w_id":Proxy.shared.user_sdk.member_id]
                UserDefaults.standard.set(login_data, forKey: "LoginData")
                UserDefaults.standard.synchronize()
                WesphengerManager.framework_window = self.view.window
                WesphengerManager.parent_VC = self.parent
                nav?.pushViewController(WesphengerManager.loginViewPresent(), animated: true)
            }
        }
        dismissView()
    }
    @IBAction func userToUserBtnAction(_ sender: Any) {
        if Proxy.shared.isGuestAccess {
            self.navigateToLogin()
        }
        dismissView()
    }
    @IBAction func conferenceBtnAction(_ sender: Any) {
        let conferenceVC = storyboard?.instantiateViewController(withIdentifier: "ConferenceViewController") as! ConferenceViewController
        nav?.pushViewController(conferenceVC, animated: false)
        dismissView()
    }
    @IBAction func menuBtnAction(_ sender: UIButton) {
        
        dismissView()
    }
    func dismissView(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(translationX: -self.view.frame.size.width, y: 0)
        }) { (stus) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    func navigateToLogin(){
        let loginVC = Main_Storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = UINavigationController(rootViewController: loginVC)
        nav.navigationBar.isHidden = true
        self.view.window?.rootViewController = nav
    }
    @IBAction func logOUtBtnAction(_ sender: Any) {
        Proxy.shared.showAlertTwoAction(title: Alert_Msg.LogOut, okayTitle: Alert_Msg.LogOut, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.LogOut_Confirm, viewController: self) { (action) in
            if action{
                Proxy.shared.removeSession()
                self.navigateToLogin()
            }
        }
    }
    @IBAction func loginBtnAction(_ sender: Any) {
        self.navigateToLogin()
    }
    @IBAction func registerBtnAction(_ sender: Any) {
        Proxy.shared.isForReister = true
        self.navigateToLogin()
    }
}
