//
//  UserHomeViewController.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 20/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Wesphenger

class UserHomeViewController: UIViewController {

    @IBOutlet weak var lbl_registrationRequired2: UILabel!
    @IBOutlet weak var lbl_registrationRequired1: UILabel!
    @IBOutlet weak var btn_logout: UIButton!
    @IBOutlet weak var lbl_register: UILabel!
    @IBOutlet weak var lbl_userName: UILabel!
    let homeVM = HomeViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewSetup()
        if !Proxy.shared.isGuestAccess {
            getSDKdetails()
        }
    }
    func getSDKdetails()  {
        let param = ["token":Token,
                     "user":Proxy.shared.loginUser.email]
        homeVM.getSDK_Details(viewController: self, parameter: param) { (result) in
            let user = User_Details.init(dic: result)
            Proxy.shared.user_sdk = user
        }
    }
    func viewSetup(){
        if Proxy.shared.isGuestAccess{
            btn_logout.isHidden = true
        }else{
            btn_logout.isHidden = false
            lbl_register.isHidden = true
            lbl_registrationRequired1.isHidden = true
            lbl_registrationRequired2.isHidden = true
            lbl_userName.text = "Hello \(Proxy.shared.loginUser.firstname ?? "")"
        }
    }
    @IBAction func menuBtnAction(_ sender: UIButton) {
        let sideVC = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        sideVC.nav = navigationController
        Proxy.shared.presentToLeft(src: self, dst: sideVC)
    }
    @IBAction func userToStaffBtnAction(_ sender: Any) {
        if Proxy.shared.isGuestAccess {
            self.navigateToLogin()
        }else{
            if Proxy.shared.user_sdk != nil{
                let login_data = ["login_token":Proxy.shared.user_sdk.token,
                                  "provider_id":Proxy.shared.user_sdk.page_id,
                                  "user_id":Proxy.shared.user_sdk.user_id,
                                  "username":Proxy.shared.user_sdk.username,
                                  "w_id":Proxy.shared.user_sdk.user_id,
                                  "member_id":Proxy.shared.user_sdk.member_id]
                UserDefaults.standard.set(login_data, forKey: "LoginData")
                UserDefaults.standard.synchronize()
                WesphengerManager.framework_window = self.view.window
                WesphengerManager.parent_VC = self.parent
                navigationController?.pushViewController(WesphengerManager.loginViewPresent(), animated: true)
            }else{
                getSDKdetails()
            }
        }
    }
    @IBAction func userToUserBtnaction(_ sender: Any) {
        if Proxy.shared.isGuestAccess {
            self.navigateToLogin()
            return
        }
        if Proxy.shared.user_sdk == nil{
            getSDKdetails()
            return
        }
        let loginData = ["token":Proxy.shared.user_sdk.token,
                          "page_id":Proxy.shared.user_sdk.page_id,
                          "user_id":Proxy.shared.user_sdk.user_id,
                          "username":Proxy.shared.user_sdk.username,
                          "member_id":Proxy.shared.user_sdk.member_id]
        UserDefaults.standard.set(loginData, forKey: "LoginData")
        UserDefaults.standard.synchronize()
        let conferenceVC = U2U_StoryBoard.instantiateViewController(withIdentifier: "U2UHomeVC") as! U2UHomeVC
               navigationController?.pushViewController(conferenceVC, animated: true)
    }
    @IBAction func guestAccessBtnAction(_ sender: Any) {
        let conferenceVC = storyboard?.instantiateViewController(withIdentifier: "ConferenceViewController") as! ConferenceViewController
        navigationController?.pushViewController(conferenceVC, animated: false)
    }
    func navigateToLogin(){
        let loginVC = Main_Storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let nav = UINavigationController(rootViewController: loginVC)
        nav.navigationBar.isHidden = true
        self.view.window?.rootViewController = nav
    }
    @IBAction func logoutBtnAction(_ sender: Any) {
        Proxy.shared.showAlertTwoAction(title: Alert_Msg.LogOut, okayTitle: Alert_Msg.LogOut, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.LogOut_Confirm, viewController: self) { (action) in
            if action{
                Proxy.shared.removeSession()
                self.navigateToLogin()
            }
        }
    }
}
