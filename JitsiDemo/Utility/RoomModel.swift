//
//  RoomModel.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 27/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit

struct Room {
    let roomName:String?
    let startDate:Date
    let endDate:Date
    
    init(dic:[String:Any]) {
        self.roomName = dic["roomName"] as? String
        self.startDate = dic["startDate"] as! Date
        self.endDate = dic["endDate"] as! Date
    }
}
