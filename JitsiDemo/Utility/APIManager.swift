//
//  APIManager.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 28/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class APIManager {
    private init() {}
    static let shared = APIManager()
    
    static let base_url = "https://rockstardaddy.com/moodle30"
    static let sdk_details_url = "https://wespher.rockstardaddy.com/wespher_support_system/ws/app.php?act=sso"
    
    static let login_endPoint = "/app_api/login.php"
    static let signUp_endPoint = "/app_api/register.php"
    static let countryList_endPoint = "/app_api/country_list.php"
    
    func hitRequest(method:String,end_point:String,param:[String:String]?,viewcontroller:UIViewController,callback:@escaping (_ json:JSON) -> Void) {
        
        var urlStr = APIManager.base_url + end_point
        if end_point.isEmpty{
            urlStr = APIManager.sdk_details_url
        }
        
        var request = URLRequest(url: URL(string: urlStr)!)
        request.httpMethod = method
        if let pr = param{
            let keys = Array(pr.keys)
            var postString = ""
            for key in keys {
                postString = postString + "\(key)=\(pr[key] ?? "")&"
            }
            let postData = postString.data(using: .utf8)
            request.httpBody = postData
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
            SVProgressHUD.dismiss()
            }
            if error == nil{
                if let json = try? JSON(data: data!){
                    callback(json)
                }else{
                    DispatchQueue.main.async {
                        Proxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: Alert_Msg.Wrong, viewController: viewcontroller) { (isTrue) in
                            if isTrue{
                                self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                            }
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    Proxy.shared.showAlertTwoAction(title: Alert_Msg.Opps, okayTitle: Alert_Msg.TryAgain, cancelTitle: Alert_Msg.Cancel, message: error!.localizedDescription, viewController: viewcontroller) { (isTrue) in
                        if isTrue{
                            self.hitRequest(method: method, end_point: end_point, param: param, viewcontroller: viewcontroller, callback: callback)
                        }
                    }
                }
            }
        }
        task.resume()
        
    }
    
}
