//
//  Constant.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 27/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import Foundation
import UIKit


let Date_Formatter = DateFormatter()
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let Main_Storyboard = UIStoryboard(name: "Main", bundle: nil)
let U2U_StoryBoard = UIStoryboard(name: "U2U", bundle: nil)
//let Token = "8376536684"
let Token = "4247269180"
let JitsiToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImppdHNpL2N1c3RvbV9rZXlfbmFtZSJ9.eyJjb250ZXh0Ijp7InVzZXIiOnsiYXZhdGFyIjoiIiwibmFtZSI6IlVzZXIiLCJlbWFpbCI6IiIsImlkIjoiYWJjZDphMWIyYzMtZDRlNWY2LTBhYmMxLTIzZGUtYWJjZGVmMDExMSJ9LCJncm91cCI6ImExMjMtMTIzLTQ1Ni03ODkifSwiYXVkIjoiaml0c2kiLCJpc3MiOiJteV9jbGllbnQiLCJzdWIiOiJtZWV0LmppdC5zaSIsInJvb20iOiIqIiwiZXhwIjoxNTAwMDA2OTIzfQ.OXYUbSaXkn2udKCFQ7JN8v7Hu7rVemegdmpckeFaxZA"

struct Error_Alert {
    static let Missing_Username = "Please enter user name."
    static let Missing_Email = "Please enter email address."
    static let Missing_Password = "Please enter password."
    static let Missing_Gender = "Please select your gender."
    static let Missing_DOB = "Please select date of birth."
    static let Missing_Country = "Please select your country."
    static let Invalid_Email = "Please enter a valid email."
}
struct Alert_Msg {
    static let Ok = "Okay"
    static let TryAgain = "Try Again"
    static let Wrong = "Something went wrong"
    static let Cancel = "Cancel"
    static let Opps = "Ops!"
    static let Leave = "Leave"
    static let Continue = "Continue"
    static let Done = "Done!"
    static let LogOut = "Logout"
    static let LogOut_Confirm = "Are you sure you want to logout?"
}

struct HTTP_Method {
    static let post = "POST"
    static let get = "GET"
}
