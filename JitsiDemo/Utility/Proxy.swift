//
//  Proxy.swift
//  JitsiDemo
//
//  Created by Pankaj Sonava on 21/04/20.
//  Copyright © 2020 Pankaj. All rights reserved.
//

import UIKit
import Foundation

class Proxy {
    private init() {}
    static let shared = Proxy()
    var loginUser:User!
    var isGuestAccess = true
    var isForReister = false
    var user_sdk:User_Details!
    
    // Remove session
    func removeSession() {
        loginUser = nil
        isGuestAccess = true
        UserDefaults.standard.removeObject(forKey: "Login_Data")
    }
    // Present side menu
    func presentToLeft(src:UIViewController,dst:UIViewController) {
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.modalPresentationStyle = .fullScreen
        dst.modalPresentationStyle = .overCurrentContext
        dst.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)

        src.present(dst, animated: false, completion: nil)
        UIView.animate(withDuration: 0.25,
               delay: 0.0,
               options: UIView.AnimationOptions.curveEaseInOut,
               animations: {
                    dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
            },
                   completion: { finished in
        })
    }
    // Register table view cell
    func registerTableViewCell(identifier:String,tableView:UITableView)  {
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
    }
    // Get Current Date String
    func getDateStr(formate:String,date:Date) -> String {
        Date_Formatter.dateFormat = formate
        return Date_Formatter.string(from: date)
    }
    // Email validate
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    // Show alert
    func showOnlyAlert(title:String = Alert_Msg.Opps,message:String,viewController:UIViewController)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: Alert_Msg.Ok, style: .default, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }

    func showAlertOneAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,message:String,viewController:UIViewController,callback:@escaping () -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    func showAlertTwoAction(title:String = Alert_Msg.Opps,okayTitle:String=Alert_Msg.Ok,cancelTitle:String=Alert_Msg.Cancel,message:String,viewController:UIViewController,callback:@escaping (_ isOkay:Bool) -> Void)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: okayTitle, style: .default){ (_) in
            callback(true)
        }
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { (_) in
            callback(false)
        }
        alert.addAction(action)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}

